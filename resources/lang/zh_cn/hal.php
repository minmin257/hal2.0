<?php
return [
    'HOME'              => '首页',
    'COMPANY'           => '公司',
    'PROFILE'           => '概要',
    'HISTORY'           => '沿革',
    'NEWS'              => '消息',
    'PRODUCT'           => '产品',
    'SOLUTION'          => '应用',
    'ADVANTAGE'         => '优势',
    'CONTACT'           => '联络',
    'LANGUAGE'          => '语系',

    'More'              => '了解更多',
    'Recommended'       => '推荐产品',
    'FEATURES'          => '概要',
    'DATASHEET'         => '规格',
    'INQUIRY'           => '洽询',
    'Download'          => '下載',

    'Company Name'      => '公司名称',
    'Name'              => '姓名',
    'Tel'               => '电话',
    'Product Category'  => '洽询产品分類',
    'Product Inquiry'   => '洽询产品',
    'Message'           => '洽询内容',
    'Submit'            => '送 出',
    'contact_message'   => '欢迎留下讯息，我们将会尽快回复您。',
    'verification code' => '验证码',
    'refresh'           => '点我刷新验证码',

];
