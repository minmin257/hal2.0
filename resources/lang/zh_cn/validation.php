<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
     */

    'accepted'             => '必须接受 :attribute。 ',
    'active_url'           => ':attribute 并非一个有效的网址。 ',
    'after'                => ':attribute 必须要晚于 :date。 ',
    'after_or_equal'       => ':attribute 必须要等于 :date 或更晚',
    'alpha'                => ':attribute 只能以字母组成。 ',
    'alpha_dash'           => ':attribute 只能以字母、数字、连接线(-)及底线(_)组成。 ',
    'alpha_num'            => ':attribute 只能以字母及数字组成。 ',
    'array'                => ':attribute 必须为阵列。 ',
    'before'               => ':attribute 必须要早于 :date。 ',
    'before_or_equal'      => ':attribute 必须要等于 :date 或更早。 ',
    'between'              => [
        'numeric' => ':attribute 必须介于 :min 至 :max 之间。 ',
        'file'    => ':attribute 必须介于 :min 至 :max KB 之间。 ',
        'string'  => ':attribute 必须介于 :min 至 :max 个字元之间。 ',
        'array'   => ':attribute: 必须有 :min - :max 个元素。 ',
    ],
    'boolean'              => ':attribute 必须为布林值。 ',
    'confirmed'            => ':attribute 确认栏位的输入不一致。 ',
    'date'                 => ':attribute 并非一个有效的日期。 ',
    'date_equals'          => ':attribute 必须等于 :date。 ',
    'date_format'          => ':attribute 不符合 :format 的格式。 ',
    'different'            => ':attribute 与 :other 必须不同。 ',
    'digits'               => ':attribute 必须是 :digits 位数字。 ',
    'digits_between'       => ':attribute 必须介于 :min 至 :max 位数字。 ',
    'dimensions'           => ':attribute 图片尺寸不正确。 ',
    'distinct'             => ':attribute 已经存在。 ',
    'email'                => ':attribute 必须是有效的电子邮件位址。 ',
    'exists'               => '所选择的 :attribute 选项无效。 ',
    'file'                 => ':attribute 必须是一个档案。 ',
    'filled'               => ':attribute 不能留空。 ',
    'gt'                   => [
        'numeric' => ':attribute 必须大于 :value。 ',
        'file'    => ':attribute 必须大于 :value KB。 ',
        'string'  => ':attribute 必须多于 :value 个字元。 ',
        'array'   => ':attribute 必须多于 :value 个元素。 ',
    ],
    'gte'                  => [
        'numeric' => ':attribute 必须大于或等于 :value。 ',
        'file'    => ':attribute 必须大于或等于 :value KB。 ',
        'string'  => ':attribute 必须多于或等于 :value 个字元。 ',
        'array'   => ':attribute 必须多于或等于 :value 个元素。 ',
    ],
    'image'                => ':attribute 必须是一张图片。 ',
    'in'                   => '所选择的 :attribute 选项无效。 ',
    'in_array'             => ':attribute 没有在 :other 中。 ',
    'integer'              => ':attribute 必须是一个整数。 ',
    'ip'                   => ':attribute 必须是一个有效的 IP 位址。 ',
    'ipv4'                 => ':attribute 必须是一个有效的 IPv4 位址。 ',
    'ipv6'                 => ':attribute 必须是一个有效的 IPv6 位址。 ',
    'json'                 => ':attribute 必须是正确的 JSON 字串。 ',
    'lt'                   => [
        'numeric' => ':attribute 必须小于 :value。 ',
        'file'    => ':attribute 必须小于 :value KB。 ',
        'string'  => ':attribute 必须少于 :value 个字元。 ',
        'array'   => ':attribute 必须少于 :value 个元素。 ',
    ],
    'lte'                  => [
        'numeric' => ':attribute 必须小于或等于 :value。 ',
        'file'    => ':attribute 必须小于或等于 :value KB。 ',
        'string'  => ':attribute 必须少于或等于 :value 个字元。 ',
        'array'   => ':attribute 必须少于或等于 :value 个元素。 ',
    ],
    'max'                  => [
        'numeric' => ':attribute 不能大于 :max。 ',
        'file'    => ':attribute 不能大于 :max KB。 ',
        'string'  => ':attribute 不能多于 :max 个字元。 ',
        'array'   => ':attribute 最多有 :max 个元素。 ',
    ],
    'mimes'                => ':attribute 必须为 :values 的档案。 ',
    'mimetypes'            => ':attribute 必须为 :values 的档案。 ',
    'min'                  => [
        'numeric' => ':attribute 不能小于 :min。 ',
        'file'    => ':attribute 不能小于 :min KB。 ',
        'string'  => ':attribute 不能小于 :min 个字元。 ',
        'array'   => ':attribute 至少有 :min 个元素。 ',
    ],
    'not_in'               => '所选择的 :attribute 选项无效。 ',
    'not_regex'            => ':attribute 的格式错误。 ',
    'numeric'              => ':attribute 必须为一个数字。 ',
    'present'              => ':attribute 必须存在。 ',
    'regex'                => ':attribute 的格式错误。 ',
    'required'             => ':attribute 不能留空。 ',
    'required_if'          => '当 :other 是 :value 时 :attribute 不能留空。 ',
    'required_unless'      => '当 :other 不是 :values 时 :attribute 不能留空。 ',
    'required_with'        => '当 :values 出现时 :attribute 不能留空。 ',
    'required_with_all'    => '当 :values 出现时 :attribute 不能为空。 ',
    'required_without'     => '当 :values 留空时 :attribute field 不能留空。 ',
    'required_without_all' => '当 :values 都不出现时 :attribute 不能留空。 ',
    'same'                 => ':attribute 与 :other 必须相同。 ',
    'size'                 => [
        'numeric' => ':attribute 的大小必须是 :size。 ',
        'file'    => ':attribute 的大小必须是 :size KB。 ',
        'string'  => ':attribute 必须是 :size 个字元。 ',
        'array'   => ':attribute 必须是 :size 个元素。 ',
    ],
    'starts_with'          => ':attribute 必须以 :values 其中之一开头。 ',
    'string'               => ':attribute 必须是一个字串。 ',
    'timezone'             => ':attribute 必须是一个正确的时区值。 ',
    'unique'               => ':attribute 已经存在。 ',
    'uploaded'             => ':attribute 上传失败。 ',
    'url'                  => ':attribute 的',
    'uuid'                 => ':attribute 必須是有效的 UUID。',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention 'attribute.rule' to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
     */

    'custom'               => [
        'attribute-name'       => [
            'rule-name' => 'custom-message',
        ],
        'captcha' => [
            'required' => '验证码不能留空。',
            'captcha'  => '验证码有误！',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of 'email'. This simply helps us make messages a little cleaner.
    |
     */

    'attributes'           => [
        'address'               => '地址',
        'age'                   => '年龄',
        'available'             => '可用的',
        'city'                  => '城市',
        'content'               => '内容',
        'country'               => '国家',
        'date'                  => '日期',
        'day'                   => '天',
        'description'           => '描述',
        'email'                 => 'E-mail',
        'excerpt'               => '摘要',
        'first_name'            => '名',
        'gender'                => '性别',
        'hour'                  => '时',
        'last_name'             => '姓',
        'minute'                => '分',
        'mobile'                => '手机',
        'month'                 => '月',
        'name'                  => '姓名',
        'password'              => '密码',
        'password_confirmation' => '确认密码',
        'phone'                 => '电话',
        'second'                => '秒',
        'sex'                   => '性别',
        'size'                  => '大小',
        'time'                  => '时间',
        'title'                 => '标题',
        'username'              => '使用者名字',
        'year'                  => '年',
        'state'                 => '启用状态',
        'filepath'              => '图片',
        'filepath.*'            => '图片',
        'sort'                  => '顺序',
        'tel'                   => '电话',
        'account'               => '帐号',
        'reply'                 => '回覆',
        'sitename'              => '网站名称',
        'maintain'              => '网站维护',
        'message'               => '维护讯息',
        'role'                  => '等级',
        'recipient'             => '副本',
        'company_name'          => '公司名称',
        'url'                   => '显示网址',
        'url_en'                => '显示网址(英)',
        'title_en'              => '标题(英)',
        'company'               => '公司名称',
    ],
];
