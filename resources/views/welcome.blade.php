<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title>某某系統</title>
		<link rel="shortcut icon" href="images/icon-logo.png" />

		<!-- Bootstrap -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js 讓 IE8 支援 HTML5 元素與媒體查詢 -->
		<!-- 警告：Respond.js 無法在 file:// 協定下運作 -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	</head>
	<body>
		<div class="wrap">




			<div class="container">




				<!-- 內容=================================================================== -->
				<content>


					<div class="content-title">
						<h3>聯絡表單</h3>
					</div>
					<div class="loader loader-default" data-text></div>

					<form id="contact">
						<div class="row">
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label><span>*</span>姓名：</label>
									<input type="text" name="name" class="form-control" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label><span>*</span>電話：</label>
									<input type="text" name="tel" class="form-control" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label><span>*</span>E-mail：</label>
									<input type="text" name="email" class="form-control" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="form-group">
									<label><span>*</span>地址：</label>
									<input type="text" name="add" class="form-control" required>
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label>公司名稱：</label>
									<input type="text" name="company" class="form-control">
								</div>
							</div>
							<div class="col-xs-12 col-sm-12">
								<div class="form-group">
									<label><span>*</span>內容：</label>
									<textarea name="content" rows="8" class="form-control" required></textarea>
								</div>
							</div>	
						</div>
					</form>
					<div id="error_message"></div>
					<div class="col-12 col-xs-6 col-xs-push-3 col-sm-4 col-sm-push-4 col-md-2 col-md-push-5">
						<button class="form-btn" onclick="send_mail()">送 出</button>
						<hr>
						<div class="links">
			                <a href="{{ route('backend') }}">按我導到後台登入頁</a>
			            </div>
					</div>



				</content>



			</div>















		</div>

	</body>
</html>

<script type="text/javascript">
	function send_mail(){
		$.ajax({
			url: '{{ route('create_contact') }}',
			type: 'POST',
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			data: $('#contact').serialize(),
			error: function(data) {
				var error = data.responseJSON;
				errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
				$.each(error.errors, function(key,value){
					// console.log(value);
					errorsHtml += '<li>'+ value +'</li>';
				});
				errorsHtml += '</ul></div></div>';

				$('#error_message').html(errorsHtml);
			},
			success: function(data) {
				$('#error_message').html('');
				$.ajax({
					url: '{{ route('contact.send') }}',
					type: 'POST',
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					data: $('#contact').serialize(),
					beforeSend: function() {
                        swal.fire({
							title: '郵件發送中~請稍後!',
							allowEscapeKey: false,
							allowOutsideClick: false,
							timer: 1500,
							onOpen: () => {
								swal.showLoading();
							}
						})
                    },
                    error: function(data) {
						var error = data.responseJSON;
	                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
	                    errorsHtml += '<li>'+ error.message +'</li>';
	                    errorsHtml += '</ul></div></div>';

	                    $('#error_message').html(errorsHtml);
					},
					success: function(data) {
						swal.fire({ 
							title: '已收到您的來信，我們會盡快聯絡!',
							type: 'success',
							allowEscapeKey: false,
							allowOutsideClick: false,
						}).then(function(){
	                        location.reload();
	                    })
					}
				});
			}

		});
    }
</script>