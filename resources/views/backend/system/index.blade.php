@extends('backend.system.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							設定
						</h1>
					</div>
					<div>
						<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
						<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
					</div>
					<div class="space"></div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<form method="post" id="form">
								@csrf
								<div class="form-group">
									<label>預設語言</label>
									<select name="language" class="form-control">
										<option value="zh-CN" {{ $lang->name == 'zh-CN' ? 'selected' : '' }}>簡中</option>
										<option value="en" {{ $lang->name == 'en' ? 'selected' : '' }}>英文</option>
										<option value="auto" {{ $lang->name == 'auto' ? 'selected' : '' }}>自動判定</option>
									</select>
								</div>
								<div class="form-group">
									<label>選單logo(尺寸：230*50px)</label>
									<button class="form-control btn btn-primary lfm" data-input="logo" data-preview="logo_src">
										<i class="far fa-image"></i>上傳圖片
									</button>
									<input id="logo" class="form-control" type="hidden" name="logo" value="{{ $system->logo }}">
									<div class="form-group">
										<img id="logo_src" class="img-responsive img-shadow" src="{{ $system->logo }}">
									</div>
								</div>
								<div class="form-group">
									<label>頁尾copyright</label>
									<input type="text" name="copy_right" class="form-control" value="{{ $system->copy_right }}" required>
								</div>
								<div class="form-group">
									<label>網站名稱</label>
									<input type="text" name="sitename" class="form-control" value="{{ $system->sitename }}" required>
								</div>
								<div class="form-group">
									<label>網站描述</label>
									<input type="text" name="description" class="form-control" value="{{ $system->description }}" required>
								</div>
								<div class="form-group">
									<label>網站關鍵字</label>
									<input type="text" name="keywords" class="form-control" value="{{ $system->keywords }}" required>
								</div>
								<div class="form-group">
									<label>cookie 宣告</label>
									<textarea name="cookie" class="form-control editor">{!! $system->cookie !!}</textarea>
								</div>
								<div class="form-group">
									<label>網站維護</label>
									@if($system->maintain)
										<div class="input-wrapper">
											<input type="radio" id="open_maintain" name="maintain" value="1" checked>
											<label for="open_maintain">開啟</label>
											<span></span>
											<input type="radio" id="close_maintain" name="maintain" value="0">
											<label for="close_maintain">關閉</label>
										</div>
									@else
										<div class="input-wrapper">
											<input type="radio" id="open_maintain" name="maintain" value="1">
											<label for="open_maintain">開啟</label>
											<span></span>
											<input type="radio" id="close_maintain" name="maintain" value="0" checked>
											<label for="close_maintain">關閉</label>
										</div>
									@endif
								</div>
								<div class="form-group">
									<label>維護訊息</label>
									<textarea rows="5" name="message" class="form-control" required>{{ $system->message }}</textarea>
								</div>
								{{-- 錯誤警示位置 --}}
					    	    @include('errors.errors')
								<input type="button" class="btn btn-primary" value="送出" onclick="update()">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection
	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		$(function() {
			$('.lfm').filemanager('image');
		});
		function update()
		{
			tinyMCE.triggerSave();
			event.preventDefault();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_system') }}',$('#form').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection
