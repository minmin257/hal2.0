@extends('backend.system.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							信箱設定
						</h1>
					</div>
					<div class="row">
						<form id="form">
							<div class="col-lg-4 col-xs-6">
								<label for="sender_mail">寄件信箱</label>
								<input class="form-control" type="text" name="email" id="sender_mail" value="{{ $mail_account->email }}">
							</div>
							<div class="col-lg-4 col-xs-6">
								<label for="sender_password">寄件者密碼</label>
								<div class="input-group">
									<input class="form-control" type="password" name="password" id="sender_password">
									<div class="input-group-btn">
										<button class="btn btn-success" onclick="send_mail()">驗證
											<i class="fas fa-paper-plane"></i>
										</button>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div id="error_message"></div>
					<hr>
					<div class="row">
						<form method="post" id="mail_form">
							@csrf
							<div class="col-xs-12 col-md-6">
								<div class="form-group">
									<label for="sender_name">寄件者名稱</label>
									<input type="text" name="name" class="form-control" value="{{ $mail_account->name }}">
								</div>
								<div class="form-group">
									<label>寄件主旨</label>
									<input type="text" name="subject" class="form-control" value="{{ $mail_account->subject }}">
								</div>
								<div class="form-group">
									<label>寄件主旨(英)</label>
									<input type="text" name="subject_en" class="form-control" value="{{ $mail_account->subject_en }}">
								</div>
								<div class="form-group">
									<label for="recipient">收件人信箱(副本)</label>
									<input type="text" name="recipient" class="form-control" value="{{ $mail_account->recipient }}" placeholder="example1@gamil.com,example2@yahoo.com.tw">
								</div>
								<div class="form-group">
									<label for="headline">Headline</label>
									<input type="text" name="headline" class="form-control" value="{{ $mail_account->headline }}">
								</div>
								<div class="form-group">
									<label for="signature">簽名檔</label>
									<textarea name="signature" class="form-control editor">{!! $mail_account->signature !!}</textarea>
								</div>
								{{-- 錯誤警示位置 --}}
								@include('errors.errors')
								<input type="button" class="btn btn-primary" value="送出" onclick="update()">
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function send_mail()
		{
			event.preventDefault();
			var ajaxRequest = new ajaxMail('POST','{{ route('update_mail_account') }}',$('#form').serialize());
			ajaxRequest.request();
		}
		function update()
		{
			event.preventDefault();
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_mail') }}',$('#mail_form').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection
