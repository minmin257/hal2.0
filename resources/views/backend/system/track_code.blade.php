@extends('backend.system.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							追蹤碼設定
						</h1>
					</div>
					<div class="space"></div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<form method="post" id="form">
								@csrf
								<div class="form-group">
									<label>Yahoo</label>
									<textarea rows="5" name="yahoo" class="form-control">{!! $code->yahoo !!}</textarea>
								</div>
								<div class="form-group">
									<label>Google</label>
									<textarea rows="5" name="google" class="form-control">{!! $code->google !!}</textarea>
								</div>
								<div class="form-group">
									<label>百度</label>
									<textarea rows="5" name="biadu" class="form-control">{!! $code->biadu !!}</textarea>
								</div>
								{{-- 錯誤警示位置 --}}
					    	    @include('errors.errors')
								<input type="button" class="btn btn-primary" value="送出" onclick="update()">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection
	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function update()
		{
			event.preventDefault();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_track_code') }}',$('#form').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection