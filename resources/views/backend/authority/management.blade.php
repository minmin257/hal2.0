@extends('backend.authority.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							帳號管理
						</h1>
					</div>
					<div class="row">
						@if(auth()->user()->user_has_role->role->id === 1 || auth()->user()->user_has_role->role->id === 2)
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
						</div>
						@endif
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>帳號</th>
											<th>姓名</th>
											<th>權限</th>
											<th>狀態</th>
										</tr>
									</thead>

									<tbody>
										@foreach($users as $user)
										<form id="form{{ $user->id }}">
											<input type="hidden" name="id" value="{{ $user->id }}">
											<tr>
												<td data-title="編輯">
													<a href="{{ route('edit_user') }}?id={{ $user->id }}" class="btn btn-info btn-outline btn-circle">
														<i class="fas fa-edit"></i>
													</a>
												</td>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $user->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
												<td data-title="帳號">
													{{ $user->account }}
												</td>
												<td data-title="姓名">
													<input type="hidden" name="name" value="{{ $user->name }}">
													{{ $user->name }}
												</td>
												<td data-title="權限">
													<a href="{{ route('permission') }}?id={{ $user->id }}" class="btn btn-success" title="">
														權限
													</a>
												</td>
												<td data-title="狀態">
													<select name="state" class="form-control" onchange="do_update({{ $user->id }})">
														@if($user->state)
															<option value="1" selected>啟用</option>
															<option value="0">停用</option>
														@else
															<option value="1">啟用</option>
															<option value="0" selected>停用</option>
														@endif
													</select>
												</td>
											</tr>
										</form>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增帳號</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>帳號<small>(4-10字元)</small></label>
											<input type="text" class="form-control" name="account">
										</div>
										<div class="form-group">
											<label>密碼<small>(4-10字元)</small></label>
											<input type="password" class="form-control" name="password">
										</div>
										<div class="form-group">
											<label>姓名</label>
											<input type="text" class="form-control" name="name">
										</div>
										<div class="form-group">
											<label>狀態</label>
											<select name="state" class="form-control">
												<option value="1">啟用</option>
												<option value="0">停用</option>
											</select>
										</div>
										@if(auth()->user()->user_has_role->role->id === 1)
										<div class="form-group">
											<label>等級</label>
											<select name="role" class="form-control">
												@foreach($roles as $role)
													<option value="{{ $role->id }}">{{ $role->name }}</option>
												@endforeach
											</select>
										</div>
										@elseif(auth()->user()->user_has_role->role->id === 2)
										<div class="form-group">
											<label>等級</label>
											<select name="role" class="form-control">
												@foreach($roles as $role)
													@if(!$loop->first)
													<option value="{{ $role->id }}">{{ $role->name }}</option>
													@endif
												@endforeach
											</select>
										</div>
										@endif
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="確定送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function do_update(id)
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_profile') }}',$('#form'+id).serialize());
			ajaxRequest.request();
		}

		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_account') }}',id);
			ajaxRequest.request();
		}

		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_user') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection