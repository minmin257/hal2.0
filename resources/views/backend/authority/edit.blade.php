@extends('backend.authority.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							編輯帳號
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<a href="{{ route('management') }}" title=""><button type="button" class="btn btn-default">返回</button></a>
							<div class="space"></div>
							
							<form id="form">
								<input type="hidden" name="id" value="{{ $user->id }}">
								<div class="form-group">
									<label>管理帳號</label>
									<input type="text" class="form-control" value="{{ $user->account }}" readonly>
								</div>
								<div class="form-group">
									<label>姓名</label>
									<input type="text" name="name" class="form-control" value="{{ $user->name }}" required>
								</div>
								<div class="form-group">
									<label>修改密碼<small>(4-10字元)</small></label>
									<input type="password" name="password" class="form-control">
									<small>若不更改密碼，請空白</small>
								</div>
								<div class="form-group">
									<label>確認密碼</label>
									<input type="password" name="password_confirmation" class="form-control">
								</div>
								<div class="form-group">
									<label>狀態</label>
									<select name="state" class="form-control">
										@if($user->state)
											<option value="1" selected>啟用</option>
											<option value="0">停用</option>
										@else
											<option value="1">啟用</option>
											<option value="0" selected>停用</option>
										@endif
									</select>
								</div>
								@if(auth()->user()->user_has_role->role->id === 1)
								<div class="form-group">
									<label>等級</label>
									<select name="role" class="form-control">
										@foreach($roles as $role)
											@if($user->user_has_role->role->id == $role->id)
											<option value="{{ $role->id }}" selected>{{ $role->name }}</option>
											@else
											<option value="{{ $role->id }}">{{ $role->name }}</option>
											@endif
										@endforeach
									</select>
								</div>
								@endif
								<div class="form-group">
									<label>建立日期</label>
									<p>
										{{ $user->created_at }}
									</p>
								</div>
								{{-- 錯誤警示位置 --}}
					    	    @include('errors.errors')
							</form>
							<input type="button" class="btn btn-primary" value="儲存" onclick="do_update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function do_update(id)
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_profile') }}',$('#form').serialize(),'{{ route('management') }}');
			ajaxRequest.request();
		}
	</script>
	@endsection