@extends('backend.authority.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ $user->name }} - 權限設定
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>標題</th>
											<th>新增</th>
											<th>查看</th>
											<th>編輯</th>
											<th>刪除</th>
										</tr>
									</thead>

									<tbody>
										<form id="form" method="post" action="{{ route('create_permission') }}">
											@csrf
											<input type="hidden" name="id" value="{{ $user->id }}">
											@foreach($permission_types as $permission_type)
											<tr>
												<td data-title="標題">
													{{ $permission_type->name }}
												</td>
												@foreach($permission_type->permission as $key => $permission)
													<input type="hidden" name="permission_id[]" value="{{ $permission->id }}">
													@if(count($permission->user_has_permission->where('user_id',$user->id)))
													@switch($key)
														@case('0')
															<td data-title="新增">
															@break
														@case('1')
															<td data-title="查看">
															@break
														@case('2')
															<td data-title="編輯">
															@break
														@case('3')
															<td data-title="刪除">
															@break
													@endswitch
														<select name="state[]" class="form-control on">
															<option class="on" value="1" selected>啟用</option>
															<option class="off" value="0">停用</option>
														</select>
													</td>
													@else
													<td>
														<select name="state[]" class="form-control off">
															<option class="on" value="1">啟用</option>
															<option class="off" value="0" selected>停用</option>
														</select>
													</td>
													@endif

												@endforeach
											</tr>
											@endforeach
											
										</form>
									</tbody>
								</table>
								<div class="form-group">
									<input type="submit" class="btn btn-primary" value="儲存" onclick="$('#form').submit();">
									<a href="{{ route('management') }}" class="btn btn-default" title="">返回</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script type="text/javascript">
		//select 換顏色
	    $('select.form-control').change(function(){
	        var color = $(this).find(':selected').attr('class');
	        if ( color === "on") {
	            $(this).removeClass('off').addClass('on');
	        }else if ( color === "off") {
	            $(this).removeClass('on').addClass('off');
	        }
	    });
	</script>
	@endsection