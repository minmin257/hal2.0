@extends('backend.form.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							表單管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="javascript:history.back();" title=""><button type="button" class="btn btn-default">返回</button></a>
							<fieldset>
								<center>
									<h3>
										<b>聯絡資訊</b>
									</h3>
								</center>
								<ol>
									<li>
										<label class="label-text">詢問時間:</label>{{ $contact->created_at }}
										<br>
									</li>
									<li>
										<label class="label-text">公司名稱:</label>{!! $contact->company !!}
										<br>
									</li>
									<li>
										<label class="label-text">姓名:</label>{!! $contact->name !!}
										<br>
									</li>
									<li>
										<label class="label-text">電話:</label>{!! $contact->tel !!}
										<br>
									</li>
									<li>
										<label class="label-text">信箱:</label>{!! $contact->email !!}
										<br>
									</li>
									<li>
										<label class="label-text">洽询产品分類:</label>
										@if($contact->type_id)
										{{ $contact->type->class->title }}/{{ $contact->type->title }}
										@endif
										<br>
									</li>
									<li>
										<label class="label-text">洽詢產品:</label>{{ $contact->product->title }}
										<br>
									</li>
									<li>
										<label class="label-text">洽詢內容:</label>{!! $contact->content !!}
										<br>
									</li>
									<li>
										<label class="label-text">狀態：</label>
										<form id="form">
											<input type="hidden" name="id" value="{{ $contact->id }}">
											<div class="col-md-1 row" style="margin-right: 2px">
												<select class="form-control" name="state" onchange="update()">
													@if($contact->state)
													<option value="0">未讀</option>
													<option value="1" selected>已讀</option>
													@else
													<option value="0" selected>未讀</option>
													<option value="1">已讀</option>
													@endif
												</select>
											</div>
											<div class="col-md-1 row">
												<select class="form-control" name="reply" onchange="update()">
													@if($contact->reply)
													<option value="0">未回覆</option>
													<option value="1" selected>已回覆</option>
													@else
													<option value="0" selected>未回覆</option>
													<option value="1">已回覆</option>
													@endif
												</select>
											</div>
										</form>
										<br>
										<br>
									</li>
								</ol>
							</fieldset>

							
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function update()
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('reply_contact') }}',$('#form').serialize(),'{{ route('form') }}');
			ajaxRequest.request();
		}
	</script>
	@endsection