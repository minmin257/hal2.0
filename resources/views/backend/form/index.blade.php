@extends('backend.form.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							表單管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">
				                <div class="input-group">
				                    <form id="search-form" method="post" action="{{ route('form.filter') }}">
				                    	@csrf
				                    	<input type="text" class="form-control" autocomplete="off" placeholder="請輸入公司名稱或姓名" name="search" value="{{ Request::input('search') }}">
				                    </form>
				                    <span class="input-group-btn">
				                        <button class="btn btn-default" onclick="$('#search-form').submit();"><i class="fas fa-search"></i></button>
				                    </span>
				                </div>
				            </div>

							<div class="btn-group btn-group-justified">
								<a href="{{ route('form') }}" class="btn btn-default {{ active('form') }}">全部</a>
								<a href="{{ route('form_no_read') }}" class="btn btn-default {{ active('form_no_read') }}">未讀</a>
								<a href="{{ route('form_no_reply') }}" class="btn btn-default {{ active('form_no_reply') }}">未回覆</a>
								<a href="{{ route('form_replied') }}" class="btn btn-default {{ active('form_replied') }}">已回覆</a>
							</div>
							{{-- 空格 --}}
							<div class="space"></div>

							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>查看</th>
											<th>刪除</th>
											<th>公司名稱</th>
											<th>姓名</th>
											<th>信箱</th>
											<th>詢問時間</th>
											<th></th>
											<th>回覆狀態</th>
										</tr>
									</thead>
									
									<tbody>
										@foreach($contacts as $contact)
										<tr>
											<td data-title="查看">
												<button class="btn btn-info btn-outline btn-circle" type="button" onclick="do_submit('{{ $contact->id }}')">
													<i class="fas fa-info-circle"></i>
												</button>
											</td>
											<td data-title="刪除">
												<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $contact->id }}')">
													<i class="far fa-trash-alt"></i>
												</button>
											</td>
											<td data-title="公司名稱">
												{{ $contact->company }}
											</td>
											<td data-title="姓名">
												{{ $contact->name }}
											</td>
											<td data-title="信箱">
												{{ $contact->email }}
											</td>
											<td>
												{{ $contact->created_at }}
											</td>
											<td data-title="">
												@if($contact->state)
													<label>已讀</label>
												@else
													<label class="blue">未讀</label>
												@endif
											</td>
											<td data-title="回覆狀態">
												@if($contact->reply)
													<label>已回覆</label>
												@else
													<label class="red">未回覆</label>
												@endif
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>

							<form id="form" method="post" action="{{ route('form.detail') }}">
								@csrf
								<input type="hidden" name="id">
							</form>

						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function do_submit(id)
    	{
    		$('input[name="id"]').val(id);
			if($('input[name="id"]').val()){
				$('#form').submit();
			}
    	}

    	function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_contact') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection