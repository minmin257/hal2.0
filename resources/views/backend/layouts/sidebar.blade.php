<div id="sidebar" class="sidebar responsive" data-sidebar="true" data-sidebar-scroll="true" data-sidebar-hover="true">
	<ul class="nav nav-list">
		<li class="{{ active('backend.index') }}">
			<a href="{{ route('backend.index') }}">
				<i class="menu-icon fas fa-tachometer-alt"></i>
				<span class="menu-text">首頁</span>
			</a> 
			<b class="arrow"></b>
		</li>
		@if(auth()->user()->hasPermission('content read'))
		<li class="{{ Request::segment(2) === 'content' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon far fa-edit"></i>
				<span class="menu-text">內容管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ active('home_index') }}">
					<a href="{{ route('home_index') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						首頁管理
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ active('backend.inner_banner') }}">
					<a href="{{ route('backend.inner_banner') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						內頁banner管理
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ Request::segment(3) === 'company' ? 'open' : '' }} open-submenu">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						公司管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ active('backend.about') }}">
							<a href="{{ route('backend.about') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								概要
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
					<ul class="submenu nav-hide">
						<li class="{{ active('history') }}">
							<a href="{{ route('history') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								沿革
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
					<ul class="submenu nav-hide">
						<li class="{{ active('company_page') }}">
							<a href="{{ route('company_page') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								頁面管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ Request::segment(3) === 'news' ? 'open' : '' }} open-submenu">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						消息管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>

					<ul class="submenu nav-hide">
						<li class="{{ Request::segment(4) === 'zh_cn' ? 'active' : '' }}">
							<a href="{{ route('backend.news',['lang'=>'zh_cn']) }}">
								<i class="menu-icon fas fa-caret-right"></i>
								簡中
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
					<ul class="submenu nav-hide">
						<li class="{{ Request::segment(4) === 'en' ? 'active' : '' }}">
							<a href="{{ route('backend.news',['lang'=>'en']) }}">
								<i class="menu-icon fas fa-caret-right"></i>
								English
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ Request::segment(3) === 'product' ? 'open' : '' }} open-submenu">
					<a href="#" class="dropdown-toggle">
						<i class="menu-icon fas fa-caret-right"></i>
						產品管理
						<b class="arrow fas fa-angle-down"></b>
					</a>
					<b class="arrow"></b>
					<ul class="submenu nav-hide">
						<li class="{{ active('class') }}">
							<a href="{{ route('class') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								分類管理
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
					<ul class="submenu nav-hide">
						<li class="{{ active('backend.product') }}">
							<a href="{{ route('backend.product') }}">
								<i class="menu-icon fas fa-caret-right"></i>
								產品列表
							</a>
							<b class="arrow"></b>
						</li>
					</ul>
				</li>
				<li class="{{ active('backend.solution') }}">
					<a href="{{ route('backend.solution') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						應用管理
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ active('backend.advantage') }}">
					<a href="{{ route('backend.advantage') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						優勢管理
					</a>
					<b class="arrow"></b>
				</li>
				<li class="{{ active('company_info') }}">
					<a href="{{ route('company_info') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						公司資訊
					</a>
					<b class="arrow"></b>
				</li>
			</ul>
		</li>
		@endif
		@if(auth()->user()->hasPermission('form read'))
		<li class="{{ Request::segment(2) === 'form' ? 'active' : '' }}">
			<a href="{{ route('form') }}">
				<i class="menu-icon fab fa-wpforms"></i>
				<span class="menu-text">表單管理</span>
			</a>
			<b class="arrow"></b>
		</li>
		@endif
		<li class="{{  Request::segment(2) === 'authority' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-users-cog"></i>
				<span class="menu-text">權限管理</span>
				<b class="arrow fas fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				<li class="{{ active('profile') }}">
					<a href="{{ route('profile') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						基本資料
					</a>
					<b class="arrow"></b>
				</li>
				@if(auth()->user()->hasPermission('authority read'))
				<li class="{{ (Request::segment(count(Request::segments())) === 'edit_user' || Request::segment(count(Request::segments())) === 'permission') ? 'active' : '' }}{{ active('management') }}">
					<a href="{{ route('management') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						帳號管理
					</a>
					<b class="arrow"></b>
				</li>
				@endif
			</ul>
		</li>
		@if(auth()->user()->hasPermission('system read') || auth()->user()->hasPermission('mail_account read'))
		<li class="{{ Request::segment(2) === 'system' ? 'open' : '' }}">
			<a href="#" class="dropdown-toggle">
				<i class="menu-icon fas fa-cogs"></i>
				<span class="menu-text">系統設定</span>
				<b class="arrow fa fa-angle-down"></b>
			</a>
			<b class="arrow"></b>

			<ul class="submenu">
				@if(auth()->user()->hasPermission('system read'))
				<li class="{{ active('system') }}">
					<a href="{{ route('system') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						網站設定
					</a>
					<b class="arrow"></b>
				</li>
				@endif
				@if(auth()->user()->hasPermission('mail_account read'))
				<li class="{{ active('mail_setting') }}">
					<a href="{{ route('mail_setting') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						信箱設定
					</a>
					<b class="arrow"></b>
				</li>
				@endif
				@if(auth()->user()->hasPermission('system read'))
				<li class="{{ active('track_code') }}">
					<a href="{{ route('track_code') }}">
						<i class="menu-icon fas fa-caret-right"></i>
						追蹤碼設定
					</a>
					<b class="arrow"></b>
				</li>
				@endif
			</ul>
		</li>
		@endif
	</ul>

	<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
		<i id="sidebar-toggle-icon" class="ace-icon fas fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fas fa-angle-double-right"></i>
	</div>
</div>