@extends('backend.content.product_class.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品分類---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('class') }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $class->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>標題</label>
											<input type="text" class="form-control" name="title" value="{{ $class->title }}">
										</div>
										<div class="col-md-6">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url" value="{{ $class->url }}">
										</div>
										<div class="col-md-6">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en" value="{{ $class->en->title }}">
										</div>
										<div class="col-md-6">
											<label>顯示網址(英)</label>
											<input type="text" class="form-control" name="url_en" value="{{ $class->en->url }}">
										</div>
										<div class="col-md-6">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $class->sort }}">
										</div>
									</div>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		function update()
		{
			url = $('#back').attr('href');
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_class') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
	</script>
	@endsection