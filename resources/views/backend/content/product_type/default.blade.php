@extends('backend.layouts.default')

	@inject('title', 'App\Repositories\ReadRepository')
	@section('title')
		<title>{{ $title->get_system()->sitename }}</title>
	@endsection

	@section('navbar')
		@include('backend.layouts.navbar')
	@endsection

	@section('sidebar')
		@include('backend.layouts.sidebar')
	@endsection

	@section('footer')
		@include('backend.layouts.footer')
	@endsection