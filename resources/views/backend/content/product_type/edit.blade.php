@extends('backend.content.product_type.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品小分類---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('type',['id'=>$type->class->id]) }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $type->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>分類</label>
											<select name="class_id" class="form-control">
												<option value=""></option>
												@foreach($classes as $class)
												<option value="{{ $class->id }}" {{ ($class->id == $type->class->id)?'selected':'' }}>{{ $class->title }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-6">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $type->sort }}">
										</div>
										<div class="col-md-6">
											<label>標題</label>
											<input type="text" class="form-control" name="title" value="{{ $type->title }}">
										</div>
										<div class="col-md-6">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url" value="{{ $type->url }}">
										</div>
										<div class="col-md-6">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en" value="{{ $type->en->title }}">
										</div>
										<div class="col-md-6">
											<label>顯示網址(英)</label>
											<input type="text" class="form-control" name="url_en" value="{{ $type->en->url }}">
										</div>

										
									</div>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		function update()
		{
			url = $('#back').attr('href');
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_type') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
	</script>
	@endsection