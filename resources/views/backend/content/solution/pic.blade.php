@extends('backend.content.solution.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ $solution->title }} 圖片管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('backend.solution') }}" class="btn btn-default" id="back">返回</a>
							@if(count($solution->pic) < 4)
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							@endif
							<input type="button" class="btn btn-primary" value="儲存變更" onclick="$('#form').submit()">
							<small>二語系將同步新增/刪除</small>
							@include('errors.errors')
						</div>
						<div class="col-xs-12">
							
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>刪除</th>
											<th>圖片(尺寸：800*600px)</th>
											<th>順序</th>
										</tr>
									</thead>
									<tbody>
										<form id="form" action="{{ route('update_pic') }}" method="post">
											@csrf
											@foreach($solution->pic as $pic)
											<input type="hidden" id="thumbnail{{ $pic->id }}" name="pic_src[{{ $pic->id }}]" value="{{ $pic->src }}">
											<tr>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $pic->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
												<td data-title="圖片">
													<div class="col-md-4">
														<img id="holder{{ $pic->id }}" class="img-responsive img-shadow lfm" data-input="thumbnail{{ $pic->id }}" data-preview="holder{{ $pic->id }}" src="{{ $pic->src }}">
													</div>
												</td>
												<td data-title="順序">
													<input type="number" class="form-control" name="pic_sort[{{ $pic->id }}]" pattern="[0-9]" value="{{ $pic->sort }}">
												</td>
											</tr>
											@endforeach
										</form>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增圖片</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<input type="hidden" name="solution_id" value="{{ $solution->id }}">
									<div class="col-lg-12">
										<div class="form-group">
											<label>圖片(尺寸：800*600px)</label>
											<button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="src">
										</div>
										<div class="form-group">
											<img id="holder" class="img-responsive">
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});
		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_pic') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function alert_del(id) 
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_pic') }}',id);
			ajaxRequest.request();
		}
		function update()
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_pic') }}',$('#form').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection