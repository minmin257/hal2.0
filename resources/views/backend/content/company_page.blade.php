@extends('backend.system.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							頁面管理
						</h1>
					</div>
					<div class="space"></div>
					<div class="row">
						<div class="col-xs-12 col-md-6">
							<form method="post" action="{{ route('update_page') }}">
								@csrf
								<div class="form-group">
									<label>公司</label>
									@if($page->profile)
										<div class="input-wrapper">
											<label><input type="radio" name="profile" value="1" checked>開啟</label>
											<span></span>
											<label><input type="radio" name="profile" value="0">關閉</label>
										</div>
									@else
										<div class="input-wrapper">
											<label><input type="radio" name="profile" value="1">開啟</label>
											<span></span>
											<label><input type="radio" name="profile" value="0" checked>關閉</label>
										</div>
									@endif
								</div>
								<div class="form-group">
									<label>歷程</label>
									@if($page->history)
										<div class="input-wrapper">
											<label><input type="radio" name="history" value="1" checked>開啟</label>
											<span></span>
											<label><input type="radio" name="history" value="0">關閉</label>
										</div>
									@else
										<div class="input-wrapper">
											<label><input type="radio" name="history" value="1">開啟</label>
											<span></span>
											<label><input type="radio" name="history" value="0" checked>關閉</label>
										</div>
									@endif
								</div>
								<div class="form-group">
									<label>消息</label>
									@if($page->news)
										<div class="input-wrapper">
											<label><input type="radio" name="news" value="1" checked>開啟</label>
											<span></span>
											<label><input type="radio" name="news" value="0">關閉</label>
										</div>
									@else
										<div class="input-wrapper">
											<label><input type="radio" name="news" value="1">開啟</label>
											<span></span>
											<label><input type="radio" name="news" value="0" checked>關閉</label>
										</div>
									@endif
								</div>
								{{-- 錯誤警示位置 --}}
					    	    @include('errors.errors')
								<input type="submit" class="btn btn-primary" value="送出">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection