@extends('backend.content.history.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							沿革
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
						</div>
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>年分</th>
											<th>事蹟</th>
											<th>事蹟(英)</th>
										</tr>
									</thead>

									<tbody>
										@foreach($content as $history)
										<tr>
											<td data-title="編輯">
												<button class="btn btn-info btn-outline btn-circle" onclick="edit('{{ $history->id }}')">
													<i class="fas fa-edit"></i>
												</button>
											</td>
											<td data-title="刪除">
												<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $history->id }}')">
													<i class="far fa-trash-alt"></i>
												</button>
											</td>
											<td data-title="年分">
												{{ $history->year }}
											</td>
											<td data-title="事蹟">
												{!! $history->info !!}
											</td>
											<td data-title="事蹟(英)">
												{!! $history->info_en !!}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<div class="col-xs-12 page">
									{!! $content->links() !!}
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增沿革</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>年分</label>
											<input type="number" name="year" class="form-control">
										</div>
										<div class="form-group">
											<label>事蹟</label>
											<textarea name="info" class="form-control"></textarea>
										</div>
										<div class="form-group">
											<label>事蹟(英)</label>
											<textarea name="info_en" class="form-control"></textarea>
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
		<!-- 編輯區塊 -->
		<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">編輯沿革</h4>
					</div>
					<div class="modal-body">
						<form id="form_edit">
							<div class="panel-body">
								<div class="row">
									<input type="hidden" name="id" id="id">
									<div class="col-lg-12">
										<div class="form-group">
											<label>年分</label>
											<input type="number" name="year" class="form-control" id="year">
										</div>
										<div class="form-group">
											<label>事蹟</label>
											<textarea name="info" class="form-control" id="info"></textarea>
										</div>
										<div class="form-group">
											<label>事蹟(英)</label>
											<textarea name="info_en" class="form-control" id="info_en"></textarea>
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message_edit"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="update()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_history') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function edit(id)
		{
			$.ajax({
				type : 'POST',
				url : '{{ route('edit_history') }}',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data : {id:id},
				error: function (data) {
					var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        // console.log(value);
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
				},
				success : function(data){
					$('#id').val(data.id);
					$('#year').val(data.year);
					$('#info').val(data.info);
					$('#info_en').val(data.info_en);
					$('#editModal').modal('show');
				}
				
			});
			
		}
		function update()
		{
			$.ajax({
				type : 'POST',
				url : '{{ route('update_history') }}',
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				data : $('#form_edit').serialize(),
				error: function (data) {
					var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message_edit').html(errorsHtml);
				},
				success : function(data){
					$('#error_message_edit').html('');
                    swal.fire({
                        title: '修改成功!',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        location.reload();
                    })
				}
				
			});
		}
		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_history') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection