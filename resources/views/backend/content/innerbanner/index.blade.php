@extends('backend.content.innerbanner.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							內頁banner(<label>尺寸：1920*480</label>)
						</h1>

					</div>
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-7">
							
							<ul class="nav nav-pills" id="pills-tab" role="tablist">
								@foreach($innerbanners as $innerbanner)
								<li class="nav-item">
									<a class="nav-link" data-toggle="pill" href="#pills-{{$innerbanner['id']}}">{{ $innerbanner->Subject->name }}</a>
								</li>
								@endforeach
							</ul>

							<div class="tab-content">
								@foreach($innerbanners as $innerbanner)
								<div class="tab-pane fade" id="pills-{{$innerbanner['id']}}">
									<form id="form{{$innerbanner['id']}}">
										<input type="hidden" name="id" value="{{ $innerbanner['id'] }}">
										<label style="color: #ccc">點兩下圖片刪除</label>
										<button class="form-control btn btn-primary lfm" data-input="thumbnail{{$innerbanner['id']}}" data-preview="holder{{$innerbanner['id']}}">
											<i class="far fa-image"></i>上傳圖片
										</button>
										<input id="thumbnail{{$innerbanner['id']}}" class="form-control" type="hidden" name="filepath" value="{{ $innerbanner['src'] }}">
										<div class="form-group">
											<img id="holder{{$innerbanner['id']}}" class="img-responsive img-shadow" src="{{ $innerbanner['src'] }}" ondblclick="alert_del('{{$innerbanner['id']}}')">
										</div>
									</form>
									<input type="button" class="btn btn-primary float-right" value="送出" onclick="do_update({{$innerbanner['id']}})">
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});

		function do_update(id)
		{
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_inner_banner') }}',$('#form'+id).serialize());
			ajaxRequest.request();
		}

		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_inner_banner') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection