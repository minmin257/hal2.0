@extends('backend.content.home.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							首頁管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12" style="margin-bottom: 10px">
							<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
							<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
						</div>
						<div class="col-xs-12">
							<form method="post" action="" id="form">
								@csrf
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>Banner(尺寸：1920*640)</label>
											<button class="form-control btn btn-primary lfm" data-input="banner" data-preview="banner_src">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="banner" class="form-control" type="hidden" name="banner" value="{{ $content->banner }}">
											<div class="form-group">
												<img id="banner_src" class="img-responsive img-shadow" src="{{ $content->banner }}">
											</div>
										</div>
										<div class="col-sm-12">
											<label>Banner連結</label>
											<input id="href" class="form-control" type="text" name="href" value="{{ $content->href }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>Slogan底圖</label>
											<button class="form-control btn btn-primary lfm" data-input="slogan_src" data-preview="slogan">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="slogan_src" class="form-control" type="hidden" name="slogan_src" value="{{ $content->slogan_src }}">
											<div class="form-group">
												<img id="slogan" class="img-responsive img-shadow" src="{{ $content->slogan_src }}">
											</div>
										</div>
										<div class="col-sm-12">
											<label>Slogan文字</label>
											<textarea name="slogan" class="form-control editor">{{ $content->slogan }}</textarea>
										</div>
									</div>
									<div class="row">

									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>公司底圖</label>
											<button class="form-control btn btn-primary lfm" data-input="company_src" data-preview="company">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="company_src" class="form-control" type="hidden" name="company_src" value="{{ $content->company_src }}">
											<div class="form-group">
												<img id="company" class="img-responsive img-shadow" src="{{ $content->company_src }}">
											</div>
										</div>
										<div class="col-sm-12">
											<label>公司文字</label>
											<textarea name="company" class="form-control editor">{{ $content->company }}</textarea>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-sm-6">
											<label>應用底圖</label>
											<button class="form-control btn btn-primary lfm" data-input="application_src" data-preview="application">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="application_src" class="form-control" type="hidden" name="application_src" value="{{ $content->application_src }}">
											<div class="form-group">
												<img id="application" class="img-responsive img-shadow" src="{{ $content->application_src }}">
											</div>
										</div>
									</div>
								</div>
								{{-- 錯誤警示位置 --}}
					    	    <div id="error_message"></div>
								<input type="button" class="btn btn-primary" value="送出" onclick="update()">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script src="/js/ajax.js"></script>
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});

		function update()
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_index') }}',$('#form').serialize());
			ajaxRequest.request();
		}


	</script>
	@endsection
