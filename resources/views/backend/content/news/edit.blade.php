@extends('backend.content.news.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							最新消息---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ route('backend.news',['lang'=>Session::get('backend_lang')]) }}" class="btn btn-default">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $news->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-xs-6">
											<label>日期</label>
											<input type="date" class="form-control" name="date" value="{{ $news->date }}">
										</div>
										<div class="col-xs-6">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $news->sort }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>標題</label>
									<input type="text" class="form-control" name="title" value="{{ $news->title }}">
								</div>
								<div class="form-group">
									<label>內容</label>
									<textarea name="content" class="form-control editor">{!! $news->content !!}</textarea>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		function update()
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_news') }}',$('#form').serialize(),'{{ route('backend.news',['lang'=>Session::get('backend_lang')]) }}');
			ajaxRequest.request();
		}
	</script>
	@endsection
