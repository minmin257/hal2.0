@extends('backend.content.company_info.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							公司資訊---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12" style="margin-bottom: 10px">
							<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
							<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
						</div>
						<div class="col-xs-12">
							<a href="{{ route('company_info') }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $company_info->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>地點</label>
											<input type="text" class="form-control" name="location" value="{{ $company_info->location }}">
										</div>
										<div class="col-md-4">
											<label>公司名稱</label>
											<input type="text" class="form-control" name="name" value="{{ $company_info->name }}">
										</div>
										<div class="col-md-4">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $company_info->sort }}">
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<label>電話</label>
											<input type="text" class="form-control" name="tel" value="{{ $company_info->tel }}">
										</div>
										<div class="col-md-4">
											<label>傳真</label>
											<input type="text" class="form-control" name="fax" value="{{ $company_info->fax }}">
										</div>
										<div class="col-md-4">
											<label>Email</label>
											<input type="text" class="form-control" name="email" value="{{ $company_info->email }}">
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<label>地址</label>
											<input type="text" class="form-control" name="add" value="{{ $company_info->add }}">
										</div>
										<div class="col-md-4">
											<label>地址連結</label>
											<input type="text" class="form-control" name="add_href" value="{{ $company_info->add_href }}">
										</div>
										<div class="col-md-4">
											<label>網站</label>
											<input type="text" class="form-control" name="website" value="{{ $company_info->website }}">
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<label>google map</label>
											<textarea class="form-control" name="map" rows="4">{{ $company_info->map }}</textarea>
										</div>
										
									</div>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		function update()
		{
			url = $('#back').attr('href');
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_company_info') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
	</script>
	@endsection