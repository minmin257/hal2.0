@extends('backend.content.company_info.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							公司資訊
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<small>二語系將同步新增/刪除</small>
						</div>
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>地點</th>
											<th>公司名稱</th>
											<th>順序</th>
										</tr>
									</thead>

									<tbody>
										@foreach($company_infos as $company_info)
										<tr>
											<td data-title="編輯">
												<a href="{{ route('edit_company_info') }}?id={{ $company_info->id }}" class="btn btn-info btn-outline btn-circle">
													<i class="fas fa-edit"></i>
												</a>
											</td>
											<td data-title="刪除">
												<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $company_info->id }}')">
													<i class="far fa-trash-alt"></i>
												</button>
											</td>
											<td data-title="地點">
												{{ $company_info->location }}
											</td>
											<td data-title="公司名稱">
												{{ $company_info->name }}
											</td>
											<td data-title="順序">
												{{ $company_info->sort }}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增公司資訊</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>地點</label>
											<input type="text" class="form-control" name="location">
										</div>
										<div class="form-group">
											<label>公司名稱</label>
											<input type="text" class="form-control" name="name">
										</div>
										<div class="form-group">
											<label>電話</label>
											<input type="text" class="form-control" name="tel">
										</div>
										<div class="form-group">
											<label>傳真</label>
											<input type="text" class="form-control" name="fax">
										</div>
										<div class="form-group">
											<label>Email</label>
											<input type="text" class="form-control" name="email">
										</div>
										<div class="form-group">
											<label>地址</label>
											<input type="text" class="form-control" name="add">
										</div>
										<div class="form-group">
											<label>地址連結</label>
											<input type="text" class="form-control" name="add_href">
										</div>
										<div class="form-group">
											<label>網站</label>
											<input type="text" class="form-control" name="website">
										</div>
										<div class="form-group">
											<label>google map</label>
											<textarea class="form-control" name="map"></textarea>
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_company_info') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function alert_del(id) 
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_company_info') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection