@extends('backend.content.product.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品分類---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12" style="margin-bottom: 10px">
							<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
							<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
						</div>
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('backend.product') }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $product->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-6">
											<label>圖片</label>
											<small>(建議尺寸：500*500px、產品距離邊界20px的png透明背景圖片，長寬最多不超過800px)</small>
											<button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
													<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="filepath" value="{{ $product->src }}">
											<div class="form-group">
												<img id="holder" class="img-responsive img-shadow" src="{{ $product->src }}">
											</div>
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>顯示於首頁</label>
											@if($product->display_on_index)
											<p>
												<label class="radio-inline"><input type="radio" name="display" value="1" checked>顯示</label>
												<label class="radio-inline"><input type="radio" name="display" value="0">不顯示</label>
											</p>
											@else
											<p>
												<label class="radio-inline"><input type="radio" name="display" value="1">顯示</label>
												<label class="radio-inline"><input type="radio" name="display" value="0" checked>不顯示</label>
											</p>
											@endif
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<label>狀態</label>
											@if($product->state)
											<p>
												<label class="radio-inline"><input type="radio" name="state" value="1" checked>啟用</label>
												<label class="radio-inline"><input type="radio" name="state" value="0">停用</label>
											</p>
											@else
											<p>
												<label class="radio-inline"><input type="radio" name="state" value="1">啟用</label>
												<label class="radio-inline"><input type="radio" name="state" value="0" checked>停用</label>
											</p>
											@endif
										</div>

									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>分類</label>
											<select name="class" class="form-control" onchange="show_type(this.value)">
												<option value=""></option>
												@foreach($classes as $class)
												<option value="{{ $class->id }}"{{ ($class->id == $product->type->class->id)?'selected':'' }}>{{ $class->title }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-4">
											<label>小分類</label>
											<select name="type" class="form-control">
												@foreach($types->where('product_class_id',$product->type->class->id) as $type)
												<option value="{{ $type->id }}"{{ ($type->id == $product->product_type_id)?'selected':'' }}>{{ $type->title }}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-4">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $product->sort }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>標題</label>
											<input type="text" class="form-control" name="title" value="{{ $product->title }}">
										</div>
										<div class="col-md-4">
											<label>副標題</label>
											<input type="text" class="form-control" name="subtitle" value="{{ $product->subtitle }}">
										</div>
										<div class="col-md-4">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url" value="{{ $product->url }}">
										</div>

									</div>
								</div>
								<div class="form-group">
									<label>概要</label>
									<textarea name="content" class="form-control editor">{!! $product->content !!}</textarea>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script>
		$(function() {
			$('.lfm').filemanager('image');
		});
		function update()
		{
			url = $('#back').attr('href');
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_product') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
		function show_type(id)
		{
			$('select[name=type] option').remove();
			$.ajax({
                type : 'POST',
                url : '{{ route('show_type') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                	id: id,
                },
                success : function(data){
                	$.each(data.type, function(key,value){
                        $('select[name=type]').append("<option value='"+value.id+"'>"+value.title+"</option>");
                    });

                },
                error: function (data) {
                	var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);

                }
    		});
		}
	</script>
	@endsection
