@extends('backend.content.product.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品列表
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<small>二語系將同步新增/刪除</small>
						</div>
						<div class="col-md-3" style="margin-top: 10px">
							<select name="class" class="form-control" onchange="location.replace('{{ route('backend.product') }}?type='+this.value);">
								<option value="">全部</option>
								@foreach($classes as $class)
								<optgroup label="{{ $class->title }}">
									@foreach($class->type as $type)
									<option value="{{ $type->id }}" {{ ($type->id == Request::input('type'))?'selected':'' }}>{{ $type->title }}</option>
									@endforeach
								</optgroup>
								@endforeach
							</select>
						</div>
						<div class="col-xs-12">
							
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>複製</th>
											<th>分類</th>
											<th>小分類</th>
											<th>標題</th>
											<th>標題(英)</th>
											<th>規格</th>
											<th>順序</th>
										</tr>
									</thead>

									<tbody>
										@foreach($products as $product)
										<tr>
											<td data-title="編輯">
												<a href="{{ route('edit_product') }}?id={{ $product->id }}" class="btn btn-info btn-outline btn-circle">
													<i class="fas fa-edit"></i>
												</a>
											</td>
											<td data-title="刪除">
												<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $product->id }}')">
													<i class="far fa-trash-alt"></i>
												</button>
											</td>
											<td data-title="複製">
												<button class="btn btn-default btn-outline btn-circle" type="button" onclick="copy('{{ $product->id }}')">
													<i class="far fa-copy"></i>
												</button>
											</td>
											<td data-title="分類">
												{{ $product->type->class->title }}
											</td>
											<td data-title="小分類">
												{{ $product->type->title }}
											</td>
											<td data-title="標題">
												{{ $product->title }}
											</td>
											<td data-title="標題(英)">
												{{ $product->en->title }}
											</td>
											<td data-title="規格">
												<a href="{{ route('file') }}?id={{ $product->id }}" class="btn btn-warning btn-outline btn-circle">
													<i class="fas fa-folder-open"></i>
												</a>
											</td>
											<td data-title="順序">
												{{ $product->sort }}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<div class="col-xs-12 page">
									{!! $products->appends(Request::input()) !!}
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增產品</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>分類</label>
											<select name="class" class="form-control" onchange="show_type(this.value)">
												<option value=""></option>
												@foreach($classes as $class)
												<option value="{{ $class->id }}">{{ $class->title }}</option>
												@endforeach
											</select>
										</div> 
										<div class="form-group">
											<label>小分類</label>
											<select name="type" class="form-control">
												<option value=""></option>
											</select>
										</div>      
										<div class="form-group">
											<label>標題</label>
											<input type="text" class="form-control" name="title">
										</div>
										<div class="form-group">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en">
										</div>
										<div class="form-group">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url">
										</div>
										<div class="form-group">
											<label>顯示網址(英)</label>
											<input type="text" class="form-control" name="url_en">
										</div>
										<div class="form-group">
											<label>圖片</label>
											<small>(建議尺寸：500*500px、產品距離邊界20px的png透明背景圖片，長寬最多不超過800px)</small>
											<button class="form-control btn btn-primary lfm" data-input="thumbnail" data-preview="holder">
												<i class="far fa-image"></i>上傳圖片
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="filepath">
										</div>
										<div class="form-group">
											<img id="holder" class="img-responsive">
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										<div class="form-group">
											<label class="radio-inline"><input type="radio" name="state" value="1" checked>啟用</label>
											<label class="radio-inline"><input type="radio" name="state" value="0">停用</label>
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.lfm').filemanager('image');
		});
		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_product') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function alert_del(id) 
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_product') }}',id);
			ajaxRequest.request();
		}
		function copy(id)
		{
			var ajaxRequest = new ajaxCopy('POST','{{ route('copy_product') }}',id);
			ajaxRequest.request();
		}
		function show_type(id) 
		{
			$('select[name=type] option').remove();
			$.ajax({
                type : 'POST',
                url : '{{ route('show_type') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data : {
                	id: id,
                },
                success : function(data){
                	$.each(data.type, function(key,value){
                        $('select[name=type]').append("<option value='"+value.id+"'>"+value.title+"</option>");
                    });
                	
                },
                error: function (data) {
                	var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
                	
                }
    		});
		}
	</script>
	@endsection