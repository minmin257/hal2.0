@extends('backend.content.product.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							產品規格---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ route('file',['id' => $file->product->id]) }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $file->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>標題</label>
											<input type="text" class="form-control" name="title" value="{{ $file->title }}">
										</div>
										<div class="col-md-4">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en" value="{{ $file->en->title }}">
										</div>
										<div class="col-md-4">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $file->sort }}">
										</div>
									</div>
									<div class="row">
										<div class="col-md-4">
											<label>檔案</label>
											<button class="form-control btn btn-primary file" data-input="thumbnail">
												<i class="far fa-image"></i>上傳檔案
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="href" value="{{ $file->href }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<span id="holder">已選擇：{{ $file->href }}</span>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script>
		$(function() {
			$('.file').filemanager('file');
			$('#thumbnail').change(function(){
			  	$('#holder').html('已選擇：'+$('#thumbnail').val());
			});
			
		});
		function update()
		{
			url = $('#back').attr('href');
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_file') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
		
	</script>
	@endsection