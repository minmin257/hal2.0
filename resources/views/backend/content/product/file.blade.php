@extends('backend.content.product.default')
	
	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ $product->title }} 規格管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('backend.product') }}" class="btn btn-default" id="back">返回</a>
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<small>二語系將同步新增/刪除</small>
						</div>
						<div class="col-xs-12">
							
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>標題</th>
											<th>標題(英)</th>  
											<th>順序</th>
										</tr>
									</thead>
									<tbody>
										<form id="form">
											@foreach($product->file as $file)
											<tr>
												<td data-title="編輯">
													<a href="{{ route('edit_file') }}?id={{ $file->id }}" class="btn btn-info btn-outline btn-circle">
														<i class="fas fa-edit"></i>
													</a>
												</td>
												<td data-title="刪除">
													<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $file->id }}')">
														<i class="far fa-trash-alt"></i>
													</button>
												</td>
												<td data-title="標題">
													{{ $file->title }}
												</td>
												<td data-title="標題(英)">
													{{ $file->en->title }}
												</td>
												<td data-title="順序">
													{{ $file->sort }}
												</td>
											</tr>
											@endforeach
										</form>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增規格</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<input type="hidden" name="product_id" value="{{ $product->id }}">
									<div class="col-lg-12">
										<div class="form-group">
											<label>標題</label>
											<input type="text" class="form-control" name="title">
										</div>
										<div class="form-group">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en">
										</div>
										<div class="form-group">
											<label>檔案</label>
											<button class="form-control btn btn-primary file" data-input="thumbnail">
												<i class="far fa-image"></i>上傳檔案
											</button>
											<input id="thumbnail" class="form-control" type="hidden" name="href">
										</div>
										<div class="form-group">
											<span id="holder"></span>
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]">
										</div>
										
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script src="/vendor/laravel-filemanager/js/lfm.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.file').filemanager('file');
			$('#thumbnail').change(function(){
			  	$('#holder').html('已選擇：'+$('#thumbnail').val());
			});
			
		});
		function store()
		{
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_file') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function alert_del(id) 
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_file') }}',id);
			ajaxRequest.request();
		}
		
	</script>
	@endsection