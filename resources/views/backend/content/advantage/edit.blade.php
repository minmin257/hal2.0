@extends('backend.content.advantage.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							優勢管理---編輯
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12" style="margin-bottom: 10px">
							<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
							<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
						</div>
						<div class="col-xs-12">
							<a href="{{ Session::get('previous') ?? route('backend.advantage') }}" class="btn btn-default" id="back">返回</a>
							<div class="space"></div>

							<form id="form" method="post" action="">
								@csrf
								<input type="hidden" name="id" value="{{ $advantage->id }}">
								<div class="form-group">
									<div class="row">
										<div class="col-md-4">
											<label>標題</label>
											<input type="text" class="form-control" name="title" value="{{ $advantage->title }}">
										</div>
										<div class="col-md-4">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url" value="{{ $advantage->url }}">
										</div>
										<div class="col-md-4">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="{{ $advantage->sort }}">
										</div>
									</div>
								</div>
								<div class="form-group">
									<label>內容</label>
									<textarea name="content" class="form-control editor">{!! $advantage->content !!}</textarea>
								</div>
							</form>
							{{-- 錯誤警示位置 --}}
				    	    <div id="error_message"></div>
				    	    <input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script>
		function update()
		{
			url = $('#back').attr('href');
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_advantage') }}',$('#form').serialize(),url);
			ajaxRequest.request();
		}
	</script>
	@endsection
