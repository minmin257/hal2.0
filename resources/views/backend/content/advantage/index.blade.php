@extends('backend.content.advantage.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							優勢管理
						</h1>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="#" class="btn btn-default" role="button" data-toggle="modal" data-target="#myModal">新增</a>
							<small>二語系將同步新增/刪除</small>
						</div>
						<div class="col-xs-12">
							<div class="table-responsive text-nowrap">
								<table class="table">
									<thead>
										<tr>
											<th>編輯</th>
											<th>刪除</th>
											<th>標題</th>
											<th>標題(英)</th>
											<th>順序</th>
										</tr>
									</thead>

									<tbody>
										@foreach($advantages as $advantage)
										<tr>
											<td data-title="編輯">
												<a href="{{ route('edit_advantage') }}?id={{ $advantage->id }}" class="btn btn-info btn-outline btn-circle">
													<i class="fas fa-edit"></i>
												</a>
											</td>
											<td data-title="刪除">
												<button class="btn btn-danger btn-outline btn-circle" type="button" onclick="alert_del('{{ $advantage->id }}')">
													<i class="far fa-trash-alt"></i>
												</button>
											</td>
											<td data-title="標題">
												{{ $advantage->title }}
											</td>
											<td data-title="標題(英)">
												{{ $advantage->en->title }}
											</td>
											<td data-title="順序">
												{{ $advantage->sort }}
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
								<div class="col-xs-12 page">
									{!! $advantages->links() !!}
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- 新增區塊 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title" id="myModalLabel">新增優勢</h4>
					</div>
					<div class="modal-body">
						<form id="form_create">
							<div class="panel-body">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group">
											<label>標題</label>
											<input type="text" class="form-control" name="title">
										</div>
										<div class="form-group">
											<label>標題(英)</label>
											<input type="text" class="form-control" name="title_en">
										</div>
										<div class="form-group">
											<label>顯示網址</label>
											<input type="text" class="form-control" name="url">
										</div>
										<div class="form-group">
											<label>顯示網址(英)</label>
											<input type="text" class="form-control" name="url_en">
										</div>
										<div class="form-group">
											<label>順序</label>
											<input type="number" class="form-control" name="sort" pattern="[0-9]" value="0">
										</div>
										<div class="form-group">
											<label>內容</label>
											<textarea name="content" class="form-control editor"></textarea>
										</div>
										{{-- 錯誤警示位置 --}}
										<div id="error_message"></div>
									</div>
									<!-- /.col-lg-12 -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.panel-body -->
						</form>
					</div>
					<div class="modal-footer">
						<input type="button" class="btn btn-primary" value="送出" onclick="store()">
					</div>
				</div>
			</div>
		</div>
	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/ajax.js"></script>
	<script type="text/javascript">
		$(document).on('focusin', function(e) {
		    if ($(e.target).closest(".mce-window").length) {
		        e.stopImmediatePropagation();
		    }
		});
		function store()
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxCreate('POST','{{ route('create_advantage') }}',$('#form_create').serialize());
			ajaxRequest.request();
		}
		function alert_del(id)
		{
			var ajaxRequest = new ajaxDel('POST','{{ route('delete_advantage') }}',id);
			ajaxRequest.request();
		}
	</script>
	@endsection
