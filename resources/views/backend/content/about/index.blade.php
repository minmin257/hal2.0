@extends('backend.content.about.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							概要
						</h1>
					</div>
					<div>
						<a href="{{ route('backend.lang') }}?lang=cn" class="btn btn-{{ (Session::get('backend_lang')=='cn')?'primary':'default' }}">簡中</a>
						<a href="{{ route('backend.lang') }}?lang=en" class="btn btn-{{ (Session::get('backend_lang')=='en')?'primary':'default' }}">English</a>
					</div>
					<br>
					<form method="post" action="" id="form">
						@csrf
						<div class="row">
							<div class="col-xs-12">
								<textarea name="html" class="form-control editor">{!! $about->html !!}</textarea>
							</div>
						</div>
						<div class="space"></div>
						{{-- 錯誤警示位置 --}}
						<div id="error_message"></div>
						<div class="form-group">
							<input type="button" class="btn btn-primary" value="送出" onclick="update()">
						</div>
					</form>
				</div>
			</div>
		</div>

	@endsection

	@section('js')
	<script src="/js/tinymce/tinymce.js"></script>
	<script type="text/javascript" src="/js/tinymce/tinymce_setting.js"></script>
	<script src="/js/ajax.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script>
		function update()
		{
			tinyMCE.triggerSave();
			var ajaxRequest = new ajaxUpdate('POST','{{ route('update_about') }}',$('#form').serialize());
			ajaxRequest.request();
		}
	</script>
	@endsection
