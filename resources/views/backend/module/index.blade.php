@extends('backend.module.default')

	@section('content')
		<div class="main-content">
			<div class="main-content-inner">
				<div class="page-content">
					<div class="page-header">
						<h1>
							{{ auth()->user()->name }} 歡迎登入~
						</h1>
					</div>
					
					<div class="highestAuthorityNote">
						<p>
							如有任何疑問，請與所屬專案業務聯絡，謝謝！<br>
							Tel：(02)8712-2020
						</p>
					</div>
				</div>
			</div>
		</div>
	@endsection