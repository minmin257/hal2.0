@inject('ReadRepository' ,'App\Repositories\ReadRepository')
@component('mail::message')
# Dear {{ $contact['name'] }} 

@if(isset($request))
回覆內容: <strong>{{ $request['reply'] }}</strong>
@else
This is to confirm that we have received your messages.
We will get back to you as soon as possible.

@component('mail::panel')
Create Time:<br>
{{ $contact['created_at'] }}<br>
@component('mail::table')
| 				   | 							    |
| ---------------- |:------------------------------:|
| Company:		   | {{ $contact['company'] }}      |
| Name:		       | {{ $contact['name'] }} 	    |
| Tel:			   | {{ $contact['tel'] }} 	        |
| E-mail:          | {{ $contact['email'] }} 	    |
@if($contact['type_id'])
@php
$type = $ReadRepository->get_product_type('locale')->find($contact['type_id']);
@endphp
| Product Category:| {{ str_replace('|', '/', $type->class->title) }} // {{ str_replace('|', '/', $type->title) }} |
@endif
@if($contact['product_id'])
@php
$product = $ReadRepository->get_product('locale')->find($contact['product_id']);
@endphp
| Product Inquiry: | {{ str_replace('|', '/', $product->title) }} |
@endif
| Message:	   	   | {{ $contact['content'] }}      |
@endcomponent
@endcomponent
@endif


{!! $ReadRepository->get_mail_account()->signature !!}

@endcomponent

