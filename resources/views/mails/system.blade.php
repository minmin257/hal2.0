@inject('ReadRepository' ,'App\Repositories\ReadRepository')
@component('mail::message')


@component('mail::panel')
询问时间:<br>
{{ $contact['created_at'] }}<br>
@component('mail::table')
| 				 | 							      |
| -------------- |:------------------------------:|
| 公司名称:		 | {{ $contact['company'] }}      |
| 姓名:		     | {{ $contact['name'] }} 	      |
| 电话:			 | {{ $contact['tel'] }} 	      |
| E-mail:        | {{ $contact['email'] }} 	      |
@if($contact['type_id'])
| 洽询产品分類:   | {{ str_replace('|', '/', $contact->type->class->title) }} // {{ str_replace('|', '/', $contact->type->title) }} |
@endif
@if($contact['product_id'])
| 洽询产品:	     | {{ str_replace('|', '/', $contact->product->title) }} |
@endif
| 洽询内容:		 | {{ $contact['content'] }}      |
@endcomponent
@endcomponent


{!! $ReadRepository->get_mail_account()->signature !!}

@endcomponent

