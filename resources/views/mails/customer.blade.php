@inject('ReadRepository' ,'App\Repositories\ReadRepository')
@component('mail::message')
# 亲爱的{{ $contact['name'] }}：您好~

@if(isset($request))
回覆內容: <strong>{{ $request['reply'] }}</strong>
@else
已收到您的联络!
我们会尽快处理,谢谢~

@component('mail::panel')
询问时间:<br>
{{ $contact['created_at'] }}<br>
@component('mail::table')
| 				 | 							      |
| -------------- |:------------------------------:|
| 公司名称:		 | {{ $contact['company'] }}      |
| 姓名:		     | {{ $contact['name'] }} 	      |
| 电话:			 | {{ $contact['tel'] }} 	      |
| E-mail:        | {{ $contact['email'] }} 	      |
@if($contact['type_id'])
| 洽询产品分類:   | {{ str_replace('|', '/', $contact->type->class->title) }} // {{ str_replace('|', '/', $contact->type->title) }} |
@endif
@if($contact['product_id'])
| 洽询产品:	     | {{ str_replace('|', '/', $contact->product->title) }} |
@endif
| 洽询内容:		 | {{ $contact['content'] }}      |
@endcomponent
@endcomponent
@endif


{!! $ReadRepository->get_mail_account()->signature !!}

@endcomponent

