@inject('ReadRepository' ,'App\Repositories\ReadRepository')
@component('mail::message')
# 親愛的客戶：您好~

信箱已設定完成!可以開始寄信!
@php
$info = $ReadRepository->get_company_info()->first();
@endphp

{{ $info->name }}<br>
電話: {{ $info->tel }}<br>
地址: {{ $info->add }}<br>
傳真: {{ $info->fax }}<br>
E-mail: {{ $info->email }}<br>

@endcomponent