@extends('frontend.layout.default')

	@section('banner')
  <!-- banner=================================================== -->
    <div class="banner">
      @if($content->href)
      <a href="{{ $content->href }}" title="">
      @else
      <a title="">
      @endif
        <img src="{{ $content->banner }}" alt="">
      </a>
    </div>

    <div class="slogan" style="background-image: url({{ $content->slogan_src }});">
      <div>
        <!-- ↓圖文編輯器↓ -->
        {!! $content->slogan !!}
        <!-- ↑圖文編輯器↑ -->
      </div>
    </div>
      


	@endsection

  @section('content')
  <!-- content================================================= -->
    <content>
      <!-- index-product-list -->
      <div class="product mt-5 mb-2">
        <div class="container">
          <h2 class="title-black tc mb-4">
            @lang('hal.PRODUCT')
          </h2>
          <div class="product-list">
            <div class="row">
              @foreach($products as $product)
              <div class="col-12 col-sm-6 col-lg-4">
                <a href="{{ route('product',['class' => $product->type->class->url,'type' => $product->type->url ]) }}" class="product-item">
                  <div class="img">
                    <img src="{{ $product->src }}" alt="">
                  </div>
                  <div class="title">
                    {{ $product->type->class->title }}
                  </div>
                </a>
              </div>
              @endforeach
              
            </div>
          </div>
        </div>
      </div>


      <!-- index-solution-list -->
      <div class="index-solution mt-5 mb-2" style="background-image: url({{ $content->application_src }});">
        <div class="container">
          <h2 class="title-black tc mb-4">
            @lang('hal.SOLUTION')
          </h2>
          <div class="tc">
            <div class="row">
              @foreach($solutions as $solution)
              <div class="col-12 col-sm-6 col-lg-3">
                <a class="solution-index-item" href="{{ route('solution',['url'=>$solution->url]) }}" title="">
                  {{ $solution->title }}
                </a>
              </div>
              @endforeach
              <div class="col-12 mt-4">
                @if($solutions->first())
                <a class="solution-more" href="{{ route('solution',['url'=>$solutions->first()->url]) }}" title="">
                  » @lang('hal.More')
                </a>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>



      <!-- index-company -->
      <div class="container">
        <div class="index-company">
          <div class="row">
            <div class="col-12 col-lg-6">
              <h2 class="title-black mb-2">
                @lang('hal.COMPANY')
              </h2>
              <div>
                <!-- ↓圖文編輯器↓ -->
                {!! $content->company !!}
                <!-- ↑圖文編輯器↑ -->
              </div>
            </div>
            <div class="col-12 col-lg-6">
              <div class="index-company-img" style="background-image: url({{ $content->company_src }});">
              </div>
            </div>
          </div>
        </div>
      </div>
    </content>
    
  

  @endsection



