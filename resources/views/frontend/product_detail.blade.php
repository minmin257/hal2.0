@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.PRODUCT')</li>
              <li class="breadcrumb-item"><a href="{{ route('product',['class'=>$product->type->class->url,'type'=>$product->type->url]) }}">{{ $product->type->class->title }}</a></li>
              <li class="breadcrumb-item"><a href="{{ route('product',['class'=>$product->type->class->url,'type'=>$product->type->url]) }}">{{ $product->type->title }}</a></li>
            </ol>
          </nav>
        </div>
        <div class="mt-5">
          <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
              <div class="side-menu">
                <div class="card">
                  <div class="card-header">
                    {{ $product->type->class->title }}
                    <a href="javascript:;" data-toggle="collapse" data-target="#sideMenu" aria-controls="sideMenu" aria-expanded="false">
                      <i class="fas fa-angle-right"></i>
                    </a>
                  </div>
                  <div class="collapse show" id="sideMenu" >
                    <ul class="list-group list-group-flush">
                      @foreach($classes->where('id',$product->type->class->id)->first()->type as $type)
                      <li class="list-group-item {{ ($type->id == $product->type->id)?'active':'' }}">
                        <a href="{{ route('product',['class'=>$type->class->url,'type'=>$type->url]) }}">
                          <i class="fas fa-angle-right"></i>
                          <span>{{ $type->title }}</span>
                        </a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <div class="content">

                <!-- 產品標題 -->
                <div class="product-title">
                  <h3>
                    {{ $product->title }}
                    <small>
                      {{ $product->subtitle }}
                    </small>
                  </h3>
                  <a href="javascript:history.back();" title="">
                    > Back
                  </a>
                </div>

                <!-- 產品圖 -->
                <div class="product-img">
                  <!-- 寬高不超過800px -->
                  <img src="{{ $product->src }}" alt="">
                </div>


                <!-- 產品介紹 -->
                <div class="product-info">
                  <ul class="nav nav-tabs row" id="myTab" role="tablist">
                    <li class="col-4 nav-item tc">
                      <a class="nav-link active" id="productDetails-tab" data-toggle="tab" href="#productDetails" role="tab" aria-controls="productDetails" aria-selected="true">@lang('hal.FEATURES')</a>
                    </li>
                    <li class="col-4 nav-item tc">
                      <a class="nav-link" id="productDownload-tab" data-toggle="tab" href="#productDownload" role="tab" aria-controls="productDownload" aria-selected="false">@lang('hal.DATASHEET')</a>
                    </li>
                    <li class="col-4 nav-item tc">
                      <a class="nav-link" href="{{ route('contact',['product' => $product->url ]) }}">@lang('hal.INQUIRY')</a>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="productDetails" role="tabpanel" aria-labelledby="productDetails-tab">
                      <!-- ↓概要圖文編輯器↓ -->
                      {!! $product->content !!}
                      <!-- ↑概要圖文編輯器↑ -->
                    </div>
                    <div class="tab-pane fade" id="productDownload" role="tabpanel" aria-labelledby="productDownload-tab">
                      <!-- ↓規格下載↓ -->
                      <ul class="product-download">
                        @foreach($product->file as $file)
                        <li>
                          <a href="{{ $file->href }}" target="_blank">
                            <i class="fa fa-arrow-circle-down"></i>
                            <span>{{ $file->title }} @lang('hal.Download')</span>
                          </a>
                        </li>
                        @endforeach
                        
                      </ul>
                      <!-- ↑規格下載↑ -->
                    </div>
                  </div>


                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </content>
  
  @endsection
  
