@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.SOLUTION')</li>
              <li class="breadcrumb-item" aria-current="page">{{ $select->title }}</li>
            </ol>
          </nav>
        </div>
        <div class="mt-5">
          <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
              <div class="side-menu">
                <div class="card">
                  <div class="card-header">
                    @lang('hal.SOLUTION')
                    <a href="javascript:;" data-toggle="collapse" data-target="#sideMenu" aria-controls="sideMenu" aria-expanded="false">
                        <i class="fas fa-angle-right"></i>
                    </a>
                  </div>
                  <div class="collapse show" id="sideMenu" >
                    <ul class="list-group list-group-flush">
                        @foreach($solutions as $solution)
                        <li class="list-group-item {{ ($solution->id == $select->id)?'active':'' }}">
                            <a href="{{ route('solution',['url'=>$solution->url]) }}">
                                <i class="fas fa-angle-right"></i>
                                <span>{{ $solution->title }}</span>
                            </a>
                        </li>
                        @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <div class="content">
                <h3 class="title-blue">
                  {{ $select->title }}
                </h3>
                <div class="solution-block">
                  <div class="row">
                    <div class="col-12 col-lg-6">
                      <!-- ↓圖文編輯器↓ -->
                      {!! $select->content !!}
                      <!-- ↑圖文編輯器↑ -->
                    </div>
                    <div class="col-12 col-lg-6">
                      <div class="row">
                        @foreach($select->pic as $pic)
                        <div class="col-12 col-sm-6">
                          <!-- <a href="{{ $pic->src }}" class="solution-img" data-lightbox="aa" style="background-image: url({{ $pic->src }});"></a> -->
                          <div class="solution-img" style="background-image: url({{ $pic->src }});"></div>
                        </div>
                        @endforeach
                      </div>
                    </div>
                  </div>
                </div>
                @if(count($products) > 0)
                <h4 class="title-s">
                  @lang('hal.Recommended')
                </h4>
                <div class="product-list">
                  <div class="row">
                    @foreach($products as $product)
                    <div class="col-12 col-sm-6 col-lg-4">
                      <a href="{{ route('product',['class' => $product->type->class->url,'type' => $product->type->url ,'product' => $product->url]) }}" class="product-item">
                        <div class="img">
                          <img src="{{ $product->src }}" alt="">
                        </div>
                        <div class="title">
                          {{ $product->title }}
                        </div>
                      </a>
                    </div>
                    @endforeach
                  </div>
                </div>
                @endif
              </div>
            </div>
          </div>
        </div>
      </div>
    </content>

  @endsection
