@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.COMPANY')</li>
              <li class="breadcrumb-item" aria-current="page"><a href="{{ route('company',['url' => 'news']) }}">@lang('hal.NEWS')</a></li>
            </ol>
          </nav>
        </div>
        <div class="mt-5">
          <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
              <div class="side-menu">
                <div class="card">
                  <div class="card-header">
                    @lang('hal.COMPANY')
                    <a href="javascript:;" data-toggle="collapse" data-target="#sideMenu" aria-controls="sideMenu" aria-expanded="false">
                        <i class="fas fa-angle-right"></i>
                    </a>
                  </div>
                  <div class="collapse show" id="sideMenu" >
                    <ul class="list-group list-group-flush">
                        @if($page->profile)
                        <li class="list-group-item">
                            <a href="{{ route('company',['url' => 'profile']) }}">
                                <i class="fas fa-angle-right"></i>
                                <span>@lang('hal.PROFILE')</span>
                            </a>
                        </li>
                        @endif
                        @if($page->history)
                        <li class="list-group-item">
                            <a href="{{ route('company',['url' => 'history']) }}">
                                <i class="fas fa-angle-right"></i>
                                <span>@lang('hal.HISTORY')</span>
                            </a>
                        </li>
                        @endif
                        @if($page->news)
                        <li class="list-group-item active">
                            <a href="{{ route('company',['url' => 'news']) }}">
                                <i class="fas fa-angle-right"></i>
                                <span>@lang('hal.NEWS')</span>
                            </a>
                        </li>
                        @endif
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <div class="content">
                <h3 class="title-blue">
                  @lang('hal.NEWS')
                </h3>
                <div class="news-block mb-5">
                  <div class="news-title">
                    <h4>{{ $news->title }}</h4>
                    <span class="date">{{ date('Y/m/d',strtotime($news->date)) }}</span>
                    <a href="javascript:history.back();" title="">
                      > Back
                    </a>
                  </div>
                  <div class="news-text">
                    <!-- ↓圖文編輯器↓ -->
                    {!! $news->content !!}
                    <!-- ↑圖文編輯器↑ -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </content>
  
  @endsection

