@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.PRODUCT')</li>
              <li class="breadcrumb-item">
                @if($select_class->type->count() > 0)
                  <a href="{{ route('product',['class' => $select_class->url, 'type' => $select_class->type->first()->url]) }}">
                @else
                  <a href="{{ route('product',['class' => $select_class->url]) }}">
                @endif
                  {{ $select_class->title }}
                </a>
              </li>
              <li class="breadcrumb-item" aria-current="page">{{ $select_type->title ?? '' }}</li>
            </ol>
          </nav>
        </div>
        <div class="mt-5">
          <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
              <div class="side-menu">
                <div class="card">
                  <div class="card-header">
                    @lang('hal.PRODUCT')
                    <a href="javascript:;" data-toggle="collapse" data-target="#sideMenu" aria-controls="sideMenu" aria-expanded="false">
                      <i class="fas fa-angle-right"></i>
                    </a>
                  </div>
                  <div class="collapse show" id="sideMenu" >
                    <ul class="list-group list-group-flush">
                      @foreach($classes as $class)
                      <li class="list-group-item {{ ($class->id == $select_class->id)?'active':'' }}">
                        @if($class->type->count() > 0)
                          <a href="{{ route('product',['class' => $class->url, 'type' => $class->type->first()->url]) }}">
                        @else
                          <a href="{{ route('product',['class' => $class->url]) }}">
                        @endif
                            <i class="fas fa-angle-right"></i>
                            <span>{{ $class->title }}</span>
                          </a>

                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <div class="content">
                <h3 class="title-blue">
                  {{ $select_class->title }}
                </h3>

                <div class="product-type-list">
                  <div class="row">
                    @foreach($select_class->type as $type)
                    <div class="col-12 col-sm-6 col-lg-4">
                      <a class="product-type-item {{ ($type->id == $select_type->id)?'active':'' }}" href="{{ route('product',['class' => $type->class->url, 'type' => $type->url]) }}" title="{{ $type->title }}">
                        {{ $type->title }}
                        <i class="fas fa-angle-right"></i>
                      </a>
                    </div>
                    @endforeach
                  </div>
                </div>


                <div class="product-list">
                  <div class="row">
                    @if($select_type)
                      @foreach($select_type->product as $product)
                      <div class="col-12 col-sm-6 col-lg-4">
                        <a href="{{ route('product',['class' => $product->type->class->url,'type' => $product->type->url ,'product' => $product->url]) }}" class="product-item">
                          <div class="img">
                            <img src="{{ $product->src }}" alt="">
                          </div>
                          <div class="title">
                            {{ $product->title }}
                          </div>
                        </a>
                      </div>
                      @endforeach
                    @endif
                  </div>
                </div>


              </div>
            </div>
          </div>
        </div>
      </div>
    </content>

  @endsection

