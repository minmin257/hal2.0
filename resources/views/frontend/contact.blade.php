@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.CONTACT')</li>
            </ol>
          </nav>
        </div>
        <div class="row mt-5 justify-content-center">
          <div class="col-lg-8">
            <h2 class="title-black tc mb-4">
              @lang('hal.CONTACT')
            </h2>
            <p class="mt-5 mb-5 tc">
              @lang('hal.contact_message')
            </p>
            <form class="mb-5" id="contact">
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>*@lang('hal.Company Name')：</label>
                    <input type="text" class="form-control" name="company">
                  </div>
                  <div class="form-group">
                    <label>*@lang('hal.Name')：</label>
                    <input type="text" class="form-control" name="name">
                  </div>
                  <div class="form-group">
                    <label>*@lang('hal.Tel')：</label>
                    <input type="text" class="form-control" name="tel">
                  </div>
                  <div class="form-group">
                    <label>*E-mail：</label>
                    <input type="email" class="form-control" name="email">
                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group">
                    <label>@lang('hal.Product Category')：</label>
                    <select class="form-control" name="type_id" onchange="show_product(this.value)">
                      <option value=""></option>
                      @foreach($classes as $class)
                      <optgroup label="{{ $class->title }}">
                        @foreach($class->type as $type)
                          @if($product)
                          <option value="{{ $type->id }}" {{ ($product->product_type_id == $type->id)?'selected':'' }}>{{ $type->title }}</option>
                          @else
                          <option value="{{ $type->id }}">{{ $type->title }}</option>
                          @endif
                        @endforeach
                      </optgroup>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label>@lang('hal.Product Inquiry')：</label>
                    <select class="form-control" name="product_id">
                      <option value=""></option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>*@lang('hal.Message')：</label>
                    <textarea class="form-control" style="height:130px;" name="content"></textarea>
                  </div>
                </div>
                <div class="col-12 col-md-6 mb-3">
                  <div class="form-group">
                    <label>@lang('hal.verification code')：</label>
                    <div class="input-group"> 
                      <input type="text" name="captcha" maxlength="4" class="form-control" required autocomplete="off">
                      <span class="input-group-addon">
                        <img src="{{captcha_src()}}" onclick="this.src='/captcha/default?'+Math.random()" id="captchaCode" class="captcha">
                      </span>
                    </div>
                    <a rel="nofollow" href="javascript:;" onclick="document.getElementById('captchaCode').src='captcha/default?'+Math.random()" class="reflash" style="color: #ddd">
                      @lang('hal.refresh')
                    </a>
                  </div>
                </div>
                <div class="col-12 col-md-6 tc">
                  <button type="button" class="contact-btn" onclick="send_mail()">
                    @lang('hal.Submit')
                  </button>
                <div id="error_message"></div>
                </div>
              </div>
            </form>


            <div class="company-list mt-5 mb-5">
              @foreach($companys as $company)
              <div class="company-item">
                <span class="location">
                  {{ $company->location }}
                </span>
                <h4 class="title">
                  {{ $company->name }}
                </h4>
                <ul class="info">
                  @if($company->tel)
                  <li>
                    <i class="fas fa-phone-alt"></i>
                    <span>
                      <a href="tel:+{{ preg_replace('/[^\d\#]/','',$company->tel) }}" title="">{{ $company->tel }}</a>
                    </span>
                  </li>
                  @endif
                  @if($company->fax)
                  <li>
                    <i class="fas fa-print"></i>
                    <span>
                      {{ $company->fax }}
                    </span>
                  </li>
                  @endif
                  @if($company->email)
                  <li>
                    <i class="fas fa-envelope"></i>
                    <span>
                      <a href="mailto:{{ $company->email }}" title="">{{ $company->email }}</a>
                    </span>
                  </li>
                  @endif
                  @if($company->add)
                  <li>
                    <i class="fas fa-map-marker-alt"></i>
                    <span>
                      <a href="{{ $company->add_href }}" target="_blank">{{ $company->add }}</a>
                    </span>
                  </li>
                  @endif
                  @if($company->website)
                  <li>
                    <i class="fas fa-globe-americas"></i>
                    <span>
                      <a href="{{ $company->website }}" target="_blank">{{ $company->website }}</a>
                    </span>
                  </li>
                  @endif
                </ul>
                <div class="map">
                  {!! $company->map !!}
                </div>
              </div>
              @endforeach
              
            </div>


          </div>
        </div>
      </div>
    </content>
  
  @endsection

  @section('js')
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript">
    $(function() {
      @if($product)
        show_product('{{ $product->product_type_id }}');
      @endif
    });
    function send_mail()
    {
      $.ajax({
        url: '{{ route('create_contact') }}',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: $('#contact').serialize(),
        error: function(data) {
          var error = data.responseJSON;
          errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
          $.each(error.errors, function(key,value){
          // console.log(value);
          errorsHtml += '<li>'+ value +'</li>';
          });
          errorsHtml += '</ul></div></div>';

          $('#error_message').html(errorsHtml);
        },
        success: function(data) {
          $('#error_message').html('');
          $.ajax({
            url: '{{ route('contact.send') }}',
            type: 'POST',
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: $('#contact').serialize(),
            beforeSend: function() {
              swal.fire({
                @if(Session::has('locale'))
                  @if(Session::get('locale') == 'en')
                    title: 'Sending email~',
                  @else
                    title: '郵件發送中~請稍後!',
                  @endif
                @else
                  @if(app()->getLocale() == 'en')
                    title: 'Sending email~',
                  @else
                    title: '郵件發送中~請稍後!',
                  @endif
                @endif
                allowEscapeKey: false,
                allowOutsideClick: false,
                // timer: 1500,
                onOpen: function() {
                  swal.showLoading();
                }
              })
            },
            complete: function(){
              swal.fire({
                @if(Session::has('locale'))
                  @if(Session::get('locale') == 'en')
                    title: 'We have already received your messages and will get back to you shortly.',
                  @else
                    title: '已收到您的來信，我們會盡快聯絡!',
                  @endif
                @else
                  @if(app()->getLocale() == 'en')
                    title: 'We have already received your messages and will get back to you shortly.',
                  @else
                    title: '已收到您的來信，我們會盡快聯絡!',
                  @endif
                @endif 
                
                type: 'success',
                allowEscapeKey: false,
                allowOutsideClick: false,
              }).then(function(){ 
                location.reload();
              })
            },
            error: function(data) {

              var error = data.responseJSON;
              errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
              $.each(error.errors, function(key,value){
                // console.log(value);
                errorsHtml += '<li>'+ value +'</li>';
              });
              errorsHtml += '</ul></div></div>';

              $('#error_message').html(errorsHtml);
            },
            success: function(data) {
              // location.reload();
            }
          });
        }

      });
    }
    function show_product(type_id)
    {
      $('select[name=product_id] option').remove();
      $.ajax({
        url: '{{ route('show_product') }}',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          type_id:type_id,
        },
        error: function(data) {
          var error = data.responseJSON;
          errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
          $.each(error.errors, function(key,value){
          // console.log(value);
          errorsHtml += '<li>'+ value +'</li>';
          });
          errorsHtml += '</ul></div></div>';

          $('#error_message').html(errorsHtml);
        },
        success: function(data) {
          $('#error_message').html('');
          $('select[name=product_id]').append("<option value=''></option>");
          $.each(data, function(key,value){
            $('select[name=product_id]').append("<option value='"+value.id+"'>"+value.title+"</option>");
          });
          @if($product)
            $('select[name=product_id]').val('{{ $product->id }}');
          @endif
        }

      });
    }
  </script>
  @endsection