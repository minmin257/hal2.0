@extends('frontend.layout.default')



	
	@section('banner')
      <!-- banner================================================= -->
      <div class="banner">
        <img src="{{ $banner->src }}" alt="">
      </div>

	    
	@endsection

  @section('content')
    <content>
      <div class="container">
        <div class="path">
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{ route('index') }}">@lang('hal.HOME')</a></li>
              <li class="breadcrumb-item">@lang('hal.ADVANTAGE')</li>
              <li class="breadcrumb-item" aria-current="page">{{ $select->title }}</li>
            </ol>
          </nav>
        </div>
        <div class="mt-5">
          <div class="row">
            <div class="col-12 col-md-4 col-lg-3">
              <div class="side-menu">
                <div class="card">
                  <div class="card-header">
                    @lang('hal.ADVANTAGE')
                    <a href="javascript:;" data-toggle="collapse" data-target="#sideMenu" aria-controls="sideMenu" aria-expanded="false">
                      <i class="fas fa-angle-right"></i>
                    </a>
                  </div>
                  <div class="collapse show" id="sideMenu" >
                    <ul class="list-group list-group-flush">
                      @foreach($advantages as $advantage)
                      <li class="list-group-item {{ ($advantage->id == $select->id)?'active':'' }}">
                        <a href="{{ route('advantage',['url'=>$advantage->url]) }}">
                          <i class="fas fa-angle-right"></i>
                          <span>{{ $advantage->title }}</span>
                        </a>
                      </li>
                      @endforeach
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-md-8 col-lg-9">
              <div class="content">
                <h3 class="title-blue">
                  {{ $select->title }}
                </h3>
                <div>
                  <!-- ↓圖文編輯器↓ -->
                  {!! $select->content !!}
                  <!-- ↑圖文編輯器↑ -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </content>

  @endsection
