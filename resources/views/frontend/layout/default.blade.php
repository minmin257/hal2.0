<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" locale="{{ session('locale') }}">
  <head>
    @inject('read', 'App\Repositories\ReadRepository')
    {!! $read->get_track_code()->yahoo !!}

    {!! $read->get_track_code()->google !!}

    {!! $read->get_track_code()->biadu !!}
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    

    <!-- 網站標題icon -->

    <title>{{ $read->get_system()->sitename }}</title>
    <link rel="shortcut icon" href="/images/logo-icon.png" />
    <meta name="keywords" content="{{ $read->get_system()->keywords }}">
    <meta name="description" content="{{ $read->get_system()->description }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans|Noto+Sans+SC&display=swap" rel="stylesheet">
    <link href="/css/bootstrap_r.css" rel="stylesheet">
    <link href="/css/lightbox.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
  </head>

  <body>

    <div class="wrap">

      <!-- header=================================================== -->
      @if(Session::has('locale'))
        @if(Session::get('locale') == 'en')
          <div class="header en">
        @else
          <div class="header">
        @endif
      @else
        @if(app()->getLocale() == 'en')
        <div class="header en">
        @else
          <div class="header">
        @endif
      @endif
      

        <nav class="navbar navbar-expand-lg">
          <div class="container">
            <a class="navbar-brand" href="{{ route('index') }}">
              <img src="{{ $read->get_system()->logo }}" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <i class="fas fa-bars"></i>
            </button>


            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle"{{--  href="javascript:;" --}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @lang('hal.COMPANY')
                  </a>

                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    @if($read->get_company_page()->profile)
                    <a class="dropdown-item" href="{{ route('company',['url' => 'profile']) }}">@lang('hal.PROFILE')</a>
                    @endif
                    @if($read->get_company_page()->history)
                    <a class="dropdown-item" href="{{ route('company',['url' => 'history']) }}">@lang('hal.HISTORY')</a>
                    @endif
                    @if($read->get_company_page()->news)
                    <a class="dropdown-item" href="{{ route('company',['url' => 'news']) }}">@lang('hal.NEWS')</a>
                    @endif
                  </div>

                </li>

                <li class="nav-item dropdown">

                  <a class="nav-link dropdown-toggle"{{--  href="javascript:;" --}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                    @lang('hal.PRODUCT')

                  </a>

                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">

                    @foreach($read->get_product_class('locale')->get() as $class)
                      @if($class->type->count() > 0)
                      <a class="dropdown-item" href="{{ route('product',['class' => $class->url,'type'=>$class->type->first()->url]) }}">{{ $class->title }}</a>
                      @else
                      <a class="dropdown-item" href="{{ route('product',['class' => $class->url]) }}">{{ $class->title }}</a>
                      @endif
                    @endforeach
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle"{{--  href="javascript:;" --}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @lang('hal.SOLUTION')
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($read->get_solution('locale')->get() as $solution)
                    <a class="dropdown-item" href="{{ route('solution',['url'=>$solution->url]) }}">{{ $solution->title }}</a>
                    @endforeach
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle"{{--  href="javascript:;" --}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @lang('hal.ADVANTAGE')
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    @foreach($read->get_advantage('locale')->get() as $advantage)
                    <a class="dropdown-item" href="{{ route('advantage',['url'=>$advantage->url]) }}">{{ $advantage->title }}</a>
                    @endforeach
                  </div>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link" href="{{ route('contact') }}">@lang('hal.CONTACT')</a>
                </li>
                <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle"{{--  href="javascript:;" --}} id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @lang('hal.LANGUAGE')
                  </a>
                  <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="{{ route('set_lang') }}?lang=zh_cn">简体中文</a>
                    <a class="dropdown-item" href="{{ route('set_lang') }}?lang=en">English</a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>

      @yield('banner')
      

      @yield('content')
      

    </div>






    <!-- footer====================================================== -->

    <div class="footer">
      <div class="container">
        {{ $read->get_system()->copy_right }}
        <!-- <a href="http://www.yuan-pu.com.tw/" target="_blank">Design by YUAN-PU</a> -->
      </div>
    </div>







    <div class="side-btn">
      <a class="btn-contact" href="{{ route('contact') }}">
        <i class="fas fa-comment-alt"></i>
        @if(Session::has('locale'))
          @if(Session::get('locale') == 'en')
            <span class="en">
          @else
            <span>
          @endif
        @else
          @if(app()->getLocale() == 'en')
          <span class="en">
          @else
            <span>
          @endif
        @endif
          @lang('hal.CONTACT')</span>
      </a>
      <a class="btn-goTop" href="javascript:;">
        <i class="fas fa-angle-up"></i>
      </a>
    </div>



    <!-- cookie-announce -->
    <div class="cookie-announce move">
      <div class="container-fluid">
        <div>
          {!! $read->get_system()->cookie !!}
        </div>
        <a class="cookie-close btn confirm">OK</a>
      </div>
    </div>



    <!-- 選擇性的 JavaScript -->
    <script src="/js/jquery.1.11.1.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/lightbox.js"></script>
    <script src="https://kit.fontawesome.com/588be6838c.js"></script>
    <script src="/js/yp.js"></script>

    @yield('js')

  </body>
</html>