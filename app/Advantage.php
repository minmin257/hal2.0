<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage extends Model
{
    protected $fillable = [
    	'title','url','content','delete','sort'
    ];

    public function en()
    {
        return $this->hasOne(Advantage_en::class,'id','id')->where('delete',0);
    }
}
