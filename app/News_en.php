<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News_en extends Model
{
    protected $fillable = [
        'date', 'title', 'content', 'sort','delete'
    ];
}
