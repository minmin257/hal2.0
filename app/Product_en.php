<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_en extends Model
{
    protected $fillable = [
    	'title','subtitle','url','sort','delete','content','src','product_type_id','state','display_on_index'
    ];

    public function type()
    {
    	return $this->belongsTo(ProductType_en::class ,'product_type_id','id');
    }

    public function file()
    {
    	return $this->hasMany(ProductFile_en::class,'product_id','id')->where('delete',0)->orderBy('sort','desc');
    }
}
