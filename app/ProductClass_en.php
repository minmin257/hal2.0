<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductClass_en extends Model
{
    protected $fillable = [
    	'title','url','sort','delete'
    ];

    public function type()
    {
    	return $this->hasMany(ProductType_en::class,'product_class_id','id')->where('delete',0)->orderBy('sort','desc');
    }
}
