<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyPage extends Model
{
    protected $fillable = [
    	'profile','history','news',
    ];
}
