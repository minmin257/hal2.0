<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType_en extends Model
{
    protected $fillable = [
    	'title','url','sort','delete','product_class_id'
    ];

    public function class()
    {
    	return $this->belongsTo(ProductClass_en::class ,'product_class_id','id');
    }

    public function product()
    {
    	return $this->hasMany(Product_en::class,'product_type_id','id')->where('delete',0)->orderBy('sort','desc')->where('state',1);
    }
}
