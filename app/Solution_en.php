<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solution_en extends Model
{
    protected $fillable = [
    	'title','url','content','delete','sort'
    ];

    public function pic()
    {
    	return $this->hasMany(SolutionPic::class,'solution_id','id')->where('delete',0)->orderBy('sort','desc');
    }
    public function product()
    {
        return $this->hasMany(SolutionHasProduct::class,'solution_id','id');
    }
}
