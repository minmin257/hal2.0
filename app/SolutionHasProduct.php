<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolutionHasProduct extends Model
{
    protected $fillable = [
    	'solution_id','product_id',
    ];
}
