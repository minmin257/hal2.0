<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solution extends Model
{
    protected $fillable = [
    	'title','url','content','delete','sort'
    ];

    public function en()
    {
        return $this->hasOne(Solution_en::class,'id','id')->where('delete',0);
    }

    public function pic()
    {
    	return $this->hasMany(SolutionPic::class,'solution_id','id')->where('delete',0)->orderBy('sort','desc');
    }
    public function product()
    {
        return $this->hasMany(SolutionHasProduct::class,'solution_id','id');
    }
}
