<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Index extends Model
{
    protected $fillable = [
    	'banner','href','slogan','slogan_src','application_src','company','company_src'
    ];

}
