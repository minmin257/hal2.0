<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InnerBanner extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject_id', 'src'
    ];

    public function Subject()
    {
    	return $this->belongsTo(Subject::class,'subject_id','id');
    }
}
