<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionType extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function permission()
    {
    	return $this->hasMany(Permission::class);
    }
}
