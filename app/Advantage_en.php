<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advantage_en extends Model
{
    protected $fillable = [
    	'title','url','content','delete','sort'
    ];

    
}
