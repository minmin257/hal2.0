<?php

namespace App\Console\Commands;
use Illuminate\Console\Command;
class SiteMapCommand extends Command
{
    protected $signature = 'sitemap:build';
    protected $description = 'Making sitemap.xml';
    public function __construct()
    {
        parent::__construct();
    }
     
        /**
         * Execute the console command.
         *
         * @return mixed
         */
    public function handle()
    {
        $this->Build();
    }

    public function Build()
    {
        $sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\r\n";
        $sitemap1 = "";
        $sitemap2 = "";
        $sitemap3 = "";
        $sitemap4 = "";
        $sitemap5 = "";
        $gets = \Route::getRoutes()->get("GET");
        if($gets)
        {
            $sitemap .="<url>\r\n<loc>".env('APP_URL')."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>1.00</priority>\r\n</url>";
            foreach ($gets as $key => $get)
            {
                $array = explode("/",$get->uri());
                if($array[0] == 'webmin' ||  $array[0] == 'laravel-filemanager' || $array[0] == 'api' || $array[0] == 'captcha' || $array[0] == 'UpdateSiteMap' || $array[0] == null)
                {

                }
                else
                {

                    if(strpos($get->uri(), '{') !== false || strpos($get->uri(), '}') !== false)
                    {

                    }
                    else
                    {
                        switch (count($array)) 
                        {
                            case 1:
                                $sitemap1.="<url>\r\n<loc>".env('APP_URL').$get->uri()."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>0.80</priority>\r\n</url>";
                                break;
                            case 2:
                                $sitemap2.="<url>\r\n<loc>".env('APP_URL').$get->uri()."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>0.64</priority>\r\n</url>";
                                break;
                            case 3:
                                $sitemap3.="<url>\r\n<loc>".env('APP_URL').$get->uri()."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>0.51</priority>\r\n</url>";
                                break;
                            case 4:
                                $sitemap4.="<url>\r\n<loc>".env('APP_URL').$get->uri()."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>0.41</priority>\r\n</url>";
                                break;
                            default:
                                $sitemap5.="<url>\r\n<loc>".env('APP_URL').$get->uri()."</loc>\r\n<lastmod>".date('c')."</lastmod>\r\n<priority>0.32</priority>\r\n</url>";
                                break;
                        }
                    } 
                    
                }
            }
            $sitemap .= $sitemap1.$sitemap2.$sitemap3.$sitemap4.$sitemap5;
        }
        
        $sitemap .= "\r\n</urlset>";
        $this->write($sitemap);
    }

    private function write($sitemap)
    {
        file_put_contents(public_path().'/sitemap.xml', $sitemap);
    }
    
}

?>