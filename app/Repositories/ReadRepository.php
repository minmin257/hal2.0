<?php

namespace App\Repositories;
use Illuminate\Http\Request;
use App\System;
use App\System_en;
use App\Contact;
use App\MailAccount;
use App\InnerBanner;
use App\About;
use App\News;
use App\News_en;
use App\User;
use App\Role;
use App\PermissionType;
use App\Index;
use App\Index_en;
use App\History;
use App\ProductClass;
use App\ProductClass_en;
use App\ProductType;
use App\ProductType_en;
use App\Product;
use App\Product_en;
use App\ProductFile;
use App\Solution;
use App\Solution_en;
use App\Advantage;
use App\Advantage_en;
use App\CompanyInfo;
use App\CompanyInfo_en;
use App\TrackCode;
use App\CompanyPage;
use App\Language;

class ReadRepository
{
    protected $locale;
    public function __construct()
    {
        $this->locale = app()->getLocale();
    }
    public function get_language()
    {
        return Language::first();
    }
    public function get_track_code()
    {
        return TrackCode::first();
    }
    public function get_company_page()
    {
        return CompanyPage::first();
    }
    public function search_advantage_by_url(Request $request)
    {
        $advantage = Advantage::where('url',$request->url)->first();
        $advantage_en = Advantage_en::where('url',$request->url)->first();
        if($advantage || $advantage_en)
        {
            $advantage_id = $advantage->id ?? $advantage_en->id;
            return $advantage_id;
        }
        else
        {
            abort(404);
        }
        
    }
    public function search_solution_by_url(Request $request)
    {
        $solution = Solution::where('url',$request->url)->first();
        $solution_en = Solution_en::where('url',$request->url)->first();
        if($solution || $solution_en)
        {
            $solution_id = $solution->id ?? $solution_en->id;
            return $solution_id;
        }
        else
        {
            abort(404);
        }
        
    }
    public function search_product_by_url(Request $request)
    {
        $type_id = null;

        $class = ProductClass::where('url',$request->class)->first();
        $class_en = ProductClass_en::where('url',$request->class)->first();
        if($class || $class_en)
        {
            $class_id = $class->id ?? $class_en->id;
        }
        else
        {
            abort(404);
        }

        $type = ProductType::where('url',$request->type)->first();
        $type_en = ProductType_en::where('url',$request->type)->first();
        if($request->type)
        {
            if($type || $type_en)
            {
                $type_id = $type->id ?? $type_en->id;
            }
            else
            {
                abort(404);
            }
        }
        
        if($request->product)
        {
            $product = Product::where('url',$request->product)->first();
            $product_en = Product_en::where('url',$request->product)->first(); 
            if($product || $product_en)
            {
                $product_id = $product->id ?? $product_en->id;
            }
            else
            {
                abort(404);
            }
            
            return [
                'class_id' => $class_id,
                'type_id' => $type_id,
                'product_id' => $product_id,
            ];
        }
        else
        {
            return [
                'class_id' => $class_id,
                'type_id' => $type_id,
            ];
        }
 
    }
    public function get_company_info_by_lang($index = false)
    {
        if($index)
        {
            return CompanyInfo::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
        }
        else
        {
            if(session()->has('backend_lang'))
            {
                if(session()->get('backend_lang')=='en')
                {
                    return CompanyInfo_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return CompanyInfo::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
            }
            else
            {
                return CompanyInfo::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        
    }
    public function get_company_info()
    {
        if(session()->has('locale'))
        {
            if(session()->get('locale')=='en')
            {
                return CompanyInfo_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return CompanyInfo::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            
            if($this->locale =='en')
            {
                return CompanyInfo_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return CompanyInfo::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    
    public function get_advantage_zh()
    {
        return Advantage::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
    }
    public function get_advantage($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return Advantage_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return Advantage::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale =='en')
                {
                    return Advantage_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return Advantage::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
            }
            else
            {
                return Advantage_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    
    public function get_solution_zh()
    {
        return Solution::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
    }
    public function get_solution($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return Solution_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return Solution::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale == 'en')
                {
                    return Solution_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return Solution::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
            }
            else
            {
                return Solution_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    public function get_product_file_zh()
    {
        return ProductFile::where('delete',0)->orderBy('sort','desc');
    }
    
    public function get_product_zh(Request $request)
    {
        if($request->type)
        {
            return Product::join('product_types', 'products.product_type_id', '=', 'product_types.id')->orderBy('product_types.product_class_id','desc')->orderBy('products.product_type_id','desc')->orderBy('products.sort','desc')->orderBy('products.created_at','desc')->where('products.delete',0)->where('products.product_type_id',$request->type)->select('products.*');
        }
        else
        {
            return Product::join('product_types', 'products.product_type_id', '=', 'product_types.id')->orderBy('product_types.product_class_id','desc')->orderBy('products.product_type_id','desc')->orderBy('products.sort','desc')->orderBy('products.created_at','desc')->where('products.delete',0)->select('products.*');
        }
    }
    public function get_product_type_zh()
    {
        return ProductType::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
    }
    public function get_product_class_zh()
    {
        return ProductClass::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
    }
    public function get_product($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return Product_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return Product::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {

                if($this->locale=='en')
                {
                    return Product_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return Product::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }

            }
            else
            {
                return Product_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    public function get_product_type($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return ProductType_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return ProductType::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale =='en')
                {
                    return ProductType_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return ProductType::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
            }
            else
            {
                return ProductType_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    public function get_product_class($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return ProductClass_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            else
            {
                return ProductClass::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale =='en')
                {
                    return ProductClass_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
                else
                {
                    return ProductClass::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
                }
            }
            else
            {
                return ProductClass_en::orderBy('sort','desc')->orderBy('created_at','desc')->where('delete',0);
            }
            
        }
    }
    public function get_news($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return News_en::orderBy('sort','desc')->orderBy('date','desc')->where('delete',0);
            }
            else
            {
                return News::orderBy('sort','desc')->orderBy('date','desc')->where('delete',0);
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale =='en')
                {
                    return News_en::orderBy('sort','desc')->orderBy('date','desc')->where('delete',0);
                }
                else
                {
                    return News::orderBy('sort','desc')->orderBy('date','desc')->where('delete',0);
                }
            }
            else
            {
                return News_en::orderBy('sort','desc')->orderBy('date','desc')->where('delete',0);
            }
            
        }
    }
    public function get_history($back = false)
    {
        $history = History::orderBy('year','desc')->where('delete',0);
        if($back)
        {
            return $history;
        }
        else
        {
            if(session()->has('locale'))
            {
                if(session()->get('locale')=='en')
                {
                    return $history->select('year', 'info_en as info');
                }
                else
                {
                    return $history->select('year', 'info');
                }
            }
            else
            {
                
                if($this->locale =='en')
                {
                    return $history->select('year', 'info_en as info');
                }
                else
                {
                    return $history->select('year', 'info');
                }
                
            }
        }
        
    }
    public function get_index($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return Index_en::first();
            }
            else
            {
                return Index::first();
            }
        }
        else
        {
            if($lang == 'locale')
            {

                if($this->locale=='en')
                {
                    return Index_en::first();
                }
                else
                {
                    return Index::first();
                }

            }
            else
            {
                return Index_en::first();
            }
            
        }
    }
    public function get_system_by_lang()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                return System_en::first();
            }
            else
            {
                return System::first();
            }
        }
        else
        {
            return System::first();
        }
        
    }
    public function get_system()
    {
        if(session()->has('locale'))
        {
            if(session()->get('locale')=='en')
            {
                return System_en::first();
            }
            else
            {
                return System::first();
            }
        }
        else
        {
            if($this->locale == 'en')
            {
                return System_en::first();
            }
            else
            {
                return System::first();
            }
        }
        
    }

    public function get_contact()
    {
    	return Contact::where('delete',0)->orderBy('created_at','desc');
    }

    public function get_mail_account()
    {
    	return MailAccount::first();
    }

    public function get_inner_banner()
    {
        return InnerBanner::all();
    }

    public function get_about($lang)
    {
        if(session()->has($lang))
        {
            if(session()->get($lang)=='en')
            {
                return About::select('html_en as html')->first();
            }
            else
            {
                return About::first();
            }
        }
        else
        {
            if($lang == 'locale')
            {
                if($this->locale == 'en')
                {
                    return About::select('html_en as html')->first();
                }
                else
                {
                    return About::first();
                }
            }
            else
            {
                return About::select('html_en as html')->first();
            }
            
        }
        
    }


    public function get_user()
    {
        return User::all()->where('delete',0);
    }

    public function get_role()
    {
        return Role::all();
    }

    public function get_permission_type()
    {
        return PermissionType::all();
    }

}
?>