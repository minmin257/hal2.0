<?php

namespace App\Repositories;
use Illuminate\Http\Request;
use App\Contact;
use App\News;
use App\News_en;
use App\UserHasPermission;
use App\User;
use App\UserHasRole;
use App\History;
use App\ProductClass;
use App\ProductClass_en;
use App\ProductType;
use App\ProductType_en;
use App\Product;
use App\Product_en;
use App\ProductFile;
use App\ProductFile_en;
use App\Solution;
use App\Solution_en;
use App\SolutionPic;
use App\SolutionHasProduct;
use App\Advantage;
use App\Advantage_en;
use App\CompanyInfo;
use App\CompanyInfo_en;

class CreateRepository
{
    public function create_copy_product(Request $request)
    {
        $newProduct = Product::find($request->id)->replicate();
        $newProduct->title = $newProduct->title.'_copy';
        $newProduct->url = $newProduct->url.'_'.strtotime('now');
        $newProduct->save();
        $newProduct_en = Product_en::find($request->id)->replicate();
        $newProduct_en->title = $newProduct->title.'_copy';
        $newProduct_en->url = $newProduct->url.'_'.strtotime('now');
        $newProduct_en->save();
    }
    public function create_company_info(Request $request)
    {
        CompanyInfo::create($request->toArray());
        CompanyInfo_en::create($request->toArray());
    }
    public function create_advantage(Request $request)
    {
        Advantage::create($request->toArray());
        Advantage_en::create([
            'title' => $request->title_en,
            'url' => $request->url_en,
            'sort' => $request->sort,
            'content' => $request->content,
        ]);
    }
    public function create_solution_pic(Request $request)
    {
        SolutionPic::create($request->toArray());
    }
    public function create_solution(Request $request)
    {
        Solution::create($request->toArray());
        Solution_en::create([
            'title' => $request->title_en,
            'url' => $request->url_en,
            'sort' => $request->sort,
            'content' => $request->content,
        ]);
    }
    public function create_file(Request $request)
    {
        ProductFile::create($request->toArray());
        ProductFile_en::create([
            'product_id' => $request->product_id,
            'title' => $request->title_en,
            'href' => $request->href,
            'sort' => $request->sort,
        ]);
    }
    public function create_product(Request $request)
    {
        Product::create([
            'product_type_id' => $request->type,
            'title' => $request->title,
            'url' => $request->url,
            'sort' => $request->sort,
            'state' => $request->state,
            'src' => $request->filepath,
        ]);
        Product_en::create([
            'product_type_id' => $request->type,
            'title' => $request->title_en,
            'url' => $request->url_en,
            'sort' => $request->sort,
            'state' => $request->state,
            'src' => $request->filepath,
        ]);
    }
    public function create_type(Request $request)
    {
        ProductType::create([
            'product_class_id' => $request->class_id,
            'title' => $request->title,
            'url' => $request->url,
            'sort' => $request->sort,
        ]);
        ProductType_en::create([
            'product_class_id' => $request->class_id,
            'title' => $request->title_en,
            'url' => $request->url_en,
            'sort' => $request->sort,
        ]);
    }
    public function create_class(Request $request)
    {
        ProductClass::create([
            'title' => $request->title,
            'url' => $request->url,
            'sort' => $request->sort,
        ]);
        ProductClass_en::create([
            'title' => $request->title_en,
            'url' => $request->url_en,
            'sort' => $request->sort,
        ]);
    }
    public function create_history(Request $request)
    {
        History::create($request->toArray());
    }
    public function create_contact(array $request)
    {
        Contact::create($request);
    }

    public function create_news(array $request)
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                News_en::create($request);
            }
            else
            {
                News::create($request);
            }
        }
        else
        {
            News::create($request);
        }
        
    }

    public function create_permission(Request $request)
    {
        foreach($request['state'] as $key => $value)
        {
            if($value)
            {
                UserHasPermission::create([
                    'user_id'=>$request['id'],
                    'permission_id'=>$request['permission_id'][$key],
                ]);
            }
        }
    }

    public function create_user(Request $request)
    {
        $user = User::create([
            'account'=>$request['account'],
            'password'=>bcrypt($request['password']),
            'name'=>$request['name'],
            'state'=>$request['state'],
        ]);

        UserHasRole::create([
            'user_id'=>$user->id,
            'role_id'=>$request['role'],
        ]);
    }
}
?>