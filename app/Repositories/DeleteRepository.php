<?php

namespace App\Repositories;
use Illuminate\Http\Request;
use App\UserHasPermission;

class DeleteRepository
{
    public function delete_permission(Request $request)
    {
        UserHasPermission::where('user_id',$request['id'])->delete();
    }
    
}
?>