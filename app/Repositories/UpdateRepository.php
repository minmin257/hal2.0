<?php

namespace App\Repositories;
use Illuminate\Http\Request;
use App\User;
use App\System;
use App\System_en;
use App\Contact;
use App\InnerBanner;
use App\About;
use App\News;
use App\News_en;
use App\MailAccount;
use App\UserHasRole;
use App\Index;
use App\Index_en;
use App\History;
use App\ProductClass;
use App\ProductClass_en;
use App\ProductType;
use App\ProductType_en;
use App\Product;
use App\Product_en;
use App\ProductFile;
use App\ProductFile_en;
use App\Solution;
use App\Solution_en;
use App\SolutionPic;
use App\SolutionHasProduct;
use App\Advantage;
use App\Advantage_en;
use App\CompanyInfo;
use App\CompanyInfo_en;
use App\TrackCode;
use App\CompanyPage;
use App\UserHasPermission;
use App\Language;

class UpdateRepository
{
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function update_language()
    {
        Language::first()->update(['name'=>$this->request->language]);
    }
    public function update_company_page()
    {
        CompanyPage::first()->update($this->request->toArray());
    }
    public function update_track_code()
    {
        TrackCode::first()->update($this->request->toArray());
    }
    public function delete_company_info()
    {
        CompanyInfo::find($this->request->id)->update(['delete' => 1]);
        CompanyInfo_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_company_info()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                $company_info = CompanyInfo_en::find($this->request->id);
            }
            else
            {
                $company_info = CompanyInfo::find($this->request->id);
            }
        }
        else
        {
            $company_info = CompanyInfo::find($this->request->id);
        }
        $company_info->update([
            'location' => $this->request->location,
            'name' => $this->request->name,
            'tel' => $this->request->tel,
            'fax' => $this->request->fax,
            'email' => $this->request->email,
            'add' => $this->request->add,
            'add_href' => $this->request->add_href,
            'website' => $this->request->website,
            'map' => $this->request->map,
        ]);
        CompanyInfo::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
        CompanyInfo_en::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
    }
    public function delete_advantage()
    {
        Advantage::find($this->request->id)->update(['delete' => 1]);
        Advantage_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_advantage()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                $advantage = Advantage_en::find($this->request->id);
            }
            else
            {
                $advantage = Advantage::find($this->request->id);
            }
        }
        else
        {
            $advantage = Advantage::find($this->request->id);
        }
        $advantage->update([
            'title' => $this->request->title,
            'url' => $this->request->url,
            'content' => $this->request->content,
        ]);
        Advantage::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
        Advantage_en::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
    }
    public function delete_solution_pic()
    {
        SolutionPic::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_solution_pic()
    {
        foreach ($this->request->pic_src as $key => $src) {
            SolutionPic::find($key)->update([
                'src' => $src,
                'sort' => $this->request->pic_sort[$key],
            ]);
        }
        
    }
    public function delete_solution()
    {
        Solution::find($this->request->id)->update(['delete' => 1]);
        Solution_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_solution()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                $solution = Solution_en::find($this->request->id);
            }
            else
            {
                $solution = Solution::find($this->request->id);
            }
        }
        else
        {
            $solution = Solution::find($this->request->id);
        }
        $solution->update([
            'title' => $this->request->title,
            'url' => $this->request->url,
            'content' => $this->request->content,
        ]);
        Solution::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
        Solution_en::find($this->request->id)->update([
            'sort' => $this->request->sort,
        ]);
        SolutionHasProduct::where('solution_id',$this->request->id)->delete();
        foreach ($this->request->product_id as $key => $id) 
        {
            SolutionHasProduct::create([
                'solution_id' => $this->request->id,
                'product_id' => $id,
            ]);
        }
    }
    public function delete_file()
    {
        ProductFile::find($this->request->id)->update(['delete' => 1]);
        ProductFile_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_file()
    {
        ProductFile::find($this->request->id)->update([
            'title' => $this->request->title,
            'href' => $this->request->href,
            'sort' => $this->request->sort,
        ]);
        ProductFile_en::find($this->request->id)->update([
            'title' => $this->request->title_en,
            'href' => $this->request->href,
            'sort' => $this->request->sort,
        ]);
    }
    public function delete_product()
    {
        Product::find($this->request->id)->update(['delete' => 1]);
        Product_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_product()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                $product = Product_en::find($this->request->id);
            }
            else
            {
                $product = Product::find($this->request->id);
            }
        }
        else
        {
            $product = Product::find($this->request->id);
        }
        $product->update([
            'title' => $this->request->title,
            'subtitle' => $this->request->subtitle,
            'url' => $this->request->url,
            'content' => $this->request->content,
        ]);
        Product_en::find($this->request->id)->update([
            'product_type_id' => $this->request->type,
            'sort' => $this->request->sort,
            'state' => $this->request->state,
            'display_on_index' => $this->request->display,
            'src' => $this->request->filepath,
        ]);
        Product::find($this->request->id)->update([
            'product_type_id' => $this->request->type,
            'sort' => $this->request->sort,
            'state' => $this->request->state,
            'display_on_index' => $this->request->display,
            'src' => $this->request->filepath,
        ]);
    }
    public function delete_type()
    {
        ProductType::find($this->request->id)->update(['delete' => 1]);
        ProductType_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_type()
    {
        ProductType::find($this->request->id)->update([
            'product_class_id' => $this->request->class_id,
            'title' => $this->request->title,
            'url' => $this->request->url,
            'sort' => $this->request->sort,
        ]);
        ProductType_en::find($this->request->id)->update([
            'product_class_id' => $this->request->class_id,
            'title' => $this->request->title_en,
            'url' => $this->request->url_en,
            'sort' => $this->request->sort,
        ]);
    }
    public function delete_class()
    {
        ProductClass::find($this->request->id)->update(['delete' => 1]);
        ProductClass_en::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_class()
    {
        ProductClass::find($this->request->id)->update([
            'title' => $this->request->title,
            'url' => $this->request->url,
            'sort' => $this->request->sort,
        ]);
        ProductClass_en::find($this->request->id)->update([
            'title' => $this->request->title_en,
            'url' => $this->request->url_en,
            'sort' => $this->request->sort,
        ]);
    }
    public function delete_news()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                News_en::find($this->request->id)->update(['delete' => 1]);
            }
            else
            {
                News::find($this->request->id)->update(['delete' => 1]);
            }
        }
        else
        {
            News::find($this->request->id)->update(['delete' => 1]);
        }
    }
    public function delete_history()
    {
        History::find($this->request->id)->update(['delete' => 1]);
    }
    public function update_history()
    {
        History::whereid($this->request->id)
                ->select('year','info','info_en')
                ->update($this->request->toArray());
    }
    public function update_index()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                Index_en::first()->update($this->request->toArray());
            }
            else
            {
                Index::first()->update($this->request->toArray());
            }
        }
        else
        {
            Index::first()->update($this->request->toArray());
        }
    }
    public function update_profile()
    {
        if($this->request->filled('password'))
        {
            if($this->request->has('state'))
            {
                User::where('id',$this->request['id'])->update([
                    'name'=>$this->request['name'],
                    'password'=>bcrypt($this->request['password']),
                    'state'=>$this->request['state'],
                ]);
            }
            else
            {
                User::where('id',$this->request['id'])->update([
                    'name'=>$this->request['name'],
                    'password'=>bcrypt($this->request['password']),
                ]);
            }
        }
        else
        {
            if($this->request->has('state'))
            {
                User::where('id',$this->request['id'])->update([
                    'name'=>$this->request['name'],
                    'state'=>$this->request['state'],
                ]);
            }
            else
            {
                User::where('id',$this->request['id'])->update([
                    'name'=>$this->request['name'],
                ]);
            }
        }

        if($this->request->has('role'))
        {
            $user = User::where('id',$this->request['id'])->first();
            UserHasRole::where('user_id',$user->id)->update([
                'role_id'=>$this->request['role'],
            ]);
        }
    }

    public function update_system()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                System_en::first()->update($this->request->toArray());
            }
            else
            {
                System::first()->update($this->request->toArray());
            }
        }
        else
        {
            System::first()->update($this->request->toArray());
        }
        
    }

    public function update_contact_state()
    {
        Contact::find($this->request['id'])->update([
            'state'=>1,
        ]);
    }

    public function update_contact_reply()
    {
        Contact::find($this->request['id'])->update([
            'state'=>$this->request['state'],
            'reply'=>$this->request['reply'],
        ]);
    }
    
    public function update_contact_delete()
    {
        Contact::find($this->request['id'])->update([
            'delete'=>1,
        ]);
    }

    

    

    public function update_inner_banner()
    {
        InnerBanner::find($this->request['id'])->update([
            'src'=>$this->request['filepath'],
        ]);
    }

    public function delete_inner_banner()
    {
        InnerBanner::find($this->request['id'])->update([
            'src'=>null,
        ]);
    }

    public function update_about()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                About::first()->update([
                    'html_en'=>$this->request['html'],
                ]);
            }
            else
            {
                About::first()->update([
                    'html'=>$this->request['html'],
                ]);
            }
        }
        else
        {
            About::first()->update([
                'html'=>$this->request['html'],
            ]);
        }
        
    }

    public function update_news()
    {
        if(session()->has('backend_lang'))
        {
            if(session()->get('backend_lang')=='en')
            {
                News_en::find($this->request['id'])->update([
                    'date'=>$this->request['date'],
                    'sort'=>$this->request['sort'],
                    'title'=>$this->request['title'],
                    'content'=>$this->request['content'],
                ]);
            }
            else
            {
                News::find($this->request['id'])->update([
                    'date'=>$this->request['date'],
                    'sort'=>$this->request['sort'],
                    'title'=>$this->request['title'],
                    'content'=>$this->request['content'],
                ]);
            }
        }
        else
        {
            News::find($this->request['id'])->update([
                'date'=>$this->request['date'],
                'sort'=>$this->request['sort'],
                'title'=>$this->request['title'],
                'content'=>$this->request['content'],
            ]);
        }
        
    }

    public function delete_account()
    {
        UserHasRole::where('user_id',$this->request['id'])->delete();
        UserHasPermission::where('user_id',$this->request['id'])->delete();
        User::find($this->request['id'])->delete();

    }

    public function update_mail_account()
    {
        MailAccount::first()->update([
            'email'=>$this->request['email'],
            'password'=>encrypt($this->request['password']),
        ]);
    }

    public function update_mail_name()
    {
        MailAccount::first()->update([
            'name'=>$this->request['name'],
            'subject'=>$this->request['subject'],
            'subject_en'=>$this->request['subject_en'],
            'recipient'=>$this->request['recipient'],
            'headline'=>$this->request['headline'],
            'signature'=>$this->request['signature'],
        ]);
    }
}
?>