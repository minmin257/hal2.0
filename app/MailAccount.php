<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailAccount extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'subject', 'recipient','headline','signature','subject_en'
    ];
}
