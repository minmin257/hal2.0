<?php

namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;
use App\Mail\CheckMailAccount;
use App\Mail\ReplyToCustomer;
use App\Mail\ToCustomer;
use App\Mail\ToSystem;

class MailService
{
    public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
    }
    /**
     * 發送Email
     * @param array $request
     */
    public function CheckMailAccount(array $request)
    {
        try {
            $mail_account = $this->read->get_mail_account();
            \Config::set('mail.username',$request['email']);
            \Config::set('mail.password',$request['password']);
            \Config::set('app.name',$mail_account->headline);
            
            Mail::to($request['email'])->queue(new CheckMailAccount($request));
            // 發信成功才會更新
            $this->update->update_mail_account($request);
        } catch (\Exception $e) {
            info($e);
            throw new \Exception('郵件發送失敗，請檢查輸入信箱或密碼是否正確');
        }
        
    }

    public function Reply(array $request)
    {
        try {
            $mail_account = $this->read->get_mail_account();
            \Config::set('mail.username',$mail_account->email);
            \Config::set('mail.password',decrypt($mail_account->password));
            \Config::set('app.name',$mail_account->headline);

            Mail::to($request['email'])->queue(new ReplyToCustomer($request));
            // 發信成功才會更新
            $this->update->update_contact_reply($request);
        } catch (\Exception $e) {
            throw new \Exception('郵件發送失敗，請檢查輸入信箱或密碼是否正確');
        }
        
    }

    public function send(array $request)
    {
        try {
            $mail_account = $this->read->get_mail_account();
            $contact = $this->read->get_contact()->orderBy('id','desc')->first();
            \Config::set('mail.username',$mail_account->email);
            \Config::set('mail.password',decrypt($mail_account->password));
            \Config::set('app.name',$mail_account->headline);

            Mail::to($request['email'])->queue(new ToCustomer($contact));
            Mail::to($mail_account->email)->queue(new ToSystem($contact));
        } catch (\Exception $e) {
            info($e);
            // 發信失敗刪除該筆聯絡
            $contact = $this->read->get_contact()->orderBy('id','desc')->first()->delete();
            throw new \Exception('郵件發送失敗');
        }
        
    }
}

?>