<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailAccount;

class CheckMailAccount extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_account = MailAccount::first();
        $email = $mail_account->email;
        return $this->from($this->request['email'], '設定通知')
                ->subject('信箱設定成功通知')
                ->cc($mail_account->recipient)
                ->markdown('mails.check_mail_account');
    }
}
