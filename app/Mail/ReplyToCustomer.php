<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailAccount;

class ReplyToCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_account = MailAccount::first();
        $email = $mail_account->email;
        $name = $mail_account->name;
        $subject = $mail_account->subject;
        return $this->from($email,$name)
                ->subject($subject)
                ->markdown('mails.customer');

    }
}
