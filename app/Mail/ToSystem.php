<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailAccount;

class ToSystem extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contact;

    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_account = MailAccount::first();
        $email = $mail_account->email;
        $name = $mail_account->name;
        $recipients = explode(',',$mail_account->recipient);
        return $this->from($email,$name)
                ->subject('網站收到一則新詢問')
                ->cc($recipients)
                ->markdown('mails.system');   

    }
}
