<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\MailAccount;

class ToCustomer extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $contact;

    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_account = MailAccount::first();
        $email = $mail_account->email;
        $name = $mail_account->name;
        $subject = $mail_account->subject;
        $subject_en = $mail_account->subject_en;
        if(session()->has('locale'))
        {
            if(session()->get('locale')=='en')
            {
                return $this->from($email,$name)
                    ->subject($subject_en)
                    ->markdown('mails.customer_en');
            }
            else
            {
                return $this->from($email,$name)
                    ->subject($subject)
                    ->markdown('mails.customer');
            }
        }
        else
        {
            return $this->from($email,$name)
                ->subject($subject_en)
                ->markdown('mails.customer_en');
        }
        

    }
}
