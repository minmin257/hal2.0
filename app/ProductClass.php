<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductClass extends Model
{
    protected $fillable = [
    	'title','url','sort','delete'
    ];
    
    public function type()
    {
    	return $this->hasMany(ProductType::class,'product_class_id','id')->where('delete',0)->orderBy('sort','desc');
    }

    public function en()
    {
    	return $this->hasOne(ProductClass_en::class,'id','id')->where('delete',0);
    }
}
