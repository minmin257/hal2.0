<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasPermission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'permission_id'
    ];

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }
}
