<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHasRole extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'role_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}
