<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFile extends Model
{
    protected $fillable = [
    	'title','href','sort','delete','product_id'
    ];
    
    public function product()
    {
    	return $this->belongsTo(Product::class ,'product_id','id');
    }

    public function en()
    {
    	return $this->hasOne(ProductFile_en::class,'id','id')->where('delete',0);
    }
}
