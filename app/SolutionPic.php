<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolutionPic extends Model
{
    protected $fillable = [
    	'solution_id','src','delete','sort'
    ];
}
