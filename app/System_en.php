<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System_en extends Model
{
    protected $fillable = [
        'sitename', 'description','keywords', 'logo', 'copy_right', 'maintain', 'message','cookie'
    ];
}
