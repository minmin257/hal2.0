<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
    protected $fillable = [
    	'title','url','sort','delete','product_class_id'
    ];
    
    public function class()
    {
    	return $this->belongsTo(ProductClass::class ,'product_class_id','id');
    }

    public function product()
    {
    	return $this->hasMany(Product::class,'product_type_id','id')->where('delete',0)->orderBy('sort','desc')->where('state',1);
    }

    public function en()
    {
        return $this->hasOne(ProductType_en::class,'id','id')->where('delete',0);
    }
}
