<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompanyInfo_en extends Model
{
    protected $fillable = [
    	'location','name','tel','email','fax','add','add_href','map','website','sort','delete'
    ];
}
