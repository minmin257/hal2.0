<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'account', 'password', 'state', 'delete'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user_has_role()
    {
        return $this->hasOne(UserHasRole::class);
    }

    public function user_permissions()
    {
        return $this->hasMany(UserHasPermission::class);
    }

    public function hasPermission(string $permission)
    {
        $permissions_id = $this->user_permissions()->pluck('permission_id');

        if(Permission::whereIn('id',$permissions_id)->where('name',$permission)->get()->count()>0)
        {
           return true; 
        }
        else
        {
            return false;
        }
    }

}
