<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sitename', 'description','keywords', 'logo', 'copy_right', 'maintain', 'message','cookie'
    ];
}
