<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode as Middleware;
use Closure;
use App\Repositories\ReadRepository;
// use Illuminate\Http\Response;

class CheckForMaintenanceMode extends Middleware
{
    public function __construct(ReadRepository $ReadRepository)
    {
        $this->read = $ReadRepository;
    }

    /**
     * The URIs that should be reachable while maintenance mode is enabled.
     *
     * @var array
     */
    public function handle($request, Closure $next)
    {
        $system = $this->read->get_system();
    	if($system->maintain)
    	{
    		abort(503,$system->message);
    	}

    	return $next($request);
    }
    
}
