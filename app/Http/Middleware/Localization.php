<?php

namespace App\Http\Middleware;

use Closure;
use App\Language;

class Localization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = Language::first();
        $langArr = array("zh-TW","zh-CN","zh","zh-HK");

        if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
            $languages = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
        } else {
            $languages[0] = "en";
        }


        if ( \Session::has('locale')) {
            \App::setLocale(\Session::get('locale'));
        }else{
            switch ($lang->name) {

                case 'en':
                    \App::setLocale('en');
                    break;
                case 'zh-CN':
                    \App::setLocale('zh_cn');
                    break;
                default:
                    if (in_array($languages[0], $langArr))
                    {
                        \App::setLocale('zh_cn');
                    }
                    else
                    {
                        \App::setLocale("en");
                    }
                    break;
            }
            
        }
 
        return $next($request);
    }
}
