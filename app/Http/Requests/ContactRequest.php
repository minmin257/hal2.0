<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required','regex:/[\x{4e00}-\x{9fa5}a-zA-Z]$/u'],
            'tel' => ['required'],
            'email' => ['required','email'],
            'company' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'content' => ['required'],
            'captcha' => ['required','captcha'],
        ];
    }

}
