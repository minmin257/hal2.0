<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class SolutionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['sometimes','required','regex:/^[^\$#@%=?*]*$/'],
            'url' => ['sometimes','required','regex:/^[^\$#@%=?*\/\s]*$/','unique:solutions,url,'.$this->id],
            'title_en' => ['sometimes','required','regex:/^[^\$#@%=?*]*$/'],
            'url_en' => ['sometimes','required','regex:/^[^\$#@%=?*\/\s]*$/','unique:solution_ens,url,'.$this->id],
            'sort' => ['sometimes','required','numeric'],
            'id' => ['sometimes','required','exists:solutions,id'],
            'content' => ['sometimes','required'],
            'src' => ['sometimes','required'],
            'pic_src.*' => ['sometimes','required'],
            'pic_sort.*' => ['sometimes','required','numeric'],
        ];
    }
    public function messages()
    {
        return [
            'src.required' => '圖片 不能留空。',
            'src.required' => '圖片 不能留空。',
            'pic_src.*.required' => '圖片 不能留空。',
            'pic_sort.*.required' => '順序 不能留空。',
            'pic_sort.*.numeric' => '順序 必須為一個數字。',
        ];
    }
}
