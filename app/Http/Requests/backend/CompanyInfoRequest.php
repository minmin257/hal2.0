<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CompanyInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'location' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'name' => ['required','regex:/^[^\$#@%=?*]*$/',],
            'tel' => ['sometimes','nullable','regex:/[\d\w\-+()#]$/u'],
            'fax' => ['sometimes','nullable','regex:/[\d\w\-+()#]$/u'],
            'email' => ['sometimes','nullable','email'],
            'sort' => ['sometimes','required','numeric'],
            'add' => ['sometimes','required','regex:/^[^\$#@%=?*]*$/'],
            'add_href' => ['sometimes','nullable','active_url'],
            'map' => ['sometimes','required'],
            'website' => ['sometimes','nullable','active_url'],
            
        ];
    }
    public function messages()
    {
        return [
            'location.required' => '地點 不能留空。',
            'location.regex' => '地點 的格式錯誤。',
            'name.required' => '公司名稱 不能留空。',
            'name.regex' => '公司名稱 的格式錯誤。',
            'fax.regex' => '傳真 的格式錯誤。',
            'add.required' => '地址 不能留空。',
            'add.regex' => '地址 的格式錯誤。',
            'add_href.active_url' => '地址連結 並非一個有效的網址。',
            'website.active_url' => '網站 並非一個有效的網址。',
            'map.required' => 'google map 不能留空。',
        ];
    }
}
