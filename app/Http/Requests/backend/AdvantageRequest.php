<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class AdvantageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'url' => ['required','regex:/^[^\$#@%=?*\/\s]*$/','unique:advantages,url,'.$this->id],
            'title_en' => ['sometimes','required','regex:/^[^\$#@%=?*]*$/'],
            'url_en' => ['sometimes','required','regex:/^[^\$#@%=?*\/\s]*$/','unique:advantage_ens,url,'.$this->id],
            'sort' => ['sometimes','required','numeric'],
            'id' => ['sometimes','required','exists:advantages,id'],
            'content' => ['sometimes','required'],
            
        ];
    }

}
