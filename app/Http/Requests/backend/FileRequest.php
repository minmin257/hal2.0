<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class FileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'title_en' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'sort' => ['required','numeric'],
            'id' => ['sometimes','required','exists:product_files,id'],
            'product_id' => ['sometimes','required','exists:products,id'],
        ];
    }
    
}
