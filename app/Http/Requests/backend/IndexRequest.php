<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banner' => ['required'],
            'href' => ['nullable','active_url'],
            'slogan_src' => ['required'],
            'slogan' => ['required'],
            'company_src' => ['required'],
            'company' => ['required'],
            'application_src' => ['required'],
        ];
    }
    public function messages()
    {
        return [
            'banner.required' => 'Banner 不能留空。',
            'href.active_url' => 'Banner連結 格式錯誤。',
            'slogan_src.required' => 'Slogan底圖 不能留空。',
            'slogan.required' => 'Slogan 不能留空。',
            'company_src.required' => '公司底圖 不能留空。',
            'company.required' => '公司 不能留空。',
            'application_src.required' => '應用 不能留空。',
        ];
    }
}
