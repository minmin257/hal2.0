<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class SystemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sitename' => ['required','regex:/[\x{4e00}-\x{9fa5}a-zA-Z]$/u'],
            'logo' => ['required'],
            'copy_right' => ['required','regex:/^[^\$#%=?*]*$/'],
            'description' => ['required','regex:/^[^\$#%=?*]*$/'],
            'keywords' => ['required','regex:/^[^\$#%=?*]*$/'],
            'maintain' => ['required','numeric'],
            'message' => ['required'],
        ];
    }
}
