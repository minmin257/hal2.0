<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class HistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year' => ['required','numeric','min:1'],
            'info' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'info_en' => ['required','regex:/^[^\$#@%=?*]*$/'],
        ];
    }
    public function messages()
    {
        return [
            'year.required' => '年份 不能留空。',
            'year.numeric' => '年份 必須為一個數字。',
            'year.min' => '年份 必須大於0。',
            'info.required' => '事蹟 不能留空。',
            'info.regex' => '事蹟 的格式錯誤。',
            'info_en.required' => '事蹟(英) 不能留空。',
            'info_en.regex' => '事蹟 的格式錯誤。',
        ];
    }
}
