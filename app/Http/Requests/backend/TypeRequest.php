<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class TypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'url' => ['required','regex:/^[^\$#@%=?*\/\s]*$/','unique:product_types,url,'.$this->id],
            'title_en' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'url_en' => ['required','regex:/^[^\$#@%=?*\/\s]*$/','unique:product_type_ens,url,'.$this->id],
            'sort' => ['required','numeric'],
            'id' => ['sometimes','required','exists:product_types,id'],
            'class_id' => ['sometimes','required','exists:product_classes,id'],
        ];
    }
    
}
