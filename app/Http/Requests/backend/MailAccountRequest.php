<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class MailAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['sometimes','required','email'],
            'password' => ['sometimes','required'],
            'name' => ['sometimes','required'],
            'subject' => ['sometimes','required'],
            'recipient' => ['sometimes','required','regex:/^([^\s,%$#!~+=*]+@[^\s,%$#!~+=*]+\.[^\s,%$#!~+=*]+\s?,)*([^\s,%$#!~+=*]+@[^\s,%$#!~+=*]+\.[^\s,%$#!~+=*]+)$/'],
        ];
    }
}
