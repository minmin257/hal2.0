<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class NewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => ['required','date'],
            'title' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'content' => ['required'],
            'sort' => ['required','numeric'],
            'id' => ['sometimes','required','exists:news,id']
        ];
    }
}
