<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'account' => ['required','unique:users','between:4,10'],
            'password' => ['required','between:4,10'],
            'name' => ['required','regex:/[\x{4e00}-\x{9fa5}a-zA-Z]$/u'],
            'state' => ['required','numeric'],
            'role' => ['required','numeric','exists:roles,id'],
        ];
    }
}
