<?php

namespace App\Http\Requests\backend;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'class' => ['required','exists:product_classes,id'],
            'type' => ['required','exists:product_types,id'],
            'title' => ['required','regex:/^[^\$#@%=?*]*$/'],
            'url' => ['required','regex:/^[^\$#@%=?*\/\s]*$/','unique:products,url,'.$this->id],
            'title_en' => ['sometimes','required','regex:/^[^\$#@%=?*]*$/'],
            'url_en' => ['sometimes','required','regex:/^[^\$#@%=?*\/\s]*$/','unique:products,url,'.$this->id],
            'sort' => ['required','numeric'],
            'id' => ['sometimes','required','exists:products,id'],
            'state' => ['sometimes','required','boolean'],
            'display' => ['sometimes','required','boolean'],
            'filepath' => ['sometimes','required'],
            'content' => ['sometimes','required'],
        ];
    }
    public function messages()
    {
        return [
            'class.required' => '分類 不能留空。',
            'type.required' => '小分類 不能留空。',
            'content.required' => '概要 不能留空。',
        ];
    }
}
