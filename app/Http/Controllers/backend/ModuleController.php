<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ModuleController extends Controller
{
    public function index()
    {
    	return view('backend.module.index');
    }
    public function set_lang(Request $request)
    {
        
        switch($request->lang){
            case 'en':
                session()->put('backend_lang', 'en');
                break;
            case 'zh_cn':
                session()->put('backend_lang', 'cn');
                break;
            default:
                session()->put('backend_lang', 'cn');
                break;
        }
        
        return redirect()->back(); 

    }
}
