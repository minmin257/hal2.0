<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\UpdateRepository;
use App\Repositories\ReadRepository;
use App\Repositories\DeleteRepository;
use App\Repositories\CreateRepository;
use App\Http\Requests\backend\ProfileRequest;
use App\Http\Requests\backend\PermissionRequest;
use App\Http\Requests\backend\CreateUserRequest;

class AuthorityController extends Controller
{
	public function __construct(UpdateRepository $UpdateRepository,ReadRepository $ReadRepository,CreateRepository $CreateRepository,DeleteRepository $DeleteRepository)
    {
        $this->middleware('HasPermission:authority read', ['only' => ['management']]);
        $this->update = $UpdateRepository;
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->delete = $DeleteRepository;
    }

    public function index()
    {
        return view('backend.authority.index');
    }

    public function update(ProfileRequest $request)
    {
    	$this->update->update_profile($request);
    	return back();
    }

    public function management()
    {
        $users = $this->read->get_user()->whereNotIn('id',[1]);
        $roles = $this->read->get_role();
        return view('backend.authority.management',compact('users','roles'));
    }

    public function create(CreateUserRequest $request)
    {
        $this->create->create_user($request);
    }

    public function edit(Request $request)
    {
        $user = $this->read->get_user()->where('id',$request['id'])->first();
        $roles = $this->read->get_role();
        return view('backend.authority.edit',compact('user','roles'));
    }

    public function delete_account(Request $request)
    {
        $this->update->delete_account($request);
    }

    public function permission(Request $request)
    {
        $user = $this->read->get_user()->where('id',$request['id'])->first();
        $permission_types = $this->read->get_permission_type();
        return view('backend.authority.permission',compact('user','permission_types'));
    }

    public function create_permission(PermissionRequest $request)
    {
        $this->delete->delete_permission($request);
        $this->create->create_permission($request);
        return back();
    }
}
