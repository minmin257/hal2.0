<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\CompanyInfoRequest;

class CompanyInfoController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
    	$company_infos = $this->read->get_company_info_by_lang(true)->get();
    	return view('backend.content.company_info.index',compact('company_infos'));
    }

    public function create(CompanyInfoRequest $request)
    {
    	return $this->create->create_company_info($request);
    }

    public function edit(Request $request)
    {
    	$company_info = $this->read->get_company_info_by_lang()->findOrFail($request->id);
    	return view('backend.content.company_info.edit',compact('company_info'));
    }

    public function update(CompanyInfoRequest $request)
    {
    	return $this->update->update_company_info($request);
    }
    public function delete(Request $request)
    {
    	return $this->update->delete_company_info($request);
    }
}
