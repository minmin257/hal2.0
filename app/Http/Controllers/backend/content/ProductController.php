<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\ProductRequest;

class ProductController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index(Request $request)
    {
    	if(preg_match('/page/', url()->full()))
        {
            session()->put('previous',url()->full());
        }
        else
        {
            session()->forget('previous');
        }
    	$classes = $this->read->get_product_class_zh()->get();
    	$products = $this->read->get_product_zh($request)->paginate(10);
    	return view('backend.content.product.index',compact('classes','products'));
    }

    public function create(ProductRequest $request)
    {
    	return $this->create->create_product($request);
    }

    public function edit(Request $request)
    {
    	$classes = $this->read->get_product_class('backend_lang')->get();
    	$types = $this->read->get_product_type('backend_lang')->get();
    	$product = $this->read->get_product('backend_lang')->findOrFail($request->id);
    	return view('backend.content.product.edit',compact('product','classes','types'));
    }

    public function update(ProductRequest $request)
    {
    	return $this->update->update_product($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_product($request);
    }
    public function show_type(Request $request)
    {
    	$type = $this->read->get_product_type_zh()->where('product_class_id',$request->id)->select('id','title')->get();
    	if($type)
        {
            return response()->json([
                'success'=>true,
                'type' => $type
            ],200);
        }
    }
    public function copy_product(Request $request)
    {
        return $this->create->create_copy_product($request);
    }
}
