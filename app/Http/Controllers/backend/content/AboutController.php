<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\AboutRequest;

class AboutController extends Controller
{
	public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
    	$about = $this->read->get_about('backend_lang');
    	return view('backend.content.about.index',compact('about'));
    }

    public function update(AboutRequest $request)
    {
    	$this->update->update_about($request);
    	return back();
    }

    public function company_page(Request $request)
    {
        $page = $this->read->get_company_page();
        return view('backend.content.company_page',compact('page'));
    }

    public function update_page(Request $request)
    {
        $this->update->update_company_page($request);
        return back();
    }
}
