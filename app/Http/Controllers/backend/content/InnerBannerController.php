<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;

class InnerBannerController extends Controller
{
	public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
    	$innerbanners = $this->read->get_inner_banner();
        return view('backend.content.innerbanner.index',compact('innerbanners'));
    }

    public function update(Request $request)
    {
    	$this->update->update_inner_banner($request);
    }

    public function delete_inner_banner(Request $request)
    {
    	$this->update->delete_inner_banner($request);
    }
}
