<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\AdvantageRequest;

class AdvantageController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
    	if(preg_match('/page/', url()->full()))
        {
            session()->put('previous',url()->full());
        }
        else
        {
            session()->forget('previous');
        }
    	$advantages = $this->read->get_advantage_zh()->paginate(10);
    	return view('backend.content.advantage.index',compact('advantages'));
    }

    public function create(AdvantageRequest $request)
    {
    	return $this->create->create_advantage($request);
    }

    public function edit(Request $request)
    {
    	$advantage = $this->read->get_advantage('backend_lang')->findOrFail($request->id);
    	return view('backend.content.advantage.edit',compact('advantage'));
    }

    public function update(AdvantageRequest $request)
    {
    	return $this->update->update_advantage($request);
    }
    public function delete(Request $request)
    {
    	return $this->update->delete_advantage($request);
    }
}
