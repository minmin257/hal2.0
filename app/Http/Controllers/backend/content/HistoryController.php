<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\HistoryRequest;

class HistoryController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
    	$content = $this->read->get_history(true)->paginate(10);
    	return view('backend.content.history.index',compact('content'));
    }

    public function create(HistoryRequest $request)
    {
    	return $this->create->create_history($request);
    }

    public function edit(Request $request)
    {
    	return $this->read->get_history(true)->whereid($request->id)->first();
    }

    public function update(HistoryRequest $request)
    {
    	return $this->update->update_history($request);
    }
    public function delete(Request $request)
    {
    	return $this->update->delete_history($request);
    }
}
