<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\IndexRequest;

class IndexController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
    }
    public function index()
    {
    	$content = $this->read->get_index('backend_lang');
    	return view('backend.content.home.index',compact('content'));
    }
    public function update(IndexRequest $request)
    {
    	return $this->update->update_index($request);
    }
}
