<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\SolutionRequest;

class SolutionController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index(Request $request)
    {
    	if(preg_match('/page/', url()->full()))
        {
            session()->put('previous',url()->full());
        }
        else
        {
            session()->forget('previous');
        }
    	$solutions = $this->read->get_solution_zh()->paginate(10);
    	return view('backend.content.solution.index',compact('solutions'));
    }

    public function create(SolutionRequest $request)
    {
    	return $this->create->create_solution($request);
    }

    public function edit(Request $request)
    {
    	$solution = $this->read->get_solution('backend_lang')->findOrFail($request->id);
    	$products = $this->read->get_product('backend_lang')->get();
    	return view('backend.content.solution.edit',compact('solution','products'));
    }

    public function update(SolutionRequest $request)
    {
    	return $this->update->update_solution($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_solution($request);
    }
    public function pic(Request $request)
    {
    	$solution = $this->read->get_solution_zh()->findOrFail($request->id);
    	return view('backend.content.solution.pic',compact('solution'));
    }
    public function create_pic(SolutionRequest $request)
    {
    	return $this->create->create_solution_pic($request);
    }
    public function update_pic(SolutionRequest $request)
    {
    	$this->update->update_solution_pic($request);
        return back();
    }
    public function delete_pic(Request $request)
    {
        return $this->update->delete_solution_pic($request);
    }
}
