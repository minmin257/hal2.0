<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\ClassRequest;

class ProductClassController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index()
    {
        if(preg_match('/page/', url()->full()))
        {
            session()->put('previous',url()->full());
        }
        else
        {
            session()->forget('previous');
        }
        
    	$classes = $this->read->get_product_class_zh()->paginate(10);
    	return view('backend.content.product_class.index',compact('classes'));
    }

    public function create(ClassRequest $request)
    {
    	return $this->create->create_class($request);
    }

    public function edit(Request $request)
    {
    	$class = $this->read->get_product_class_zh()->findOrFail($request->id);
    	return view('backend.content.product_class.edit',compact('class'));
    }

    public function update(ClassRequest $request)
    {
    	return $this->update->update_class($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_class($request);
    }
}
