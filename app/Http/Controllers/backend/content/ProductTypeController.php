<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\TypeRequest;

class ProductTypeController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index(Request $request)
    {
        if(preg_match('/page/', url()->full()))
        {
            session()->put('previous',url()->full());
        }
        else
        {
            session()->forget('previous');
        }
    	$classes = $this->read->get_product_class_zh()->get();
    	$select_class = $this->read->get_product_class_zh()->findOrFail($request->id);
    	$types = $this->read->get_product_type_zh()->where('product_class_id',$request->id)->paginate(10);
    	return view('backend.content.product_type.index',compact('classes','select_class','types'));
    }

    public function create(TypeRequest $request)
    {
    	return $this->create->create_type($request);
    }

    public function edit(Request $request)
    {
    	$classes = $this->read->get_product_class_zh()->get();
    	$type = $this->read->get_product_type_zh()->findOrFail($request->id);
    	return view('backend.content.product_type.edit',compact('type','classes'));
    }

    public function update(TypeRequest $request)
    {
    	return $this->update->update_type($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_type($request);
    }
}
