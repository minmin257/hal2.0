<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\FileRequest;

class ProductFileController extends Controller
{
    public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index(Request $request)
    {
    	$product = $this->read->get_product_zh($request)->where('products.id',$request->id)->first();
    	return view('backend.content.product.file',compact('product'));
    }

    public function create(FileRequest $request)
    {
    	return $this->create->create_file($request);
    }

    public function edit(Request $request)
    {
    	$file = $this->read->get_product_file_zh()->findOrFail($request->id);
    	return view('backend.content.product.edit_file',compact('file'));
    }

    public function update(FileRequest $request)
    {
    	return $this->update->update_file($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_file($request);
    }
    
}
