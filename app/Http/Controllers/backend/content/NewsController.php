<?php

namespace App\Http\Controllers\backend\content;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\CreateRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\NewsRequest;

class NewsController extends Controller
{
	public function __construct(ReadRepository $ReadRepository,CreateRepository $CreateRepository,UpdateRepository $UpdateRepository)
    {
        $this->read = $ReadRepository;
        $this->create = $CreateRepository;
        $this->update = $UpdateRepository;
    }

    public function index(Request $request)
    {
        if($request->lang == 'en')
        {
            session()->put('backend_lang', 'en');
        }
        else
        {
            session()->put('backend_lang', 'zh_cn');
        }
    	$news = $this->read->get_news('backend_lang')->paginate(10);
    	return view('backend.content.news.index',compact('news'));
    }

    public function create(NewsRequest $request)
    {
    	return $this->create->create_news($request->all());
    }

    public function edit(Request $request)
    {
    	$news = $this->read->get_news('backend_lang')->where('id',$request['id'])->firstOrFail();
    	return view('backend.content.news.edit',compact('news'));
    }

    public function update(NewsRequest $request)
    {
    	return $this->update->update_news($request);
    }
    public function delete(Request $request)
    {
        return $this->update->delete_news($request);
    }
}
