<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\SystemRequest;
use App\Http\Requests\backend\MailAccountRequest;
use App\Services\MailService;

class SystemController extends Controller
{
	public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository,MailService $MailService)
    {
        $this->middleware('HasPermission:system read', ['only' => ['index']]);
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
        $this->MailService = $MailService;
    }

    public function index()
    {
    	$system = $this->read->get_system_by_lang();
        $lang = $this->read->get_language();
    	return view('backend.system.index',compact('system','lang'));
    }

    public function update(SystemRequest $request)
    {
    	$this->update->update_system($request);
        $this->update->update_language($request);
    }

    public function mail_setting()
    {
        $mail_account = $this->read->get_mail_account();
        return view('backend.system.mail_setting',compact('mail_account'));
    }

    public function update_mail_account(MailAccountRequest $request)
    {
        $this->MailService->CheckMailAccount($request->all());
    }
    public function update_mail(MailAccountRequest $request)
    {
        $this->update->update_mail_name($request);
    }
    public function track_code()
    {
        $code = $this->read->get_track_code();
        return view('backend.system.track_code',compact('code'));
    }
    public function update_track_code(Request $request)
    {
        $this->update->update_track_code($request);
    }
    
}
