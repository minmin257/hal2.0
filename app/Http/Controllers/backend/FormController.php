<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ReadRepository;
use App\Repositories\UpdateRepository;
use App\Http\Requests\backend\ReplyRequest;
use App\Services\MailService;

class FormController extends Controller
{
	public function __construct(ReadRepository $ReadRepository,UpdateRepository $UpdateRepository,MailService $MailService)
    {
        $this->read = $ReadRepository;
        $this->update = $UpdateRepository;
        $this->MailService = $MailService;
    }

    public function index()
    {
    	$contacts = $this->read->get_contact()->get();
        return view('backend.form.index',compact('contacts'));
    }

    public function form_no_read()
    {
    	$contacts = $this->read->get_contact()->where('state',0)->get();
        return view('backend.form.index',compact('contacts'));
    }

    public function form_no_reply()
    {
    	$contacts = $this->read->get_contact()->where('reply',0)->get();
        return view('backend.form.index',compact('contacts'));
    }

    public function form_replied()
    {
    	$contacts = $this->read->get_contact()->where('reply',1)->get();
        return view('backend.form.index',compact('contacts'));
    }

    public function filter(Request $request)
    {
    	$contacts = $this->read->get_contact()->get();
    	if($request->filled('search'))
        {
            $contacts = $this->read->get_contact()
            ->where(function($query) use ($request)
            {
               $query->where('company','like','%'.$request['search'].'%')
                     ->orWhere('name','like','%'.$request['search'].'%');
            })->get();
        }
        return view('backend.form.index',compact('contacts'));
    }

    public function detail(Request $request)
    {
    	$this->update->update_contact_state($request);
        $contact = $this->read->get_contact()->where('id',$request['id'])->first();
        return view('backend.form.detail',compact('contact'));
    }

    public function reply(ReplyRequest $request)
    {
    	$this->update->update_contact_reply($request);
    }

    public function delete_contact(Request $request)
    {
    	$this->update->update_contact_delete($request);
    }
}
