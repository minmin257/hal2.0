<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ReadRepository;

class FrontendController extends Controller
{
    public function __construct(ReadRepository $ReadRepository)
    {
        $this->read = $ReadRepository;
    }
    public function index()
    {
    	$content = $this->read->get_index('locale');
    	$products = $this->read->get_product('locale')->where('display_on_index',1)->get()->take(6);
        $solutions = $this->read->get_solution('locale')->get()->take(4);
    	return view('frontend.index',compact('content','products','solutions'));
    }
    public function company(Request $request)
    {
    	$banner = $this->read->get_inner_banner()->where('id',1)->first();
        $page = $this->read->get_company_page();
        switch ($request->url) {
            case 'profile':
                ($page->profile == 1) ? '':abort(404);
                $content = $this->read->get_about('locale');
                break;
            case 'history':
                ($page->history == 1) ? '':abort(404);
                $content = $this->read->get_history()->get();
                break;
            case 'news':
                ($page->news == 1) ? '':abort(404);
                $content = $this->read->get_news('locale')->paginate(10);
                break;

            default:
                abort(404);
                break;
        }
    	return view('frontend.'.$request->url,compact('banner','content','page'));
    }
    	
    public function news(Request $request)
    {
        $page = $this->read->get_company_page();
        ($page->news == 1) ? '':abort(404);
    	$banner = $this->read->get_inner_banner()->where('id',1)->first();
    	$news = $this->read->get_news('locale')->findOrFail($request->id);
    	return view('frontend.news_detail',compact('banner','news','page'));
    }

    public function product(Request $request)
    {
    	$banner = $this->read->get_inner_banner()->where('id',2)->first();
    	$classes = $this->read->get_product_class('locale')->get();
        $url = $this->read->search_product_by_url($request);
    	$select_class = $this->read->get_product_class('locale')->whereid($url['class_id'])->firstOrFail();
    	$select_type = $this->read->get_product_type('locale')->whereid($url['type_id'])->first();
    	if($request->product)
    	{
    		$product = $this->read->get_product('locale')->whereid($url['product_id'])->where('state',1)->firstOrFail();
    		return view('frontend.product_detail',compact('banner','classes','product'));
    	}
    	return view('frontend.product',compact('banner','classes','select_class','select_type'));
    }
    public function solution(Request $request)
    {
        $banner = $this->read->get_inner_banner()->where('id',3)->first();
        $solutions = $this->read->get_solution('locale')->get();
        $url = $this->read->search_solution_by_url($request);
        $select = $this->read->get_solution('locale')->whereid($url)->firstOrFail();
        $products = $this->read->get_product('locale')->whereIn('id',$select->product->pluck('product_id'))->get();
        return view('frontend.solution',compact('banner','solutions','select','products'));
    }
    public function advantage(Request $request)
    {
        $banner = $this->read->get_inner_banner()->where('id',4)->first();
        $advantages = $this->read->get_advantage('locale')->get();
        $url = $this->read->search_advantage_by_url($request);
        $select = $this->read->get_advantage('locale')->whereid($url)->firstOrFail();
        return view('frontend.advantage',compact('banner','advantages','select'));
    }
    public function contact(Request $request)
    {
        $banner = $this->read->get_inner_banner()->where('id',5)->first();
        $companys = $this->read->get_company_info()->get();
        $classes = $this->read->get_product_class('locale')->get();
        $product = $this->read->get_product('locale')->where('url',$request->product)->first();
        return view('frontend.contact',compact('banner','companys','classes','product'));
    }
    public function set_lang(Request $request)
    {
        switch($request->lang){
            case 'en':
                app()->setLocale('en');
                session()->put('locale', app()->getLocale());
                break;
            case 'zh_cn':
                app()->setLocale('zh_cn');
                session()->put('locale', app()->getLocale());
                break;
            default:
                app()->setLocale('en');
                session()->put('locale', app()->getLocale());
                break;
        }

        if(preg_match('/news_detail/', url()->previous()))
        {
            return redirect()->route('company',['url' => 'news']);
        }
        else
        {
        	return redirect()->back(); 
        }

    }
    public function show_product(Request $request)
    {
        $product = $this->read->get_product('locale')->where('product_type_id',$request->type_id)->get();
        return $product;
    }
}
