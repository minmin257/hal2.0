<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Repositories\CreateRepository;
use App\Services\MailService;

class ContactController extends Controller
{
	public function __construct(MailService $MailService,CreateRepository $CreateRepository)
	{
		$this->MailService = $MailService;
		$this->create = $CreateRepository;
	}

    public function create_contact(ContactRequest $request)
    {
    	$this->create->create_contact($request->all());
    }

    public function contact_send(Request $request)
    {
    	$this->MailService->send($request->all());
    }
}
