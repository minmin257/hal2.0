<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductFile_en extends Model
{
    protected $fillable = [
    	'title','href','sort','delete','product_id'
    ];

    public function product()
    {
    	return $this->belongsTo(Product_en::class ,'product_id','id');
    }
}
