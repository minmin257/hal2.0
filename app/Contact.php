<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'company', 'tel', 'email', 'product_id', 'content', 'state', 'reply', 'delete','type_id'
    ];

    public function product()
    {
    	return $this->belongsTo(Product::class,'product_id','id')->withDefault([
    		'title' => null
    	]);
    }
    public function type()
    {
        return $this->belongsTo(ProductType::class,'type_id','id')->withDefault([
            'title' => null
        ]);
    }
}
