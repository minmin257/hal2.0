<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'permission_types_id', 'name', 'description'
    ];

    public function user_has_permission()
    {
        return $this->hasMany(UserHasPermission::class);
    }
}
