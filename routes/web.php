<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// 前台
// 網站維護中介層
Route::group(['middleware'=>['maintenance']], function(){
	Route::get('/','FrontendController@index')->name('index');
	Route::get('company/{url}','FrontendController@company')->name('company');
	Route::get('company/news_detail/{id}','FrontendController@news')->name('news');
	Route::get('product/{class}/{type?}/{product?}','FrontendController@product')->name('product');
	Route::get('solution/{url}','FrontendController@solution')->name('solution');
	Route::get('advantage/{url}','FrontendController@advantage')->name('advantage');
	Route::get('contact','FrontendController@contact')->name('contact');
	Route::post('/create_contact','ContactController@create_contact')->name('create_contact');
	Route::post('/contact_send','ContactController@contact_send')->name('contact.send');
	Route::get('/lang/set/', 'FrontendController@set_lang')->name('set_lang');
	Route::post('show_product','FrontendController@show_product')->name('show_product');

});


// 後台
// prefix URL為/webmin/?
// namespace Controllers 預設路徑為 "App\Http\Controllers\backend"
Route::group(['prefix'=>'webmin','namespace'=>'backend'], function(){

	// 登入登出
	Route::get('/','LoginController@index')->name('backend');
	Route::post('/','LoginController@session_in')->name('login');
	Route::get('/logout','LoginController@logout')->name('logout');

	// 登入後迎賓頁
	Route::group(['middleware'=>'auth:web'], function(){
		Route::get('/index','ModuleController@index')->name('backend.index');
		Route::get('/lang', 'ModuleController@set_lang')->name('backend.lang');

		
		Route::group(['prefix'=>'content','namespace'=>'content','middleware'=>'HasPermission:content read'], function(){

			// 首頁管理
			Route::get('/','IndexController@index')->name('home_index');
			Route::post('/','IndexController@update')->name('update_index')->middleware('HasPermission:content update');

			// 內頁banner管理
			Route::get('/inner_banner','InnerBannerController@index')->name('backend.inner_banner');
			Route::post('/update_inner_banner','InnerBannerController@update')->name('update_inner_banner')->middleware('HasPermission:content update');
			Route::post('/delete_inner_banner','InnerBannerController@delete_inner_banner')->name('delete_inner_banner')->middleware('HasPermission:content delete');

			Route::group(['prefix'=>'company'], function(){
				// 概要
				Route::get('/about','AboutController@index')->name('backend.about');
				Route::post('/update_about','AboutController@update')->name('update_about')->middleware('HasPermission:content update');

				// 沿革
				Route::get('/history','HistoryController@index')->name('history');
				Route::post('/history','HistoryController@create')->name('create_history')->middleware('HasPermission:content create');
				Route::post('/edit','HistoryController@edit')->name('edit_history');
				Route::post('/update','HistoryController@update')->name('update_history')->middleware('HasPermission:content update');
				Route::post('/delete','HistoryController@delete')->name('delete_history')->middleware('HasPermission:content delete');

				Route::get('/company_page','AboutController@company_page')->name('company_page');
				Route::post('/company_page','AboutController@update_page')->name('update_page')->middleware('HasPermission:content update');
			});
			// 消息
			Route::get('/news/{lang}','NewsController@index')->name('backend.news');
			Route::post('/news','NewsController@create')->name('create_news')->middleware('HasPermission:content create');
			Route::get('/edit_news','NewsController@edit')->name('edit_news');
			Route::post('/update_news','NewsController@update')->name('update_news')->middleware('HasPermission:content update');
			Route::post('/delete_news','NewsController@delete')->name('delete_news')->middleware('HasPermission:content delete');

			// 產品管理
			Route::group(['prefix'=>'product'], function(){
				// 大分類
				Route::get('/class','ProductClassController@index')->name('class');
				Route::post('/class','ProductClassController@create')->name('create_class')->middleware('HasPermission:content create');
				Route::get('/edit_class','ProductClassController@edit')->name('edit_class');
				Route::post('/update_class','ProductClassController@update')->name('update_class')->middleware('HasPermission:content update');
				Route::post('/delete_class','ProductClassController@delete')->name('delete_class')->middleware('HasPermission:content delete');
				// 小分類
				Route::get('/type','ProductTypeController@index')->name('type');
				Route::post('/type','ProductTypeController@create')->name('create_type')->middleware('HasPermission:content create');
				Route::get('/edit_type','ProductTypeController@edit')->name('edit_type');
				Route::post('/update_type','ProductTypeController@update')->name('update_type')->middleware('HasPermission:content update');
				Route::post('/delete_type','ProductTypeController@delete')->name('delete_type')->middleware('HasPermission:content delete');
				// 產品
				Route::get('/','ProductController@index')->name('backend.product');
				Route::post('/','ProductController@create')->name('create_product')->middleware('HasPermission:content create');
				Route::get('/edit_product','ProductController@edit')->name('edit_product');
				Route::post('/update_product','ProductController@update')->name('update_product')->middleware('HasPermission:content update');
				Route::post('/delete_product','ProductController@delete')->name('delete_product')->middleware('HasPermission:content delete');
				Route::post('/show_type','ProductController@show_type')->name('show_type');
				Route::post('/copy_product','ProductController@copy_product')->name('copy_product')->middleware('HasPermission:content create');
				// 規格
				Route::get('/file','ProductFileController@index')->name('file');
				Route::post('/file','ProductFileController@create')->name('create_file')->middleware('HasPermission:content create');
				Route::get('/edit_file','ProductFileController@edit')->name('edit_file');
				Route::post('/update_file','ProductFileController@update')->name('update_file')->middleware('HasPermission:content update');
				Route::post('/delete_file','ProductFileController@delete')->name('delete_file')->middleware('HasPermission:content delete');
			});

			// 應用管理
			Route::group(['prefix'=>'solution'], function(){
				Route::get('/','SolutionController@index')->name('backend.solution');
				Route::post('/','SolutionController@create')->name('create_solution')->middleware('HasPermission:content create');
				Route::get('/edit_solution','SolutionController@edit')->name('edit_solution');
				Route::post('/update_solution','SolutionController@update')->name('update_solution')->middleware('HasPermission:content update');
				Route::post('/delete_solution','SolutionController@delete')->name('delete_solution')->middleware('HasPermission:content delete');
				Route::get('/pic','SolutionController@pic')->name('solution_pic');
				Route::post('/pic','SolutionController@create_pic')->name('create_pic')->middleware('HasPermission:content create');
				Route::post('/update_pic','SolutionController@update_pic')->name('update_pic')->middleware('HasPermission:content update');
				Route::post('/delete_pic','SolutionController@delete_pic')->name('delete_pic')->middleware('HasPermission:content delete');
			});
			// 優勢管理
			Route::group(['prefix'=>'advantage'], function(){
				Route::get('/','AdvantageController@index')->name('backend.advantage');
				Route::post('/','AdvantageController@create')->name('create_advantage')->middleware('HasPermission:content create');
				Route::get('/edit_advantage','AdvantageController@edit')->name('edit_advantage');
				Route::post('/update_advantage','AdvantageController@update')->name('update_advantage')->middleware('HasPermission:content update');
				Route::post('/delete_advantage','AdvantageController@delete')->name('delete_advantage')->middleware('HasPermission:content delete');
			});
			// 公司資訊
			Route::group(['prefix'=>'company_info'], function(){
				Route::get('/','CompanyInfoController@index')->name('company_info');
				Route::post('/','CompanyInfoController@create')->name('create_company_info')->middleware('HasPermission:content create');
				Route::get('/edit_company_info','CompanyInfoController@edit')->name('edit_company_info');
				Route::post('/update_company_info','CompanyInfoController@update')->name('update_company_info')->middleware('HasPermission:content update');
				Route::post('/delete_company_info','CompanyInfoController@delete')->name('delete_company_info')->middleware('HasPermission:content delete');
			});
		});


		// 表單管理
		Route::group(['prefix'=>'form','middleware'=>'HasPermission:form read'], function(){
			Route::get('/','FormController@index')->name('form');
			Route::get('/no_read','FormController@form_no_read')->name('form_no_read');
			Route::get('/no_reply','FormController@form_no_reply')->name('form_no_reply');
			Route::get('/replied','FormController@form_replied')->name('form_replied');
			Route::post('/filter','FormController@filter')->name('form.filter');
			Route::post('/detail','FormController@detail')->name('form.detail');
			Route::post('/reply_contact','FormController@reply')->name('reply_contact')->middleware('HasPermission:form update');
			Route::post('/delete_contact','FormController@delete_contact')->name('delete_contact')->middleware('HasPermission:form delete');
		});

		// 權限管理
		Route::group(['prefix'=>'authority'], function(){
			Route::get('/','AuthorityController@management')->name('management');
			Route::post('/','AuthorityController@create')->name('create_user')->middleware('HasPermission:authority create');
			Route::get('/edit_user','AuthorityController@edit')->name('edit_user')->middleware('HasPermission:authority update');
			Route::post('/delete_account','AuthorityController@delete_account')->name('delete_account')->middleware('HasPermission:authority delete');
			Route::get('/profile','AuthorityController@index')->name('profile');
			Route::post('/profile','AuthorityController@update')->name('update_profile');
			Route::get('/permission','AuthorityController@permission')->name('permission');
			Route::post('/create_permission','AuthorityController@create_permission')->name('create_permission');
		});

		// 系統設定
		Route::group(['prefix'=>'system'], function(){
			Route::get('/','SystemController@index')->name('system')->middleware('HasPermission:system read');
			Route::post('/','SystemController@update')->name('update_system')->middleware('HasPermission:system update');
			Route::get('/mail_setting','SystemController@mail_setting')->name('mail_setting')->middleware('HasPermission:mail_account read');
			Route::post('/update_mail_account','SystemController@update_mail_account')->name('update_mail_account')->middleware('HasPermission:mail_account update');
			Route::post('/update_mail','SystemController@update_mail')->name('update_mail')->middleware('HasPermission:mail_account update');
			Route::get('/track_code','SystemController@track_code')->name('track_code')->middleware('HasPermission:system read');
			Route::post('/track_code','SystemController@update_track_code')->name('update_track_code')->middleware('HasPermission:system update');
		});
	});

		
});