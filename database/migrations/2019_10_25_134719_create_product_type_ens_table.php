<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTypeEnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_type_ens', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_class_id');
            $table->foreign('product_class_id')->references('id')->on('product_class_ens');
            $table->string('title');
            $table->string('url');
            $table->integer('sort')->comment('順序')->default(0);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_type_ens');
    }
}
