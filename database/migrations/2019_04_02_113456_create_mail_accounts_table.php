<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->comment('寄件人');
            $table->string('email')->comment('寄件信箱');
            $table->text('password');
            $table->string('subject')->comment('寄件主旨');
            $table->string('subject_en')->comment('寄件主旨');
            $table->string('recipient')->comment('副本');
            $table->string('headline')->default('力征实业股份有限公司 High & Low Corp.')->comment('表頭');
            $table->longText('signature')->nullable()->comment('簽名檔');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mail_accounts');
    }
}
