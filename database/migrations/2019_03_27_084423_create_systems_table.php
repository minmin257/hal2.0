<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('systems', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sitename')->comment('網站名稱');
            $table->string('description')->comment('meta_description');
            $table->string('keywords')->comment('meta_keywords');
            $table->string('logo');
            $table->string('copy_right');
            $table->text('cookie');
            $table->boolean('maintain')->default(0)->comment('0關閉/1維護');
            $table->string('message')->comment('維護訊息');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('systems');
    }
}
