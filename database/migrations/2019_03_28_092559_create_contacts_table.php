<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('company')->nullable();
            $table->string('tel');
            $table->string('email');
            $table->text('content');
            $table->unsignedInteger('type_id')->nullable();
            $table->unsignedInteger('product_id')->nullable();
            $table->boolean('state')->default(0)->comment('未讀0/已讀1');
            $table->boolean('reply')->default(0)->comment('未回覆0/已回覆1');
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
