<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolutionPicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solution_pics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('solution_id');
            $table->foreign('solution_id')->references('id')->on('solutions');
            $table->string('src');
            $table->integer('sort')->comment('順序')->default(0);
            $table->boolean('delete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solution_pics');
    }
}
