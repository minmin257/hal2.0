<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndexEnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('index_ens', function (Blueprint $table) {
            $table->increments('id');
            $table->string('banner');
            $table->string('href')->nullable();
            $table->text('slogan');
            $table->string('slogan_src')->comment('slogan底圖');
            $table->string('application_src')->comment('application底圖');
            $table->text('company');
            $table->string('company_src')->comment('company底圖');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('index_ens');
    }
}
