<?php

use App\About;
use App\Advantage;
use App\Advantage_en;
use App\CompanyInfo;
use App\CompanyInfo_en;
use App\History;
use App\Index;
use App\Index_en;
use App\InnerBanner;
use App\MailAccount;
use App\News;
use App\Permission;
use App\PermissionType;
use App\Product;
use App\ProductClass;
use App\ProductClass_en;
use App\ProductType;
use App\ProductType_en;
use App\Product_en;
use App\Role;
use App\Solution;
use App\SolutionHasProduct;
use App\SolutionPic;
use App\Solution_en;
use App\Subject;
use App\System;
use App\System_en;
use App\User;
use App\UserHasPermission;
use App\UserHasRole;
use Illuminate\Database\Seeder;
use App\TrackCode;
use App\Language;
use App\CompanyPage;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        CompanyPage::create([
          'profile' => 1,
          'history' => 1,
          'news' => 1
        ]);
        TrackCode::create([
          'yahoo' => '',
          'google' => '',
          'biadu' => ''
        ]);
        Language::create([
          'name' => 'auto'
        ]);
        System::create([
            'sitename'    => '力征實業股份有限公司 High & Low Corporation',
            'keywords'    => '力征實業股份有限公司',
            'description' => '力征實業股份有限公司',
            'logo'        => '/images/logo.png',
            'copy_right'  => '© 2019  力征實業股份有限公司 High & Low Corporation. All rights reserved.',
            'message'     => '本系統於進行設備維護，暫時無法提供服務。造成不便，敬請見諒。',
            'cookie'      => '<h4>Cookie使用聲明</h4>
          <p>
            為了提供您最佳的服務，本網站會在您的電腦中放置並取用我們的Cookie，若您不願接受Cookie的寫入，您可在您使用的瀏覽器功能項中設定隱私權等級為高，即可拒絕Cookie的寫入，但可能會導至網站某些功能無法正常執行 。
          </p>',
        ]);
        System_en::create([
            'sitename'    => '力征實業股份有限公司 High & Low Corporation',
            'keywords'    => '力征實業股份有限公司',
            'description' => '力征實業股份有限公司',
            'logo'        => '/images/logo.png',
            'copy_right'  => '© 2019  力征實業股份有限公司 High & Low Corporation. All rights reserved.',
            'message'     => '本系統於進行設備維護，暫時無法提供服務。造成不便，敬請見諒。',
            'cookie'      => '<h4>Cookie使用聲明</h4>
          <p>
            為了提供您最佳的服務，本網站會在您的電腦中放置並取用我們的Cookie，若您不願接受Cookie的寫入，您可在您使用的瀏覽器功能項中設定隱私權等級為高，即可拒絕Cookie的寫入，但可能會導至網站某些功能無法正常執行 。
          </p>',
        ]);
        MailAccount::create([
            'name'      => '我是寄件人名稱',
            'email'     => 'jhong.creativity.tw@gmail.com',
            'password'  => encrypt('0423152139'),
            'subject'   => '我是主旨 主旨 主旨',
            'subject_en'   => '我是主旨 主旨 主旨',
            'recipient' => 'test@xx.xx',
        ]);

        Index::create([
            'banner'          => '/images/banner-index.jpg',
            'slogan'          => '<div class="container tc">
                          <br>
                          <img class="img-max100" src="/images/slogan.png" alt="">
                          <br>
                          <br>
                        </div>',
            'slogan_src'      => '/images/slogan-bg.jpg',
            'application_src' => '/images/solution-bg.jpg',
            'company'         => '<p>
                  力征是从事制造与设计开发EMI/RFI滤波器的专业制造商。总公司位于台湾台北, 生产基地位于中国深圳。我们有25年电磁兼容解决方案的经验且拥有领先的专利技术，搭配多种不同类型的自动/半自动化生产机台和测试设备，提供市场高品质、售价具竞争力且出货又快速的产品。
                </p>',
            'company_src'     => '/images/index-company-img.jpg',
        ]);

        Index_en::create([
            'banner'          => '/images/banner-index.jpg',
            'slogan'          => '<div class="container tc">
                          <br>
                          <img class="img-max100" src="/images/slogan.png" alt="">
                          <br>
                          <br>
                        </div>',
            'slogan_src'      => '/images/slogan-bg.jpg',
            'application_src' => '/images/solution-bg.jpg',
            'company'         => "<p>
                      H&L is a profession manufacturer developing the EMI/RFI filter solutions. H&L's head office is based in Taipei Taiwan and factory in Shenzhen China. Combining award-winning technology, editorial expertise and favorable conditions of location, H&L provides customers with highly relevant search results, competitive price, rapid delivery, good quality product and full support for marketing demands.
                  </p>",
            'company_src'     => '/images/index-company-img.jpg',
        ]);

        Subject::create([
            'name' => '公司',
        ]);
        Subject::create([
            'name' => '產品',
        ]);
        Subject::create([
            'name' => '應用',
        ]);
        Subject::create([
            'name' => '優勢',
        ]);
        Subject::create([
            'name' => '聯絡',
        ]);

        InnerBanner::create([
            'subject_id' => 1,
            'src'        => '/images/banner-company.jpg',
        ]);
        InnerBanner::create([
            'subject_id' => 2,
            'src'        => '/images/banner-product.jpg',
        ]);
        InnerBanner::create([
            'subject_id' => 3,
            'src'        => '/images/banner-solution.jpg',
        ]);
        InnerBanner::create([
            'subject_id' => 4,
            'src'        => '/images/banner-advantage.jpg',
        ]);
        InnerBanner::create([
            'subject_id' => 5,
            'src'        => '/images/banner-contact.jpg',
        ]);
        About::create([
            'html'    => '<div class="row justify-content-center mb-3">
                    <div class="col-12 col-lg-9">
                      <p>
                        力征是从事制造与设计开发EMI/RFI滤波器的专业制造商。总公司位于台湾台北，生产基地位于中国深圳，拥有领先的专利技术且结合了优势的地理位置，提供市场高品质、售价具竞争力且出货又快速的产品。<br>
                        <br>
                        我们仔细聆听客户的需求，力征与其世界各地的分销商皆秉持以力征的6种全体文化精神：勇气 / 自信 / 果决 / 责任感 / 热情 / 诚信来服务客户，并非仅是喊口号，我们的文化是付诸实际的行动力，并实践于公司内部每一件事情中。 这些文化精神是驱使我们对待客户的方式，我们不仅只是提供好的产品与服务，亦以真诚的伙伴关系去聆听并满足客户的潜在需求，因而赢得长期的合作关系。同时，我们的文化精神也要求回馈予社会及爱护共存的自然环境，并对之负起己任。
                      </p>
                    </div>
                    <div class="col-6 col-lg-3">
                      <img class="img-100" src="/images/logo-icon.png" alt="">
                    </div>
                  </div>
                  <table class="table profile-table">
                    <tbody>
                      <tr>
                        <td>产业类形</td>
                        <td>制造商</td>
                      </tr>
                      <tr>
                        <td>主要產品</td>
                        <td>电源滤波器, 电源模块式滤波器, 单级滤波器, 三相滤波器, 家电专用滤波器, 工业及自动化设备用滤波器, 通讯用滤波器, 计算机暨信息设备滤波器, 塑膠射出, 代理銷售钽质电容等。</td>
                      </tr>
                      <tr>
                        <td>成立年份</td>
                        <td>西元 1994</td>
                      </tr>
                      <tr>
                        <td>总员工数</td>
                        <td>220+人</td>
                      </tr>
                      <tr>
                        <td>资本額</td>
                        <td>美金 $1.3百万</td>
                      </tr>
                      <tr>
                        <td>年營業额</td>
                        <td>美金 $31.58百万</td>
                      </tr>
                      <tr>
                        <td>交易币別</td>
                        <td>美金</td>
                      </tr>
                      <tr>
                        <td>销售市場</td>
                        <td>欧洲, 北美, 中东, 非洲, 中南美洲, 亚洲, 澳洲</td>
                      </tr>
                      <tr>
                        <td>品牌名稱</td>
                        <td>High & Low</td>
                      </tr>
                      <tr>
                        <td>OEM/ODM设计服务</td>
                        <td>有</td>
                      </tr>
                      <tr>
                        <td>生产线数</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>工厂規模<br>
                        (平方公尺/英呎)</td>
                        <td>18000 / 193750</td>
                      </tr>
                      <tr>
                        <td>贸易条件</td>
                        <td>EX Work工廠交貨價</td>
                      </tr>
                      <tr>
                        <td>运送时间</td>
                        <td>收到货款后 4周内</td>
                      </tr>
                      <tr>
                        <td>运送方式</td>
                        <td>视客户要求</td>
                      </tr>
                      <tr>
                        <td>付款条件</td>
                        <td>预先电汇付款</td>
                      </tr>
                    </tbody>
                  </table>',
            'html_en' => '<div class="row justify-content-center mb-3">
                    <div class="col-12 col-lg-9">
                      <p>
                        力征是从事制造与设计开发EMI/RFI滤波器的专业制造商。总公司位于台湾台北，生产基地位于中国深圳，拥有领先的专利技术且结合了优势的地理位置，提供市场高品质、售价具竞争力且出货又快速的产品。<br>
                        <br>
                        我们仔细聆听客户的需求，力征与其世界各地的分销商皆秉持以力征的6种全体文化精神：勇气 / 自信 / 果决 / 责任感 / 热情 / 诚信来服务客户，并非仅是喊口号，我们的文化是付诸实际的行动力，并实践于公司内部每一件事情中。 这些文化精神是驱使我们对待客户的方式，我们不仅只是提供好的产品与服务，亦以真诚的伙伴关系去聆听并满足客户的潜在需求，因而赢得长期的合作关系。同时，我们的文化精神也要求回馈予社会及爱护共存的自然环境，并对之负起己任。
                      </p>
                    </div>
                    <div class="col-6 col-lg-3">
                      <img class="img-100" src="/images/logo-icon.png" alt="">
                    </div>
                  </div>
                  <table class="table profile-table">
                    <tbody>
                      <tr>
                        <td>产业类形</td>
                        <td>制造商</td>
                      </tr>
                      <tr>
                        <td>主要產品</td>
                        <td>电源滤波器, 电源模块式滤波器, 单级滤波器, 三相滤波器, 家电专用滤波器, 工业及自动化设备用滤波器, 通讯用滤波器, 计算机暨信息设备滤波器, 塑膠射出, 代理銷售钽质电容等。</td>
                      </tr>
                      <tr>
                        <td>成立年份</td>
                        <td>西元 1994</td>
                      </tr>
                      <tr>
                        <td>总员工数</td>
                        <td>220+人</td>
                      </tr>
                      <tr>
                        <td>资本額</td>
                        <td>美金 $1.3百万</td>
                      </tr>
                      <tr>
                        <td>年營業额</td>
                        <td>美金 $31.58百万</td>
                      </tr>
                      <tr>
                        <td>交易币別</td>
                        <td>美金</td>
                      </tr>
                      <tr>
                        <td>销售市場</td>
                        <td>欧洲, 北美, 中东, 非洲, 中南美洲, 亚洲, 澳洲</td>
                      </tr>
                      <tr>
                        <td>品牌名稱</td>
                        <td>High & Low</td>
                      </tr>
                      <tr>
                        <td>OEM/ODM设计服务</td>
                        <td>有</td>
                      </tr>
                      <tr>
                        <td>生产线数</td>
                        <td>13</td>
                      </tr>
                      <tr>
                        <td>工厂規模<br>
                        (平方公尺/英呎)</td>
                        <td>18000 / 193750</td>
                      </tr>
                      <tr>
                        <td>贸易条件</td>
                        <td>EX Work工廠交貨價</td>
                      </tr>
                      <tr>
                        <td>运送时间</td>
                        <td>收到货款后 4周内</td>
                      </tr>
                      <tr>
                        <td>运送方式</td>
                        <td>视客户要求</td>
                      </tr>
                      <tr>
                        <td>付款条件</td>
                        <td>预先电汇付款</td>
                      </tr>
                    </tbody>
                  </table>',
        ]);

        History::create([
            'year'    => '1994',
            'info'    => '力征专门从事电子零件贸易并主要代售电源滤波器，AC插座，开关，电容等产品。',
            'info_en' => '力征专门从事电子零件贸易并主要代售电源滤波器，AC插座，开关，电容等产品。',
        ]);

        History::create([
            'year'    => '1997',
            'info'    => '我们发现21世纪电源滤波器将是重要的零件，于是开始研发并生产。',
            'info_en' => '我们发现21世纪电源滤波器将是重要的零件，于是开始研发并生产。',
        ]);

        History::create([
            'year'    => '1998',
            'info'    => '为了确保产品质量，在深圳建立工厂。',
            'info_en' => '为了确保产品质量，在深圳建立工厂。',
        ]);

        History::create([
            'year'    => '1999',
            'info'    => '力征通过了 DNV 认证的 ISO 9002(edition 1994)，总公司扩大乔迁到台湾新店。',
            'info_en' => '力征通过了 DNV 认证的 ISO 9002(edition 1994)，总公司扩大乔迁到台湾新店。',
        ]);

        History::create([
            'year'    => '2001',
            'info'    => '所有系列的产品更新为医疗等级，而且取得了 CSA 安规认证。',
            'info_en' => '所有系列的产品更新为医疗等级，而且取得了 CSA 安规认证。',
        ]);

        History::create([
            'year'    => '2002',
            'info'    => 'ISO9002 更新为 ISO 9001(edition 2000)，于此同时完成了 SS6 系列的设计。',
            'info_en' => 'ISO9002 更新为 ISO 9001(edition 2000)，于此同时完成了 SS6 系列的设计。',
        ]);

        History::create([
            'year'    => '2003',
            'info'    => '为提高生产能力工厂搬到了新的工业区。',
            'info_en' => '为提高生产能力工厂搬到了新的工业区。',
        ]);

        History::create([
            'year'    => '2004',
            'info'    => '为改善管理体质导入 ERP 系统的使用。',
            'info_en' => '为改善管理体质导入 ERP 系统的使用。',
        ]);

        History::create([
            'year'    => '2005',
            'info'    => '全数导入RoHS，建立实验室。',
            'info_en' => '全数导入RoHS，建立实验室。',
        ]);

        History::create([
            'year'    => '2006',
            'info'    => '通过 IECQ QC080000 实验室取得 ISO/IEC17025 及 SMTA 认证。',
            'info_en' => '通过 IECQ QC080000 实验室取得 ISO/IEC17025 及 SMTA 认证。',
        ]);

        History::create([
            'year'    => '2007',
            'info'    => '产品已全面通过安规认证EN60939:2005。',
            'info_en' => '产品已全面通过安规认证EN60939:2005。',
        ]);

        History::create([
            'year'    => '2008',
            'info'    => 'SS1 系列通过 UL 环境温度摄氏 50 度标准的测试，可使用 UL 与 CUL 标签授权书。',
            'info_en' => 'SS1 系列通过 UL 环境温度摄氏 50 度标准的测试，可使用 UL 与 CUL 标签授权书。',
        ]);

        History::create([
            'year'    => '2009',
            'info'    => 'SS1~SS4 系列及 SS6~SS7 系列产品已全面通过 CQC 安规认证。',
            'info_en' => 'SS1~SS4 系列及 SS6~SS7 系列产品已全面通过 CQC 安规认证。',
        ]);

        History::create([
            'year'    => '2010',
            'info'    => '更新为ISO 9001:2008. S4系列及S7系列产品已全面通过 VDE 安规认证。',
            'info_en' => '更新为ISO 9001:2008. S4系列及S7系列产品已全面通过 VDE 安规认证。',
        ]);

        History::create([
            'year'    => '2011',
            'info'    => '台湾总公司搬迁至全新办公室。产品全面升级为UL1283第5版认证 - 环境温度标准升级为摄氏50度。HS-150系列产品升级为IEC60320:2007版本认证。',
            'info_en' => '台湾总公司搬迁至全新办公室。产品全面升级为UL1283第5版认证 - 环境温度标准升级为摄氏50度。HS-150系列产品升级为IEC60320:2007版本认证。',
        ]);

        History::create([
            'year'    => '2012',
            'info'    => '力征集团并购注塑及模具开发厂，扩大加工中心并成立塑料射出及模具开发的独立事业部。 <br>
                        S7系列家电专用电源滤波器已取得UL认证。',
            'info_en' => '力征集团并购注塑及模具开发厂，扩大加工中心并成立塑料射出及模具开发的独立事业部。 <br>
                        S7系列家电专用电源滤波器已取得UL认证。',
        ]);

        History::create([
            'year'    => '2013',
            'info'    => '取得ISO 14001认证。 内部操作系统ERP导入全新GP系统。',
            'info_en' => '取得ISO 14001认证。 内部操作系统ERP导入全新GP系统。',
        ]);

        History::create([
            'year'    => '2014',
            'info'    => '工厂搬迁至新工业厂区。',
            'info_en' => '工厂搬迁至新工业厂区。',
        ]);

        History::create([
            'year'    => '2015',
            'info'    => '成立欧洲办公室. 推出防水型电源模块滤波器。',
            'info_en' => '成立欧洲办公室. 推出防水型电源模块滤波器。',
        ]);

        History::create([
            'year'    => '2017',
            'info'    => '推出塑料外壳式三相滤波器。',
            'info_en' => '推出塑料外壳式三相滤波器。',
        ]);

        News::create([
            'date'    => '2019/01/30',
            'title'   => '台北办公室将于1/31举行年度尾牙活动, 当日营业时间调整',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          台北办公室将于1/31举行年度尾牙活动, <br>
                          当日营业时间调整为: 09:00 – 17:00<br>
                          最新消息文字，最新消息文字最新消息文字，最新消息文字最新消息，最新消息文字。最新消息文字最新消息文字最新消息文字最新消息文字最新消息文字。
                        </p>
                      </div>
                      <div class="col-12 col-sm-4">
                        <img class="img-100" src="/images/news.jpg" alt="">
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2018/12/20',
            'title'   => '台北办公室2019年度相关节假日及弹性放假',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          台北办公室2019年度相关节假日及弹性放假
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2014/11/20',
            'title'   => '2014/11/11-14 德国慕尼黑电子展',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2014/11/11-14 德国慕尼黑电子展
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2014/09/15',
            'title'   => '2014/10/13-16 香港秋季电子产品展',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2014/10/13-16 香港秋季电子产品展
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2014/04/10',
            'title'   => '2014/5/22-26 台中自动化工业展(摊位号码B0871)',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2014/5/22-26 台中自动化工业展(摊位号码B0871)
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2014/02/20',
            'title'   => '2014/8/27-30 台北自动化工业展 (摊位号码N1224)',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2014/8/27-30 台北自动化工业展 (摊位号码N1224)
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2014/01/31',
            'title'   => '2014/4/13-16香港春季电子产品展',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2014/4/13-16香港春季电子产品展
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2011/08/31',
            'title'   => '2011/11/2-4电磁相容与安规认证暨微波展览会',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2011/11/2-4电磁相容与安规认证暨微波展览会
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2011/06/20',
            'title'   => '2011/10/31-11/3中国国际医疗器械秋季博览会',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2011/10/31-11/3中国国际医疗器械秋季博览会
                        </p>
                      </div>
                    </div>',
        ]);

        News::create([
            'date'    => '2011/01/31',
            'title'   => '2011/5/26-29 中国国际专业音响.灯光.乐器展览会',
            'content' => '<div class="row">
                      <div class="col-12 col-sm-8">
                        <p>
                          2011/5/26-29 中国国际专业音响.灯光.乐器展览会
                        </p>
                      </div>
                    </div>',
        ]);

        ProductClass::create([
            'title' => '电源模块式滤波器',
            'url'   => '电源模块式滤波器',
        ]);
        ProductClass::create([
            'title' => '带插座式滤波器',
            'url'   => '带插座式滤波器',
        ]);
        ProductClass::create([
            'title' => '单相电源滤波器',
            'url'   => '单相电源滤波器',
        ]);
        ProductClass::create([
            'title' => '三相滤波器',
            'url'   => '三相滤波器',
        ]);
        ProductClass::create([
            'title' => '家电专用滤波器',
            'url'   => '家电专用滤波器',
        ]);
        ProductClass::create([
            'title' => '医疗专用滤波器',
            'url'   => '医疗专用滤波器',
        ]);
        ProductClass::create([
            'title' => '客制化电源滤波器',
            'url'   => '客制化电源滤波器',
        ]);

        ProductClass_en::create([
            'title' => 'IEC INLET FILTERS',
            'url'   => 'IEC_INLET_FILTERS',
        ]);
        ProductClass_en::create([
            'title' => 'POWER ENTRY MODULES',
            'url'   => 'POWER_ENTRY_MODULES',
        ]);
        ProductClass_en::create([
            'title' => 'SINGLE PHASE FILTERS',
            'url'   => 'SINGLE_PHASE_FILTERS',
        ]);
        ProductClass_en::create([
            'title' => '3 PHASE FILTERS',
            'url'   => '3_PHASE_FILTERS',
        ]);
        ProductClass_en::create([
            'title' => 'APPLIANCE FILTERS',
            'url'   => 'APPLIANCE_FILTERS',
        ]);
        ProductClass_en::create([
            'title' => 'MEDICAL APPLIANCE FILTERS',
            'url'   => 'MEDICAL_APPLIANCE_FILTERS',
        ]);
        ProductClass_en::create([
            'title' => 'CUSTMIZED',
            'url'   => 'CUSTMIZED',
        ]);

        ProductType::create([
            'product_class_id' => 1,
            'title'            => '插座 + 保险丝座',
            'url'              => '插座+保险丝座',
        ]);
        ProductType::create([
            'product_class_id' => 1,
            'title'            => '插座 + 开关',
            'url'              => '插座+开关',
        ]);
        ProductType::create([
            'product_class_id' => 1,
            'title'            => '插座 + 保险丝座 + 开关',
            'url'              => '插座+保险丝座+开关',
        ]);

        ProductType_en::create([
            'product_class_id' => 1,
            'title'            => 'SOCKET + FUSE HOLDER',
            'url'              => 'SOCKET+FUSE_HOLDER',
        ]);
        ProductType_en::create([
            'product_class_id' => 1,
            'title'            => 'SOCKET + SWITCH',
            'url'              => 'SOCKET+SWITCH',
        ]);
        ProductType_en::create([
            'product_class_id' => 1,
            'title'            => 'SOCKET + FUSE HOLDER + SWITCH',
            'url'              => 'SOCKET+FUSE_HOLDER+SWITCH',
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'Power entry moudles SS3-S',
            'subtitle'         => 'General purpose power entry modules with fuse holders.',
            'url'              => 'Power_entry_moudles_SS3_S',
            'src'              => '/images/SS3-S.png',
            'content'          => '<p>
                      Power entry module with filter<br>
                      Screw type with left & right ears<br>
                      General purpose EMI/RFI filter<br>
                      Front mounting<br>
                      With IEC inlet socket and fuse holder<br>
                      Easy to install, compact size<br>
                      Current rating from 1A to 10A<br>
                      Medical version is available
                    </p>
                    <img class="mr-1 mb-3" src="/images/icon-01.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-02.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-03.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-04.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-05.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-06.jpg" alt="">',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => '3 Phase Filter',
            'url'              => '3_Phase_Filter',
            'src'              => '/images/3PhaseFilter.png',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'Home Appliance Filter',
            'url'              => 'Home_Appliance_Filter',
            'src'              => '/images/Appliance_Filters01.png',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'IEC Inlet Filter',
            'url'              => 'IEC_Inlet_Filter',
            'src'              => '/images/IECInletFilter01.gif',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'PCB Filter',
            'url'              => 'PCB_Filter',
            'src'              => '/images/PCB04.png',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'Power Entry Module',
            'url'              => 'Power_Entry_Module',
            'src'              => '/images/PowerEntryModules17.png',
            'display_on_index' => 1,
        ]);

        Product::create([
            'product_type_id'  => 1,
            'title'            => 'Single Phase Filter',
            'url'              => 'Single_Phase_Filter',
            'src'              => '/images/SinglePhaseFilter.png',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'Power entry moudles SS3-S',
            'subtitle'         => 'General purpose power entry modules with fuse holders.',
            'url'              => 'Power_entry_moudles_SS3_S',
            'src'              => '/images/SS3-S.png',
            'content'          => '<p>
                      Power entry module with filter<br>
                      Screw type with left & right ears<br>
                      General purpose EMI/RFI filter<br>
                      Front mounting<br>
                      With IEC inlet socket and fuse holder<br>
                      Easy to install, compact size<br>
                      Current rating from 1A to 10A<br>
                      Medical version is available
                    </p>
                    <img class="mr-1 mb-3" src="/images/icon-01.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-02.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-03.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-04.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-05.jpg" alt="">
                    <img class="mr-1 mb-3" src="/images/icon-06.jpg" alt="">',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => '3 Phase Filter',
            'url'              => '3_Phase_Filter',
            'src'              => '/images/3PhaseFilter.png',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'Home Appliance Filter',
            'url'              => 'Home_Appliance_Filter',
            'src'              => '/images/Appliance_Filters01.png',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'IEC Inlet Filter',
            'url'              => 'IEC_Inlet_Filter',
            'src'              => '/images/IECInletFilter01.gif',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'PCB Filter',
            'url'              => 'PCB_Filter',
            'src'              => '/images/PCB04.png',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'Power Entry Module',
            'url'              => 'Power_Entry_Module',
            'src'              => '/images/PowerEntryModules17.png',
            'display_on_index' => 1,
        ]);

        Product_en::create([
            'product_type_id'  => 1,
            'title'            => 'Single Phase Filter',
            'url'              => 'Single_Phase_Filter',
            'src'              => '/images/SinglePhaseFilter.png',
            'display_on_index' => 1,
        ]);

        Solution::create([
            'title'   => '通讯',
            'url'     => '通讯',
            'content' => '<ul class="pl-20" style="list-style: circle;">
                        <li>电信和储存系统</li>
                        <li>电源供应器</li>
                        <li>多媒体影音处理器</li>
                        <li>主机服务器</li>
                        <li>安全监控系统</li>
                        <li>销售时点系统 (POS)</li>
                        <li>多功能事务机</li>
                        <li>点/验钞机</li>
                        <li>影像控制单元 (CCU)</li>
                        <li>停车场系统</li>
                        <li>自动贩卖机</li>
                        <li>可编过程控制器 (PLC)</li>
                        <li>安规测试仪</li>
                      </ul>',
        ]);
        Solution::create([
            'title' => '工业',
            'url'   => '工业',
        ]);
        Solution::create([
            'title' => '医疗',
            'url'   => '医疗',
        ]);
        Solution::create([
            'title' => '家电',
            'url'   => '家电',
        ]);
        Solution::create([
            'title' => '车用',
            'url'   => '车用',
        ]);
        Solution::create([
            'title' => '军用',
            'url'   => '军用',
        ]);
        Solution_en::create([
            'title'   => 'Telecom equipment',
            'url'     => 'Telecom_equipment',
            'content' => '<ul class="pl-20" style="list-style: circle;">
                        <li>电信和储存系统</li>
                        <li>电源供应器</li>
                        <li>多媒体影音处理器</li>
                        <li>主机服务器</li>
                        <li>安全监控系统</li>
                        <li>销售时点系统 (POS)</li>
                        <li>多功能事务机</li>
                        <li>点/验钞机</li>
                        <li>影像控制单元 (CCU)</li>
                        <li>停车场系统</li>
                        <li>自动贩卖机</li>
                        <li>可编过程控制器 (PLC)</li>
                        <li>安规测试仪</li>
                      </ul>',
        ]);

        Solution_en::create([
            'title' => 'Industry',
            'url'   => 'Industry',
        ]);
        Solution_en::create([
            'title' => 'Medical',
            'url'   => 'Medical',
        ]);
        Solution_en::create([
            'title' => 'Home appliance',
            'url'   => 'Home_appliance',
        ]);
        Solution_en::create([
            'title' => '车用',
            'url'   => '车用',
        ]);
        Solution_en::create([
            'title' => '军用',
            'url'   => '军用',
        ]);

        foreach (range(1, 4) as $value) {
            SolutionPic::create([
                'solution_id' => 1,
                'src'         => '/images/solution-0' . $value . '.jpg',
            ]);
            if ($value < 4) {
                SolutionHasProduct::create([
                    'solution_id' => 1,
                    'product_id'  => $value,
                ]);
            }

        }
        Advantage::create([
            'title' => '品质管制',
            'url'   => '品质管制',
        ]);
        Advantage::create([
            'title' => '研发设计',
            'url'   => '研发设计',
        ]);
        Advantage::create([
            'title' => '为何选择力征',
            'url'   => '为何选择力征',
        ]);
        Advantage_en::create([
            'title' => '品质管制',
            'url'   => '品质管制',
        ]);
        Advantage_en::create([
            'title' => '研发设计',
            'url'   => '研发设计',
        ]);
        Advantage_en::create([
            'title' => '为何选择力征',
            'url'   => '为何选择力征',
        ]);
        CompanyInfo::create([
            'location' => '台湾',
            'name'     => '力征实业股份有限公司',
            'tel'      => '+886-2-8978-1800',
            'fax'      => '+886-2-8912-1900',
            'email'    => 'service@hal.com.tw',
            'add'      => '(231)台湾新北市新店区宝桥路235巷118号7楼',
            'add_href' => 'https://goo.gl/maps/53YBV2hwsGGJTyYW8',
            'map'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3616.50480938841!2d121.54837071532022!3d24.98295718399632!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346801e372757fe1%3A0x7ba4f342e79c24f2!2z5Yqb5b6B5a-m5qWt6IKh5Lu95pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1568793700207!5m2!1szh-TW!2stw" frameborder="0" allowfullscreen=""></iframe>',
        ]);
        CompanyInfo::create([
            'location' => '欧洲',
            'name'     => 'High & Low Corp.',
            'tel'      => '39-3487319675',
            'email'    => 'romina@hal-europe.com',
            'add'      => 'Via Bolzano 1/a Vimercate MB Italy',
            'add_href' => 'https://goo.gl/maps/vJJfYS2wDWN22zmQA',
            'website'  => 'www.hal-europe.com',
            'map'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2791.8763703241016!2d9.360211315804943!3d45.59302997910269!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786b72bb011d27d%3A0xa58fa7fd9093c049!2zVmlhIEJvbHphbm8sIDEsIDIwODcxIFZpbWVyY2F0ZSBNQiwg576p5aSn5Yip!5e0!3m2!1szh-TW!2stw!4v1568794534406!5m2!1szh-TW!2stw" frameborder="0" allowfullscreen=""></iframe>',
        ]);
        CompanyInfo_en::create([
            'location' => '台湾',
            'name'     => '力征实业股份有限公司',
            'tel'      => '+886-2-8978-1800',
            'fax'      => '+886-2-8912-1900',
            'email'    => 'service@hal.com.tw',
            'add'      => '(231)台湾新北市新店区宝桥路235巷118号7楼',
            'add_href' => 'https://goo.gl/maps/53YBV2hwsGGJTyYW8',
            'map'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3616.50480938841!2d121.54837071532022!3d24.98295718399632!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346801e372757fe1%3A0x7ba4f342e79c24f2!2z5Yqb5b6B5a-m5qWt6IKh5Lu95pyJ6ZmQ5YWs5Y-4!5e0!3m2!1szh-TW!2stw!4v1568793700207!5m2!1szh-TW!2stw" frameborder="0" allowfullscreen=""></iframe>',
        ]);
        CompanyInfo_en::create([
            'location' => '欧洲',
            'name'     => 'High & Low Corp.',
            'tel'      => '39-3487319675',
            'email'    => 'romina@hal-europe.com',
            'add'      => 'Via Bolzano 1/a Vimercate MB Italy',
            'add_href' => 'https://goo.gl/maps/vJJfYS2wDWN22zmQA',
            'website'  => 'www.hal-europe.com',
            'map'      => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2791.8763703241016!2d9.360211315804943!3d45.59302997910269!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4786b72bb011d27d%3A0xa58fa7fd9093c049!2zVmlhIEJvbHphbm8sIDEsIDIwODcxIFZpbWVyY2F0ZSBNQiwg576p5aSn5Yip!5e0!3m2!1szh-TW!2stw!4v1568794534406!5m2!1szh-TW!2stw" frameborder="0" allowfullscreen=""></iframe>',
        ]);
        User::create([
            'name'     => '系統管理員',
            'account'  => 'admin',
            'password' => bcrypt('123456'),
            'state'    => 1,
        ]);
        User::create([
            'name'     => '網站管理員',
            'account'  => 'user',
            'password' => bcrypt('123456'),
            'state'    => 1,
        ]);
        User::create([
            'name'     => '網管',
            'account'  => 'guest',
            'password' => bcrypt('123456'),
            'state'    => 1,
        ]);

        PermissionType::create([
            'name' => '內容管理',
        ]);
        PermissionType::create([
            'name' => '表單管理',
        ]);
        PermissionType::create([
            'name' => '帳號管理',
        ]);
        PermissionType::create([
            'name' => '網站/追蹤碼設定',
        ]);
        PermissionType::create([
            'name' => '信箱設定',
        ]);

        Permission::create([
            'permission_type_id' => '1',
            'name'               => 'content create',
            'description'        => '新增內容管理',
        ]);
        Permission::create([
            'permission_type_id' => '1',
            'name'               => 'content read',
            'description'        => '查看內容管理',
        ]);
        Permission::create([
            'permission_type_id' => '1',
            'name'               => 'content update',
            'description'        => '更新內容管理',
        ]);
        Permission::create([
            'permission_type_id' => '1',
            'name'               => 'content delete',
            'description'        => '刪除內容管理',
        ]);
        Permission::create([
            'permission_type_id' => '2',
            'name'               => 'form create',
            'description'        => '新增表單管理',
        ]);
        Permission::create([
            'permission_type_id' => '2',
            'name'               => 'form read',
            'description'        => '查看表單管理',
        ]);
        Permission::create([
            'permission_type_id' => '2',
            'name'               => 'form update',
            'description'        => '更新表單管理',
        ]);
        Permission::create([
            'permission_type_id' => '2',
            'name'               => 'form delete',
            'description'        => '刪除表單管理',
        ]);
        Permission::create([
            'permission_type_id' => '3',
            'name'               => 'authority create',
            'description'        => '新增帳號管理',
        ]);
        Permission::create([
            'permission_type_id' => '3',
            'name'               => 'authority read',
            'description'        => '查看帳號管理',
        ]);
        Permission::create([
            'permission_type_id' => '3',
            'name'               => 'authority update',
            'description'        => '更新帳號管理',
        ]);
        Permission::create([
            'permission_type_id' => '3',
            'name'               => 'authority delete',
            'description'        => '刪除帳號管理',
        ]);
        Permission::create([
            'permission_type_id' => '4',
            'name'               => 'system create',
            'description'        => '新增網站/追蹤碼設定',
        ]);
        Permission::create([
            'permission_type_id' => '4',
            'name'               => 'system read',
            'description'        => '查看網站/追蹤碼設定',
        ]);
        Permission::create([
            'permission_type_id' => '4',
            'name'               => 'system update',
            'description'        => '更新網站/追蹤碼設定',
        ]);
        Permission::create([
            'permission_type_id' => '4',
            'name'               => 'system delete',
            'description'        => '刪除網站/追蹤碼設定',
        ]);
        Permission::create([
            'permission_type_id' => '5',
            'name'               => 'mail_account create',
            'description'        => '新增信箱設定',
        ]);
        Permission::create([
            'permission_type_id' => '5',
            'name'               => 'mail_account read',
            'description'        => '查看信箱設定',
        ]);
        Permission::create([
            'permission_type_id' => '5',
            'name'               => 'mail_account update',
            'description'        => '更新信箱設定',
        ]);
        Permission::create([
            'permission_type_id' => '5',
            'name'               => 'mail_account delete',
            'description'        => '刪除信箱設定',
        ]);

        Role::create([
            'name' => '網站管理員',
        ]);
        Role::create([
            'name' => '高階使用者',
        ]);
        Role::create([
            'name' => '一般使用者',
        ]);

        for ($i = 1; $i <= 2; $i++) {
            for ($j = 1; $j <= 20; $j++) {
                UserHasPermission::create([
                    'user_id'       => $i,
                    'permission_id' => $j,
                ]);
            }
        }

        UserHasRole::create([
            'user_id' => '1',
            'role_id' => '1',
        ]);
        UserHasRole::create([
            'user_id' => '2',
            'role_id' => '2',
        ]);
        UserHasRole::create([
            'user_id' => '3',
            'role_id' => '3',
        ]);
    }
}
