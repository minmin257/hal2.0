function ajaxCreate(method, url, data){
    return {
        method: method,
        url: url,
        data: data,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url : this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                error : function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        // console.log(value);
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
                },
                success : function(data) {
                    $('#error_message').html('');
                    swal.fire({
                        title: '新增成功!',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        location.reload();
                    })
                }
            };

            $.ajax(ajaxRequest);
        }

    }
}

function ajaxUpdate(method, url, data, redirect = null){
    return {
        method: method,
        url: url,
        data: data,
        redirect: redirect,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url : this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                error : function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        // console.log(value);
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
                },
                success : function(data) {
                    $('#error_message').html('');
                    swal.fire({
                        title: '修改成功!',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        if(redirect === null)
                        {
                            location.reload();
                        }
                        else
                        {
                            location.href = redirect;
                        }
                    })
                }
            };

            $.ajax(ajaxRequest);
        }

    }
}


function ajaxDel(method, url, data){
    return {
        method: method,
        url: url,
        data: data,
        request: function request() {
            swal.fire({
                title: '您確定要刪除嗎？',
                text: "此動作將不可回復",
                type: 'warning',
                allowEscapeKey: false,
                allowOutsideClick: false,
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '確定',
                cancelButtonText: '取消',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        type: this.method,
                        url: this.url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: this.data,
                        },
                        success: function(data) {
                            swal.fire({
                                title: '成功刪除!',
                                type: 'success',
                                showConfirmButton: false,
                                timer: 1000,
                            }).then(function(){
                                location.reload();
                            })
                        },
                        error : function(data) {
                            var error = data.responseJSON;
                            errorsHtml = '';
                            $.each(error.errors, function(key,value){
                                errorsHtml += value;
                            });
                            swal.fire({
                                title: '錯誤!',
                                html: errorsHtml,
                                type: 'error',
                                showConfirmButton: false,
                                timer: 1500,
                            })
                            
                        },
                    });
                    
                }
            })
        }
    }
}


function ajaxMail(method, url, data, redirect = null){
    return {
        method: method,
        url: url,
        data: data,
        redirect: redirect,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url : this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: this.data,
                beforeSend: function() {
                  swal.fire({
                    title: '郵件發送中~請稍後!',
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                    timer: 2000,
                    onOpen: () => {
                      swal.showLoading();
                    }
                  })
                },
                error : function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    errorsHtml += '<li>'+ error.message +'</li>';
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
                },
                success : function(data) {
                    $('#error_message').html('');
                    swal.fire({
                        title: '郵件寄送完成!',
                        type: 'success',
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        timer: 1500,
                    }).then(function(){
                        if(redirect === null)
                        {
                            location.reload();
                        }
                        else
                        {
                            location.href = redirect;
                        }
                    })
                }
            };

            $.ajax(ajaxRequest);
        }
    }
}

function ajaxCopy(method, url, data, redirect = null){
    return {
        method: method,
        url: url,
        data: data,
        redirect: redirect,
        request: function request() {
            var ajaxRequest = {
                type: this.method,
                url : this.url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id:this.data},
                error : function(data) {
                    var error = data.responseJSON;
                    errorsHtml = '<div class="form-group"><div class="alert alert-danger"><ul>';
                    $.each(error.errors, function(key,value){
                        // console.log(value);
                        errorsHtml += '<li>'+ value +'</li>';
                    });
                    errorsHtml += '</ul></div></div>';

                    $('#error_message').html(errorsHtml);
                },
                success : function(data) {
                    $('#error_message').html('');
                    swal.fire({
                        title: '複製成功!',
                        type: 'success',
                        showConfirmButton: false,
                        timer: 1000,
                    }).then(function(){
                        if(redirect === null)
                        {
                            location.reload();
                        }
                        else
                        {
                            location.href = redirect;
                        }
                    })
                }
            };

            $.ajax(ajaxRequest);
        }

    }
}